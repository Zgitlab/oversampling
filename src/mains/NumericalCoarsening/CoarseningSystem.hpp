#ifndef __COARSENINGSYSTEM_H__
#define __COARSENINGSYSTEM_H__



#include <Eigen/Sparse>
#include <iostream>


#include <mains/NumericalCoarsening/CompositeMaterial.hpp>
#include <mains/NumericalCoarsening/CoarseElement.hpp>
#include <mains/NumericalCoarsening/HybridKinetics.hpp>


using namespace std;
using namespace Eigen;


typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> T;

class CoarseningSystem
{
    public:
        CoarseningSystem(ArbitraryQuadMesh* _pHMesh,
                         vector<CompositeMaterial*> _v_pCMaterial, 
                         vector<vector<vector<Matrix2d>>> vW,
                         int fpc);

        void AssembleEnergy();
        void AssembleForce();
        void AssembleMass();
        void AssembleHessian();

        void AssemblePosition();
        void setPosition(VectorXd _p);

        void AssembleVelocity();
        void setVelocity(VectorXd _v);
        void AssembleMassMatrix();//Shape function dependent

        void getEnergy(double& e);
        void getForce(VectorXd& f);
        void getMass(SpMat& M);
        void getHessian(SpMat& H);
        void getPosition(VectorXd& p);
        void getVelocity(VectorXd& v);
        void FiniteDifferenceMethod(VectorXd& f, MatrixXd& H);


        int n_Nodes;// number of Coarse Nodes 
        int n_DOF;//  number of degree of freedom
        int n_Quads;// number of (Coarse) Quadrilateral Elements

        vector<HybridKinetics*> v_pHKinetics;

    private:       

        ArbitraryQuadMesh* pHMesh;//coarse mesh
        
        
        double Energy;

        VectorXd Force;
        SpMat Mass;
        SpMat Hessian;
        VectorXd Position;
        VectorXd Velocity;

};

CoarseningSystem::CoarseningSystem(ArbitraryQuadMesh* _pHMesh, vector<CompositeMaterial*> _v_pCMaterial, vector<vector<vector<Matrix2d>>> vW,int fpc)
{
    pHMesh=_pHMesh;
    MatrixXi C=pHMesh->ConnectivityMatrix;
    n_Quads=C.rows();
    n_Nodes=pHMesh->v_pCompleteListOfNodes.size();
    n_DOF=2*n_Nodes;
    int FEPD=fpc-1;
    for(int i=0;i<C.rows(); i++)
    {   
        vector<Node*> vq;
        for(int j=0;j<C.cols(); j++)
        {
            vq.push_back(pHMesh->v_pCompleteListOfNodes[C(i,j)]);           
        }

        v_pHKinetics.push_back(new HybridKinetics(_v_pCMaterial[i], new CoarseElement(vq,FEPD,vW[i])));
 
    }   
    AssembleHessian();
    AssembleMass();
    AssemblePosition();
    AssembleVelocity();

}

void CoarseningSystem::AssembleEnergy()
{   
    Energy=0;
    for(int i=0; i<n_Quads; i++)
    {   double e;
        v_pHKinetics[i]->getEnergy(e,true);
        Energy+=e;
    }

}
void CoarseningSystem::AssembleForce()
{   
    MatrixXi C= pHMesh->ConnectivityMatrix;
    VectorXd _Force(n_DOF);
    _Force.setZero();
    for(int i=0; i<n_Quads; i++)
    {
        vector<int> vDOF{   2*C(i,0),2*C(i,0)+1,
                            2*C(i,1),2*C(i,1)+1,
                            2*C(i,2),2*C(i,2)+1,
                            2*C(i,3),2*C(i,3)+1,
                        };
        VectorXd f;
        v_pHKinetics[i]->getForce(f,true);
        _Force(vDOF)+=f;// Vector Indexing New Supported Feature;

    }
    Force=_Force;
}

void CoarseningSystem::AssembleHessian()
{   
   
    MatrixXi C=pHMesh->ConnectivityMatrix;
    vector<T> list_Hessian;

    for(int i=0; i<n_Quads; i++)
    {
        vector<int> vDOF{   2*C(i,0),2*C(i,0)+1,
                            2*C(i,1),2*C(i,1)+1,
                            2*C(i,2),2*C(i,2)+1,
                            2*C(i,3),2*C(i,3)+1,
                        };

        MatrixXd h;
   
        double m;
        
        v_pHKinetics[i]->getHessian(h,true);

        for(int ii=0; ii<vDOF.size();ii++)
        {   
            
            for(int j=0; j<vDOF.size();j++)
            {   
                if(abs(h(ii,j))> 1e-15)
                {
                   
                      T t(vDOF[ii],vDOF[j],h(ii,j)); 
                 
                      list_Hessian.push_back(t);
                
                }
              
            }
        };     

    }

    SpMat _Hessian(n_DOF,n_DOF);
    _Hessian.setFromTriplets(list_Hessian.begin(),list_Hessian.end());

    Hessian= _Hessian;

}


void CoarseningSystem::AssembleMass()
{
    MatrixXi C= pHMesh->ConnectivityMatrix;
    vector<T> list_Mass;

    for(int i=0; i<n_Quads; i++)
    {
        vector<int> vDOF{   2*C(i,0),2*C(i,0)+1,
                            2*C(i,1),2*C(i,1)+1,
                            2*C(i,2),2*C(i,2)+1,
                            2*C(i,3),2*C(i,3)+1,
                        };

        MatrixXd h;
        double m;
        v_pHKinetics[i]->getMass(m);
        m*=0.25;

        Matrix4d MassDistribution;
        MassDistribution<< 4.0,2.0,1.0,2.0,
                           2.0,4.0,2.0,1.0,
                           1.0,2.0,4.0,2.0,
                           2.0,1.0,2.0,4.0;
        MassDistribution/=9.0;

        for(int ii=0; ii<4; ii++)
        {
            for(int j=0; j<4; j++)
            {
                T t1(vDOF[2*ii],  vDOF[2*j],  MassDistribution(ii,j)*m);
                T t2(vDOF[2*ii+1],vDOF[2*j+1],MassDistribution(ii,j)*m);
                list_Mass.push_back(t1);
                list_Mass.push_back(t2);
            }

        }         

    }
    SpMat _Mass(n_DOF,n_DOF);
    _Mass.setFromTriplets(list_Mass.begin(),list_Mass.end());
    Mass=_Mass;
}

void CoarseningSystem::AssemblePosition()
{
    VectorXd _Position(n_DOF);
    for(int i=0; i<n_Nodes;i++)
    {  
        Vector2d v;
        v=pHMesh->v_pCompleteListOfNodes[i]->getPosition();
        _Position.segment(2*i,2)=v;
    }
    Position=_Position;
}

void CoarseningSystem::setPosition(VectorXd _p)
{
    Position=_p;
    for(int i=0; i<n_Nodes;i++)
    {  
        Vector2d v;
        v=_p.segment(2*i,2);   
        pHMesh->v_pCompleteListOfNodes[i]->setPosition(v);
    }
}

void CoarseningSystem::AssembleVelocity()
{
    VectorXd _Velocity(n_DOF);
    for(int i=0; i<n_Nodes;i++)
    {  
        Vector2d v;
        v=pHMesh->v_pCompleteListOfNodes[i]->getVelocity();
        _Velocity.segment(2*i,2)=v;
    }
    Velocity=_Velocity;
}

void CoarseningSystem::setVelocity(VectorXd _v)
{
    Velocity=_v;
    for(int i=0; i<n_Nodes;i++)
    {  
        Vector2d v;
        v=_v.segment(2*i,2);   
        pHMesh->v_pCompleteListOfNodes[i]->setVelocity(v);
    }
}

void CoarseningSystem::getEnergy(double& e)
{   
    AssembleEnergy();
    e=Energy;

}
void CoarseningSystem::getForce(VectorXd& f)
{   
    AssembleForce();
    f=Force;   
}

void CoarseningSystem::getHessian(SpMat& H)
{   

    AssembleHessian();
    H=Hessian;
}

void CoarseningSystem::getMass(SpMat& M)
{   
    AssembleMass();
    M=Mass;
}

void CoarseningSystem::getPosition(VectorXd& p)
{
    p=Position;
}
void CoarseningSystem::getVelocity(VectorXd& v)
{
    v=Velocity;
}
#endif // __COARSENINGSYSTEM_H__