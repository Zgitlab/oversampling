#pragma once
#include <Eigen/Eigen>
#include <mains/2DFEM/Quadrilateral.hpp>
#include <mains/2DFEM/Mesh.hpp>
class CoarseElement
{
    public:
        int FEPD;//FineElementPerDimension;
        int n_FineNodes;//including coarse nodes
        int n_Quadrature;//quadrature points per fine elements;
        int n_FineElement;
        int n_CoarseNodes=4;
        double Area;
        double lengthFine, lengthCoarse;


        vector<Vector2d> vQuadraturePoints;// Quadrature points in the parametric coordinate of fine elements;
        vector<double> vQuadratrureWeights;

        MatrixXi C;//connectivity matrix for fine nodes inside the element;
        Vector4i SequenceChange;
        vector<Node*> v_pFineNodes;
        vector<Node*> v_pCoarseNodes;
        Matrix2d F,R;//Deformation Gradient and Rotational Matrix

        vector<Matrix2d> vF;//deformation gradient per fine element(single quadrature point in the center)
        vector<vector<Matrix2d>> vvF;// multiquadrature points

        vector<MatrixXd> vDFDX;//block B;
        vector<vector<MatrixXd>> vvDFDX;//block B multiquadrature points;


        vector<vector<Matrix2d>> arrayW;//array of tensorial weight

        QuadRectangleMesh* pReferenceMesh;

        Vector2d Center;// location of the element center in reference frame

        CoarseElement(vector<Node*> _v_pCoarseNodes,int _FEPD,vector<vector<Matrix2d>>  _arrayW);

        void setFineNodesPosition();
        void setCoarseDeformationGradient();// only for square
        void setRotation();
        void setFineDeformationGradient();
        void setvvF();// vvF[i][j] deformation gradient in fine element i on quadrature point j
        void setvvF2();//vvF2 : using linear interpolation, reuse the 2DFEM code.
        
        void setvDFDX();//set block B;
        
        void setvvDFDX();//vvDFDX[i][j] blockB in fine element i and on quadrature point j
        void setvvDFDX2();

        void setQuadrature();

        void getvDFDX(vector<MatrixXd>& _vDFDX);
        void getvvDFDX(vector<vector<MatrixXd>>& _vvDFDX);

        void getvDeformationGradient(vector<Matrix2d>& _vF);
        void getvvF(vector<vector<Matrix2d>>& _vvF);
        void setArea();//area of fine element
        void getArea(double& _a);// area of fine element

        void setConnectivity();


};

CoarseElement::CoarseElement(vector<Node*> _v_pCoarseNodes,int _FEPD, vector<vector<Matrix2d>>  _arrayW)//input is naive order
{
    v_pCoarseNodes=_v_pCoarseNodes;

    SequenceChange<< 0,2,3,1;
    FEPD=_FEPD;
    arrayW=_arrayW;

    n_FineNodes=(FEPD+1)*(FEPD+1);

    Vector2d X2=v_pCoarseNodes[2]->getInitialPosition();
    Vector2d X0=v_pCoarseNodes[0]->getInitialPosition();
    double l= (X2(0)-X0(0))/FEPD;  
    Center=(X2+X0)/2;  
    pReferenceMesh =new QuadRectangleMesh(FEPD+1,FEPD+1,l);

    v_pFineNodes= pReferenceMesh->v_pCompleteListOfNodes; // location to be centered properly


    n_FineElement=FEPD*FEPD;
    setConnectivity();
    setArea();
    setQuadrature();

  
}
void CoarseElement::setFineNodesPosition()
{

    setRotation();
  
     for (int k=0;k<n_FineNodes;k++)
     {  
         Vector2d xj;
         xj=v_pFineNodes[k]->getInitialPosition()+Center;
         xj=R*xj;
     
         for(int i=0;i<n_CoarseNodes;i++)
         {   
            int ic= SequenceChange(i);
            Vector2d xi,Xi;
            xi= v_pCoarseNodes[i]->getPosition();
            Xi= v_pCoarseNodes[i]->getInitialPosition();
          
            xj+=R*arrayW[ic][k]*(R.transpose()*xi-Xi);

         };
         v_pFineNodes[k]->setPosition(xj);
  
     };

}
void CoarseElement::setvvF()
{   
    
    setRotation();

    MatrixXd P(4,2);
    vector<vector<Matrix2d>> _vvF;
    P<< -1,-1,
         1,-1,
         1, 1,
        -1, 1;
    for (int k=0;k<n_FineElement;k++)
    {
        vector<Matrix2d> _vF;
        for(int kq=0;kq<n_Quadrature;kq++) 
        {        
            Matrix2d _F;     
            _F= R;
            for(int i=0;i<n_CoarseNodes;i++)
            {
                for(int l=0; l<4;l++)
                {   
                    int ic= SequenceChange(i);
                    Vector2d xi,Xi;
                    int j= C(k,l);
                    xi= v_pCoarseNodes[i]->getPosition();
                    Xi= v_pCoarseNodes[i]->getInitialPosition();
                    
                    Vector2d _v= R* arrayW[ic][j]*
                                (R.transpose()*xi-Xi);
                    Vector2d vpi;
                    vpi<< vQuadraturePoints[kq](1), vQuadraturePoints[kq](0);
                    Vector2d dNdX= (P.row(l).transpose()+ P(l,0)*P(l,1)*vpi)/(2.0*lengthFine);
                    Matrix2d _m= _v* dNdX.transpose();
                    _F+=_m;
                }
            }
            _vF.push_back(_F);
        }
        _vvF.push_back(_vF);        
    }
    vvF=_vvF;
}
void CoarseElement::setvvF2()
{
    setFineNodesPosition();
    vector<vector<Matrix2d>> vvF;
    for (int k=0;k<n_FineElement;k++)
    {
        MatrixXi C=pReferenceMesh->ConnectivityMatrix;
        vector<Node*> vn;

        for(int i=0; i<4;i++)
        {
            vn.push_back(v_pFineNodes[C(k,i)]);
        }
        auto pQuad= new Quadrilateral(vn);
        vector<Matrix2d> vF;
        pQuad->getvDeformationGradient(vF);
    }
    vvF.push_back(vF);
}
void CoarseElement::setvvDFDX()
{
    
    setRotation();    
    MatrixXd P(4,2);
 
    P<< -1,-1,
         1,-1,
         1, 1,
        -1, 1;// e ordering
    vector<vector<MatrixXd>> _vvDFDX;
    for (int k=0;k<n_FineElement;k++)
    {   
        vector<MatrixXd> _vDFDX;
        for(int kq=0;kq<n_Quadrature;kq++) 
        {    
            MatrixXd _DFDX;
            _DFDX.resize(4,8); 
            _DFDX.setZero();
            for(int l=0; l<4;l++)
            {     
                for (int alpha=0; alpha<2; alpha++)
                {            
                    for(int beta=0; beta<2; beta++)
                    {
                        for(int gama=0; gama<2; gama++)
                        {
                            for(int i=0;i<n_CoarseNodes;i++)
                            {
                                int j= C(k,l);
                                int ic= SequenceChange(i);
                                Matrix2d RWRT=R* arrayW[ic][j]*R.transpose();
                                Vector2d vpi;
                                vpi<< vQuadraturePoints[kq](1), vQuadraturePoints[kq](0);
                                Vector2d dNdX= (P.row(l).transpose() + P(l,0)*P(l,1)*vpi)/(2.0*lengthFine);
                                _DFDX(alpha+2*beta,2*i+gama)+=RWRT(alpha, gama)* dNdX(beta);
                            }
                        }
                    }
                }
            }
            _vDFDX.push_back(_DFDX);
        }
        vvDFDX.push_back(_vDFDX);
    }
};

void CoarseElement::getvvDFDX(vector<vector<MatrixXd>>& _vvDFDX)
{
    setvvDFDX();
    _vvDFDX=vvDFDX;
}


void CoarseElement::getvvF(vector<vector<Matrix2d>>& _vvF)
{
    setvvF();
    _vvF=vvF;
}


void CoarseElement::setQuadrature()
{   MatrixXd P(4,2);
 
    P<< -1,-1,
         1,-1,
         1, 1,
        -1, 1;
    n_Quadrature=4;
    for(int i=0; i<n_Quadrature;i++)
    {
        Vector2d v= 0.57735026919* P.row(i);//1/(sqrt(3))
        vQuadraturePoints.push_back(v);
        vQuadratrureWeights.push_back(0.25);
    }
    

}
void CoarseElement::setConnectivity()
{
    auto pMesh= new QuadRectangleMesh(FEPD+1, FEPD+1, 1.0);
    pMesh->getQuads(C);
//     VectorXi Ci,Cj,Ck;

//     //reorder the index sequence corresponding to the reference 
//     Ci=C.col(1);
//     Cj=C.col(2);
//     Ck=C.col(3);
//     C.col(3)=Cj;
//     C.col(2)=Ci;
//     C.col(1)=Ck;
}

void CoarseElement::setArea()
{
   
    Vector2d x0 =v_pCoarseNodes[0]->getInitialPosition();
    Vector2d x1 =v_pCoarseNodes[1]->getInitialPosition();
    Vector2d x2 =v_pCoarseNodes[2]->getInitialPosition();
    Vector2d x3 =v_pCoarseNodes[3]->getInitialPosition();

    Vector2d a= (x1-x0);
    Vector2d b= (x3-x0);
    Vector2d c= (x3-x2);
    Vector2d d= (x1-x2);

    Area= 0.5* abs(a(0)*b(1)-a(1)*b(0))+ 0.5* abs(c(0)*d(1)-c(1)*d(0)) ;
    Area/=FEPD*FEPD;
}

void CoarseElement::setCoarseDeformationGradient()
{
    MatrixXd x(2,4);
    MatrixXd X(2,4);
    for (int i=0; i<4; i++)
    {   
        x.col(i)=v_pCoarseNodes[i]->getPosition();
        X.col(i)=v_pCoarseNodes[i]->getInitialPosition();
    }
    MatrixXd P(4,2);
 
    P<< -1,-1,
         1,-1,
         1, 1,
        -1, 1;

    
  
    double l= abs(X(0,0)-X(0,2));
    lengthCoarse=l;
    lengthFine=l/FEPD;
    F=x*P/(2.0*l);    
}


void CoarseElement::setRotation()
{   
    setCoarseDeformationGradient();
    JacobiSVD<Matrix2d> SVD(F,ComputeFullU|ComputeFullV);
    R=SVD.matrixU()*SVD.matrixV().transpose();
}

void CoarseElement::setFineDeformationGradient()
{   
    
    setRotation();

    MatrixXd P(4,2);
    vector<Matrix2d> _vF;
    P<< -1,-1,
         1,-1,
         1, 1,
        -1, 1;
    for (int k=0;k<n_FineElement;k++)
    {
        Matrix2d _F;        
        _F= R;
        for(int i=0;i<n_CoarseNodes;i++)
        {
            for(int l=0; l<4;l++)
            {   
                int ic= SequenceChange(i);
                Vector2d xi,Xi;
                int j= C(k,l);
                xi= v_pCoarseNodes[i]->getPosition();
                Xi= v_pCoarseNodes[i]->getInitialPosition();
                Vector2d _v= R* arrayW[ic][j]*
                            (R.transpose()*xi-Xi);
                Vector2d dNdX= P.row(l)/2.0/lengthFine;
                Matrix2d _m= _v* dNdX.transpose();
                _F+=_m;
            }
        }

        _vF.push_back(_F);
    }
    vF=_vF;
}

void CoarseElement::setvDFDX()// X-> x_i 
{
    
    setRotation();    
    MatrixXd P(4,2);
 
    P<< -1,-1,
        -1, 1,
         1,-1,
         1, 1;// e ordering
     
    for (int k=0;k<n_FineElement;k++)
    {   
        MatrixXd _DFDX;
        _DFDX.resize(4,8); 
        _DFDX.setZero();
        for(int l=0; l<4;l++)
        {     
            for (int alpha=0; alpha<2; alpha++)
            {            
                for(int beta=0; beta<2; beta++)
                {
                    for(int gama=0; gama<2; gama++)
                    {
                        for(int i=0;i<n_CoarseNodes;i++)
                        {
                            int j= C(k,l);
                            int ic= SequenceChange(i);
                            Matrix2d RWRT=R* arrayW[ic][j]*R.transpose();
                            Vector2d dNdX= 0.5 *P.row(l)/lengthFine;
                            _DFDX(alpha+2*beta,2*i+gama)+=RWRT(alpha, gama)* dNdX(beta);
                        }
                    }
                }
            }
        }
        vDFDX.push_back(_DFDX);
    }
};

void CoarseElement::getvDFDX(vector<MatrixXd>& _vDFDX)
{   
    setvDFDX();
    _vDFDX=vDFDX;
}
void CoarseElement::getvDeformationGradient(vector<Matrix2d>& _vF)
{   

    setFineDeformationGradient();
    _vF= vF;
};
void CoarseElement::getArea(double& _a)
{_a= Area;}// area of fine element
