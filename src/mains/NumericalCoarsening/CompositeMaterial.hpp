#pragma once
#include <mains/2DFEM/Materials/Material_StVK.hpp>

class CompositeMaterial
{
    public:
        vector<Material*> v_pMateiral;

        void getEnergyDensity(int iMaterial,Matrix2d F, double& e)
        {  
            v_pMateiral[iMaterial]->getEnergyDensity(F,e);};//F deformation gradient

        void getPiolaStress(int iMaterial,Matrix2d F, Matrix2d& P)
        {v_pMateiral[iMaterial]->getPiolaStress(F,P);};

        void getSecondOrderDerivativeOfEnergy(int iMaterial,Matrix2d F, MatrixXd& S)
        {v_pMateiral[iMaterial]->getSecondOrderDerivativeOfEnergy(F,S);};//4*4 Matrix

};