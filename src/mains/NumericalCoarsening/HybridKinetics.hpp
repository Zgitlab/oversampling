#pragma once
#include <mains/NumericalCoarsening/CompositeMaterial.hpp>
#include <mains/NumericalCoarsening/CoarseElement.hpp>

class HybridKinetics
{
    public:
        HybridKinetics(CompositeMaterial* _pCMaterial, CoarseElement* _pCoarseEle);
        void setEnergy();
        void setForce();       
        void setHessian();
        void setMass();

        void setForce(bool multiQuadrature);
        void setEnergy(bool multiQuadrature);
        void setHessian(bool multiQuadrature);

        void getEnergy(double& e);
        void getForce(VectorXd& vf); 
        void getHessian(MatrixXd& H);

        void getEnergy(double& e,    bool multiQuadrature);
        void getForce(VectorXd& vf,  bool multiQuadrature); 
        void getHessian(MatrixXd& H, bool multiQuadrature);


        void getMass(double& m);

        CoarseElement* pCoarseElement;

    private:
        CompositeMaterial* pCMaterial;
        

        double Energy;
        double Mass;    
        VectorXd vForce;
        MatrixXd Hessian;



};

HybridKinetics::HybridKinetics(CompositeMaterial* _pCMaterial, CoarseElement* _pCoarseEle)
{   
 
    pCMaterial=_pCMaterial;
    pCoarseElement=_pCoarseEle;
    setMass();
}

void HybridKinetics::setEnergy()
{   vector<Matrix2d> vF;
    double a;
    double _Energy=0;
    pCoarseElement->getvDeformationGradient(vF);

    pCoarseElement->getArea(a);
    for(int i=0;i<vF.size();i++)
    {           
        double e;
        pCMaterial->getEnergyDensity(i,vF[i], e);
        _Energy+=e*a;               
    }
    Energy=_Energy;

}
void HybridKinetics::setForce()
{
        vector<Matrix2d> vF;
        vector<MatrixXd> vB;
        pCoarseElement->getvDeformationGradient(vF);
        pCoarseElement->getvDFDX(vB);

        double Area;
        pCoarseElement->getArea(Area);

        VectorXd _Force(8);//not general
        _Force.setZero();

        for (int i=0;i< vF.size();i++)
        {
            Matrix2d F,P;
            F=vF[i];
            pCMaterial->getPiolaStress(i,F,P);
            VectorXd VecP(Map<VectorXd>(P.data(),P.size()));
            _Force-=Area*vB[i].transpose()*VecP;            
        }
        vForce=_Force;

}
void HybridKinetics::setForce(bool multiQuadrature)
{       
        if (!multiQuadrature)
            {setForce();}
        else
        {
            vector<vector<Matrix2d>> vvF;
            vector<vector<MatrixXd>> vvB;
            pCoarseElement->getvvF(vvF);
            pCoarseElement->getvvDFDX(vvB);

            double Area;
            pCoarseElement->getArea(Area);
            int nQ= pCoarseElement->n_Quadrature;
            VectorXd _Force(8);//not general
            _Force.setZero();

            for (int i=0;i< vvF.size();i++)
            {
                for(int j=0; j< nQ; j++)
                {
                    Matrix2d F,P;
                    F=vvF[i][j];
                    pCMaterial->getPiolaStress(i,F,P);
                    VectorXd VecP(Map<VectorXd>(P.data(),P.size()));
                    _Force-=Area*vvB[i][j].transpose()*VecP;       

                }
     
            }
            vForce=_Force/nQ;

        }
       

}

void HybridKinetics::setEnergy(bool multiQuadrature)
{   
    if (!multiQuadrature)
        {setEnergy();}
    else
    {    
        vector<vector<Matrix2d>> vvF;
        pCoarseElement->getvvF(vvF);
        int nQ= pCoarseElement->n_Quadrature;

        double a;
        double _Energy=0;
        pCoarseElement->getvvF(vvF);
        pCoarseElement->getArea(a);
        for(int i=0;i<vvF.size();i++)
        {   
            for(int j=0; j< nQ; j++)
            {        
                double e;
                pCMaterial->getEnergyDensity(i,vvF[i][j], e);
                _Energy+=e*a;               
            }
        }
        Energy=_Energy/nQ;
    }
    


}


void HybridKinetics::setHessian(bool multiQuadrature)
{
    if (!multiQuadrature)
        {setHessian();}
    else
    {  
        vector<vector<Matrix2d>> vvF;
        vector<vector<MatrixXd>> vvB;
        pCoarseElement->getvvF(vvF);
        pCoarseElement->getvvDFDX(vvB);
        int nQ= pCoarseElement->n_Quadrature;

        MatrixXd _Hessian(8,8);
        _Hessian.setZero();
        double Area;
        pCoarseElement->getArea(Area);
        for(int i=0;i<vvF.size();i++)
        {   
            for(int j=0; j< nQ; j++)
            {        
                MatrixXd S;
                pCMaterial->getSecondOrderDerivativeOfEnergy(i,vvF[i][j],S);
                _Hessian-= Area* vvB[i][j].transpose()* S * vvB[i][j];
            }
        }

        Hessian=_Hessian/nQ;
    }
}


void HybridKinetics::setHessian()
{
    vector<Matrix2d> vF;
    vector<MatrixXd> vB;
    pCoarseElement->getvDeformationGradient(vF);
    pCoarseElement->getvDFDX(vB);

    MatrixXd _Hessian(8,8);
    _Hessian.setZero();
    double Area;
    pCoarseElement->getArea(Area);
    for(int i=0;i<vF.size();i++)
    {   
        MatrixXd S;
        pCMaterial->getSecondOrderDerivativeOfEnergy(i,vF[i],S);
        _Hessian-= Area* vB[i].transpose()* S * vB[i];
    }
    Hessian=_Hessian;

}
void HybridKinetics::setMass()//need improvement
{   
    Mass =0;
    for(int i=0; i< pCMaterial->v_pMateiral.size(); i++)
    {
        double rho= pCMaterial->v_pMateiral[i]->Density;
        double A;
        pCoarseElement->getArea(A);
        Mass+=rho*A;
    }
   
}

void HybridKinetics::getEnergy(double& e)
{   setEnergy();
    e=Energy;
}

void HybridKinetics::getEnergy(double& e, bool multiquadrature)
{   setEnergy(multiquadrature);
    e=Energy;
}

void HybridKinetics::getForce(VectorXd& vf)
{   setForce();
    vf=vForce;
}

void HybridKinetics::getForce(VectorXd& vf, bool multiquadrature)
{   setForce(multiquadrature);
    vf=vForce;
}

void HybridKinetics::getHessian(MatrixXd& H)
{   setHessian();
    H=Hessian;
}

void HybridKinetics::getHessian(MatrixXd& H, bool multiquadrature)
{   setHessian(multiquadrature);
    H=Hessian;
}

void HybridKinetics::getMass(double& m)
{
    m=Mass;
}


