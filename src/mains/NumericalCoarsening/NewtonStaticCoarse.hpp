#pragma once
#include <Eigen/Sparse>
#include <mains/NumericalCoarsening/CoarseningSystem.hpp>
#include <mains/2DFEM/Boundary.hpp>
#include <utils/SparseMatrixSlicing.hpp>
class NewtonStaticForCoarsening
{
    public:
        NewtonStaticForCoarsening(CoarseningSystem* _pSystem);
        void SingleStep();

        void setBC();
        void Energy(double& U);
        vector<Boundary*> v_pBoundaryConditions;
        VectorXd MG;

    private:
        CoarseningSystem* pSystem;
        VectorXd RHS;
        SpMat LHS;
        double dt=5e-2; // time step

};

NewtonStaticForCoarsening::NewtonStaticForCoarsening(CoarseningSystem* _pSystem)
{
    pSystem= _pSystem;
    LHS.resize(pSystem->n_DOF, pSystem->n_DOF);
    RHS.resize(pSystem->n_DOF);
    LHS.setZero();
    RHS.setZero();

}

void NewtonStaticForCoarsening::setBC()
{   
    
    SpMat M;
    pSystem->getMass(M);
   
    // Management of the Boundary condition
    for (int i=0; i<v_pBoundaryConditions.size();i++)
    {
       auto pBC=v_pBoundaryConditions[i];
       if (pBC->Type=='G')// Gravity
            {
                double g =pBC->valueDOF[0];
                Vector2d vg;
                vg<<0.0,g;
                VectorXd VG=vg.replicate(pSystem->n_Nodes,1);
                MG=M*VG;
                RHS-= M* VG;
            
            }
        else if (pBC->Type=='N')
            {

                if (pBC->indexDOF.size()==pBC->valueDOF.size())
                {
                    VectorXd NForce(pBC->indexDOF.size());
                    for(int i=0;i<pBC->indexDOF.size();i++)
                    {
                        NForce(i)=pBC->valueDOF[i];
                    }
                    RHS(pBC->indexDOF)-=NForce;
                    MG(pBC->indexDOF)+=NForce;
                }              

            }
        else if (pBC->Type=='D')// Dirichlet Boundary Condition
            {   
                auto valueDOF=pBC->valueDOF;//todo
                auto vFixIndices=pBC->indexDOF;
                auto vStencil=pBC->vStencil;
              
                VectorXd vzero;
                vzero.resize(vFixIndices.size());
                vzero.setZero();

                SpMat Hmix;
                vector<int> AllIndex,vFreeIndices;
                for(int k=0;k<pSystem->n_DOF;k++)
                {
                    AllIndex.push_back(i);
                }
                set_difference(AllIndex.begin(),AllIndex.end(),
                            vFixIndices.begin(),vFixIndices.end(),
                            inserter(vFreeIndices,vFreeIndices.begin()));
                Hmix=MatrixSlicing(vFreeIndices,vFixIndices,LHS);
                VectorXd p1;
                pSystem->getPosition(p1);
                
                RHS(vFreeIndices)-=Hmix*p1(vFixIndices);

                RHS(vFixIndices)=vzero;
            
                for (int k = 0; k < LHS.outerSize(); ++k)
                {
                    for (SpMat::InnerIterator it(LHS, k); it; ++it)
                    {
                        if (vStencil[it.row()] || vStencil[it.col()])
                        {
                            if (it.row() == it.col())
                            {
                                it.valueRef() = 1.0;
                            }    
                            else
                            {
                                it.valueRef() = 0.0;
                            }; 
                        };
                    };                
                }       
            }

    };
}

void NewtonStaticForCoarsening::SingleStep()
{   
    SpMat dfdx,M;
    pSystem->getHessian(dfdx);
    pSystem->getMass(M);
    LHS.resize(dfdx.rows(), dfdx.cols());
    LHS.setIdentity();
    LHS*=100;
    LHS-=dfdx;
    
    VectorXd f0,x0,x1;
    double U0,U1,step_size;
    step_size=1.0;

    pSystem->getForce(f0);
   
    pSystem->getPosition(x0);
   
    RHS=f0;
    cout<<"f0 max"<<f0.maxCoeff()<<endl;
    setBC();
    cout<<"RHS\n"<<RHS.maxCoeff()<<endl;

    Energy(U0);
    cout<<"Energy"<<U0<<endl;
 
    

	Eigen::SimplicialLDLT<SpMat> ldlt(LHS);

	Eigen::VectorXd dx = ldlt.solve(RHS);
    cout<<"dx"<<dx.maxCoeff()<<endl;

    // SparseQR<SparseMatrix<double>, COLAMDOrdering<int> >   solver;
    // solver.analyzePattern(LHS); 
    // solver.factorize(LHS);
    // VectorXd dx = solver.solve(RHS); 


    for(int i=0;i<20;i++)
    {
        x1= x0- step_size* dx;
        pSystem->setPosition(x1);
        Energy(U1);
        if(U0>U1)
        {   
            cout<<"round "<<i<<"\n U0   "<<U0<<"\n U1   "<<U1<<"\n Good one!"<<endl;
            break;
        }
        else
        {   
            cout<<"round "<<i<<"\n U0   " <<U0<<"\n U1   "<<U1<<endl;
            pSystem->setPosition(x0);
            step_size*=0.5;
        }

    }

} 

void NewtonStaticForCoarsening::Energy(double& U)
{
    pSystem->getEnergy(U);
    cout<<"Elastic Energy  "<<U<<endl;
    VectorXd x;
    pSystem->getPosition(x);
    cout<<"Gravitational Energy"<<MG.dot(x)<<endl;
    U-=MG.dot(x);

    


};



