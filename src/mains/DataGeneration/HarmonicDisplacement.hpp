#pragma once
#include <mains/2DFEM/System.hpp>
#include <mains/ADMM/util.hpp>
#include <utils/Render.hpp>

using namespace Eigen;
using namespace std;
typedef SparseMatrix<double> SpMat;

class HarmonicDisplacement
{
public:
    HarmonicDisplacement(){};

    HarmonicDisplacement(ArbitraryQuadMesh *_pMesh, System *_pSystem);

    void getHD(vector<MatrixXd> &_vHD);
    void getvvHD(vector<vector<MatrixXd>> &_vvHD, SpMat GlobalPattern, int fpc); // first index is data set, second index is the coarse element real number in world

    System *pSystem;
    SpMat Hessian;
    SparseVector<double> Force;
    ArbitraryQuadMesh *pMesh;
    VectorXd ForceDense;
    vector<MatrixXd> vHD;
    void setForce(Matrix2d stress);
};

HarmonicDisplacement::HarmonicDisplacement(ArbitraryQuadMesh *_pMesh, System *_pSystem)
{
    pSystem = _pSystem;
    pSystem->getHessian(Hessian);

    pMesh = _pMesh;

    vector<Matrix2d> stress(3);
    stress[0] << 1, 0, 0, 0;
    stress[1] << 0, 0, 0, 1;
    stress[2] << 0, 1, 1, 0;

    SparseQR<SpMat, COLAMDOrdering<int>> QRsolver(-Hessian);

    for (int i = 0; i < 3; i++)
    {
        setForce(stress[i]);

        MatrixXd a = QRsolver.solve(Force);

        a.resize(2, pSystem->n_Nodes);
        MatrixXd b;
        b = a.transpose();
        vHD.push_back(1e3 * b);
    };
}

void HarmonicDisplacement::setForce(Matrix2d stress)
{
    Force.resize(pSystem->n_DOF);
    ForceDense.resize(pSystem->n_DOF);
    ForceDense.setZero();
    MatrixXi circ;
    MatrixXd m_normal;
    pMesh->getCircumferences(circ);

    pMesh->getNormalVector(m_normal);

    for (int i = 0; i < circ.rows(); i++)
    {
        Vector2d n = m_normal.row(i);
        Vector2d f = stress * n;

        Force.coeffRef(2 * circ(i, 0)) += f(0) / 2.0;
        Force.coeffRef(2 * circ(i, 0) + 1) += f(1) / 2.0;
        ForceDense(2 * circ(i, 0)) += f(0) / 2.0;
        ForceDense(2 * circ(i, 0) + 1) += f(1) / 2.0;

        Force.coeffRef(2 * circ(i, 1)) += f(0) / 2.0;
        Force.coeffRef(2 * circ(i, 1) + 1) += f(1) / 2.0;
        ForceDense(2 * circ(i, 1)) += f(0) / 2.0;
        ForceDense(2 * circ(i, 1) + 1) += f(1) / 2.0;
    };
}

void HarmonicDisplacement::getHD(vector<MatrixXd> &_vHD)
{
    _vHD = vHD;
}
void HarmonicDisplacement::getvvHD(vector<vector<MatrixXd>> &_vvHD, SpMat globalCoarsePattern, int fpc)
{
    _vvHD.clear();
    for (auto data : vHD)
    {
        vector<MatrixXd> vM;
        for (int k = 0; k < globalCoarsePattern.outerSize(); ++k)
        {
            for (SparseMatrix<double>::InnerIterator it(globalCoarsePattern, k); it; ++it)
            {
                int xc = it.row();
                int yc = it.col();
                MatrixXd m(fpc * fpc, 2);

                for (int localIndex = 0; localIndex < fpc * fpc; localIndex++)
                {
                    int GlobalIndex;
                    ADMM::mapLocalIndexToGlobal(GlobalIndex, localIndex, xc, yc, fpc, globalCoarsePattern);
                    auto mapa = pMesh->MapAll2Active;
                    if (mapa.find(GlobalIndex) == mapa.end())
                    {
                        m.row(localIndex) = RowVector2d::Zero(); // Vaccum. will be random if necessary
                    }
                    else
                    {
                        m.row(localIndex) = data.row(mapa[GlobalIndex]);
                    }
                }
                vM.push_back(m);
                Render::RenderCoarseElement(fpc, 1.0, 20 * m);
            }
        }
        _vvHD.push_back(vM);
    }
}