#pragma once
#include <mains/2DFEM/Boundary.hpp>
class Dirichlet:public Boundary
{
    public:

        Dirichlet(vector<int> _indexDOF, vector<double> _valueDOF, vector<bool> _vStencil);
        Dirichlet(vector<int> _indexDOF, vector<double> _valueDOF, int  n_DOF);
};

Dirichlet::Dirichlet(vector<int> _indexDOF, vector<double> _valueDOF, vector<bool> _vStencil)
{
    isSet=true;
    Type='D';
    indexDOF= _indexDOF;
    valueDOF=_valueDOF;
    vStencil=_vStencil;
};

Dirichlet::Dirichlet(vector<int> _indexDOF, vector<double> _valueDOF, int  n_DOF)
{
    isSet=true;
    Type='D';
    indexDOF= _indexDOF;
    valueDOF=_valueDOF;
    vector<bool> _vStencil;
    for(int i=0; i<n_DOF;i++)
    {
        _vStencil.push_back(false);
    }
    for(int i=0; i<_indexDOF.size(); i++)
    {
        _vStencil[_indexDOF[i]]=true;
    }
    vStencil=_vStencil;
};
