#pragma once
#include <mains/2DFEM/Boundary.hpp>
class Neumann:public Boundary
{
    public:
        Neumann(vector<int> _indexDOF, vector<double> _valueDOF);
};

Neumann::Neumann(vector<int> _indexDOF, vector<double> _valueDOF)
{
    isSet=true;
    Type='N';
    indexDOF= _indexDOF;
    valueDOF=_valueDOF;
};