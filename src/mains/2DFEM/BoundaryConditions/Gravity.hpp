#pragma once
#include <mains/2DFEM/Boundary.hpp>
class Gravity:public Boundary
{
    public:

        Gravity(double g);
};

Gravity::Gravity(double g)
{   
    vector<double> _valueDOF;
    _valueDOF.push_back(g);
    isSet=true;
    Type='G';
    valueDOF=_valueDOF;
};