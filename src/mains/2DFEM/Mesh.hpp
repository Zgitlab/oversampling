#pragma once
#include <Eigen/Sparse>
#include <iostream>
#include <mains/2DFEM/Node.hpp>
#include <mains/ADMM/util.hpp>
#include <set>
typedef Eigen::SparseMatrix<double> SpMat;

class Mesh
{
    public:
        MatrixXi ConnectivityMatrix;
        MatrixXi Edges;
        vector<Node*> v_pCompleteListOfNodes;

        int N_cell; 
	    int N_edges;
	    int N_nodes;
};

class QuadRectangleMesh: public Mesh
{
    public:
    	int nx=2;//nodes along the axis;
		int ny=2;

		
		double unit_length=1;
		
		QuadRectangleMesh(int _nx, int _ny, double _unit_length);
		QuadRectangleMesh();


        void setPositions();
        void setQuads();
        void setCircumferences();// the boundary
        void setEdges();
		
		void setInnerProductOfDerivative();
		
        void getPositions(MatrixXd& _NodesPositions);
        void getQuads(MatrixXi&_Quads);
        void getCircumferences(MatrixXi& _Circs);
        void getNormalVector(MatrixXd& _Normal);
        void getEdges(MatrixXi& _Edges);
        void getInnerProductOfDerivative(MatrixXd& _Inner);
        
    private:
        MatrixXd Positions;//to be modified
        MatrixXi Quads;
        MatrixXi Circumferences;
        MatrixXd normalVector;
        MatrixXd InnerProductOfDerivative;
        

};

QuadRectangleMesh::QuadRectangleMesh(int _nx, int _ny, double _unit_length)
{
    nx=_nx;
    ny=_ny;
    unit_length=_unit_length;
    N_nodes=nx*ny;
    N_cell= (nx-1)*(ny-1);
    N_edges= 4*N_cell;

    setPositions();    
    setQuads();   
    setCircumferences();
    setEdges();

    ConnectivityMatrix=Quads;

};

QuadRectangleMesh::QuadRectangleMesh()
{
    nx=2;
    ny=2;
    unit_length=1;
    N_nodes=nx*ny;
    N_cell= (nx-1)*(ny-1);
    N_edges= 4*N_cell;    

    setPositions();    
    setQuads();   
    setCircumferences();
    //setEdges();
    ConnectivityMatrix=Quads;

};
void QuadRectangleMesh::setEdges()
{
    MatrixXi _Edges(2*nx*ny-nx-ny,2);
    int ie=0;
    for(int i=0; i<nx; i++)
    {
        for(int j=0; j<(ny-1); j++)
        {     
            Vector2i v;
            v<< ny*i+j, ny*i+j+1;
             _Edges.row(ie)=v;
             ie++;             
        }     
   }

    for(int i=0; i<nx-1; i++)
    {
        for(int j=0; j<ny; j++)
        {     
            Vector2i v;
            v<< ny*i+j, ny*(i+1)+j;
             _Edges.row(ie)=v;
             ie++;             
        }     
   }
    Edges=_Edges;
}

void QuadRectangleMesh::setPositions()
{
    Vector2d shift;
    shift<< (nx-1)*unit_length/2.0,  (ny-1)*unit_length/2.0;
    MatrixXd _Positions(N_nodes,2);
    
    
     for (int i = 0; i < nx; i++)
        {
            for (int j = 0; j < ny; j++) 
                { 
                    Vector2d a;
                    a<<i*unit_length, j*unit_length;
                    a-=shift;
                    _Positions.row(ny*i+j)= a.transpose();

                    Node* pN=  new Node(a);
                    v_pCompleteListOfNodes.push_back(pN);

                };
        }; 

    Positions= _Positions;     
  
}

void QuadRectangleMesh::setQuads()
{
    MatrixXi _C(N_cell, 4);
    int iC=0;
    for (int i = 0; i < nx-1; i++)
        {
            for (int j = 0; j < ny-1; j++) 
                {
                    _C.row(iC)<<  ny* i   + j,
                                  ny*(i+1)+ j,
                                  ny*(i+1)+(j+1),
                                  ny* i   +(j+1);
                    iC++;   
                };

        };
    Quads=_C;
}


void QuadRectangleMesh::setCircumferences()// set Circumferences and normal vector
{   int n_s= 2*((nx-1)+(ny-1));
    MatrixXi _CF(n_s,2);//circumfere edges
    MatrixXd  _n(n_s,2);// normal vectors
    int iCF=0;
    for (int i = 0; i < nx-1; i++)
        {   Vector2i cf;
            Vector2d vn;
            int j=0;
            cf<<  ny* i   + j, ny*(i+1)+j;
            vn<<  0, -1;
            _CF.row(iCF)=cf;
            _n.row(iCF)=vn;
            iCF++;

            j= ny-1;
            cf<<  ny* i   + j, ny*(i+1)+j;
            vn<<  0, 1;
            _CF.row(iCF)=cf;
            _n.row(iCF)=vn;
            iCF++;
        };

    for (int j = 0; j < ny-1; j++)
        {   Vector2i cf;
            Vector2d vn;
            int i=0;
            cf<<  ny* i   + j, ny*i+(j+1);
            vn<<  -1, 0;
            _CF.row(iCF)=cf;
            _n.row(iCF)=vn;
            iCF++;

            i= nx-1;
            cf<<  ny* i   + j, ny*i+(j+1);
            vn<<  1, 0;
            _CF.row(iCF)=cf;
            _n.row(iCF)=vn;
            iCF++;
        };
    Circumferences=_CF;
    normalVector=_n;
}

void QuadRectangleMesh::setInnerProductOfDerivative()
{
    MatrixXd C(4,4);
    C<< 4,-1,-2,-1,
        -1,4,-1,-2,
        -2,-1,4,-1,
        -1,-2,-1,4;
    MatrixXd _InnerProduct(N_nodes,N_nodes);
    _InnerProduct.setZero();
    for(int i=0; i<N_cell;i++)
    {
        _InnerProduct(Quads.row(i),Quads.row(i))+=C;
    };
    InnerProductOfDerivative=_InnerProduct;
}


void QuadRectangleMesh::getPositions(MatrixXd& _NodesPositions)
{   setPositions();
    _NodesPositions=Positions;
}

void QuadRectangleMesh::getQuads(MatrixXi&_Quads)
{
    _Quads=Quads;
}

void QuadRectangleMesh::getCircumferences(MatrixXi& _Circs)
{
    _Circs=Circumferences;
}
        
void QuadRectangleMesh::getNormalVector(MatrixXd& _Normal)
{
    _Normal=normalVector;
}

void QuadRectangleMesh::getEdges(MatrixXi& _Edges)
{
    _Edges=Edges;
}

void QuadRectangleMesh::getInnerProductOfDerivative(MatrixXd& _Inner)
{   
    setInnerProductOfDerivative();  
    _Inner=InnerProductOfDerivative;
}

class ArbitraryQuadMesh: public Mesh
{
    public:      

        int nx=2;//elements along the axis;
		int ny=2;
        double unit_length=1;
        SpMat OccupancyMatrix;
        vector<int> ActiveIndex;		       
        map<int,int> MapActive2All;
        map<int,int> MapAll2Active;

        void setPositions();
        void setQuads();
        void setCircumferences();// the boundary
        void setEdges();
		
		void getPositions(MatrixXd& _NodesPositions){_NodesPositions=Positions;};
        void getQuads(MatrixXi&_Quads){_Quads=Quads;};
        void getCircumferences(MatrixXi& _Circs){_Circs=Circumferences;};
        void getNormalVector(MatrixXd& _Normal){_Normal=normalVector;};
        void getEdges(MatrixXi& _Edges){_Edges=Edges;};


        ArbitraryQuadMesh(double _unit_length,SpMat Occupancy)//material setting should also follow sparse traversing order for the system construction
        {
            nx=Occupancy.rows();
            ny=Occupancy.cols();
            unit_length=_unit_length;
            OccupancyMatrix=Occupancy;
            cout<<"ArbitraryQuadMesh 0"<<endl;
            ADMM::getActiveIndex(ActiveIndex,Occupancy,MapActive2All,MapAll2Active);
            N_nodes=ActiveIndex.size();
            N_cell= Occupancy.nonZeros();
            N_edges= 4*N_cell;
            cout<<"ArbitraryQuadMesh 1"<<endl;
            setPositions();    
            cout<<"ArbitraryQuadMesh 2-1"<<endl;
            setQuads();   
            cout<<"ArbitraryQuadMesh 2"<<endl;
            setCircumferences();
            cout<<"ArbitraryQuadMesh 3"<<endl;
            setEdges();
            cout<<"ArbitraryQuadMesh 4"<<endl;

            ConnectivityMatrix=Quads;            
        };

    private:
        MatrixXd Positions;//to be modified
        MatrixXi Edges;
        MatrixXi Quads;
        MatrixXi Circumferences;
        MatrixXd normalVector;
        MatrixXd InnerProductOfDerivative;
        


};

void ArbitraryQuadMesh::setPositions()// not tested
{
    Vector2d shift;
    shift<< (nx-1)*unit_length/2.0,  (ny-1)*unit_length/2.0;
    MatrixXd _Positions(N_nodes,2);
    
    int count=0;
    for (int i = 0; i <= nx; i++)
        {
            for (int j = 0; j <=ny; j++) 
                { 
                    if ((ny+1)*i+j==ActiveIndex[count])
                    {
                        
                        Vector2d a;
                        a<<i*unit_length, j*unit_length;
                        a-=shift;
                        _Positions.row(count)= a.transpose();
                        Node* pN=  new Node(a);
                        v_pCompleteListOfNodes.push_back(pN);
                        count++;
                    }  
                };
        }; 

    Positions= _Positions;     
}
void ArbitraryQuadMesh::setQuads()
{
    MatrixXi _C(N_cell, 4);
    int iC=0;
    for (int k=0; k<OccupancyMatrix.outerSize(); ++k)
            {
                for (SparseMatrix<double>::InnerIterator it(OccupancyMatrix,k); it; ++it)
                {               
                    int i=it.row();   // row index
                    int j=it.col(); 
                    _C.row(iC)<<  MapAll2Active[(ny+1)* i   + j],
                                  MapAll2Active[(ny+1)*(i+1)+ j],
                                  MapAll2Active[(ny+1)*(i+1)+(j+1)],
                                  MapAll2Active[(ny+1)* i   +(j+1)];
                    iC++;   
                };

        };
    Quads=_C;
    

};

void ArbitraryQuadMesh::setEdges()
{
    
    set<set<int>> ssI;

    for(int i=0;i<Quads.rows();i++)
    {
        set<int> a0{Quads(i,0),Quads(i,1)};
        set<int> a1{Quads(i,1),Quads(i,2)};
        set<int> a2{Quads(i,2),Quads(i,3)};
        set<int> a3{Quads(i,3),Quads(i,0)};
        ssI.insert(a0);
        ssI.insert(a1);
        ssI.insert(a2);
        ssI.insert(a3);
    }
    MatrixXi _E(ssI.size(),2);
    int rowCount=0;
    for(auto s:ssI)
    {
        int colCount=0;
        for(auto its:s)
        {
            _E(rowCount,colCount)=its;
            colCount++;
        }           
        rowCount++;
    }
    
    Edges=_E;
}
void ArbitraryQuadMesh::setCircumferences()
{
    vector<RowVector2i> vR;
    vector<RowVector2d> vN;
    cout<<"ArbitraryQuadMesh::setCircumferences(0)"<<endl;
    for (int k=0; k<OccupancyMatrix.outerSize(); ++k)
    {
        for (SparseMatrix<double>::InnerIterator it(OccupancyMatrix,k); it; ++it)
        {
            
            int x=it.row();
            int y=it.col();
            
            cout<<"x  "<<x <<"y  "<<y<<endl;
            //xplus
            if (x+1==nx||OccupancyMatrix.coeff(x+1,y)==0)
            {
                RowVector2i a;
                a<< MapAll2Active[(ny+1)*(x+1)+y], MapAll2Active[(ny+1)*(x+1)+y+1];
                vR.push_back(a);
                cout<<"ArbitraryQuadMesh::setCircumferences(2)"<<endl;
                RowVector2d n;
                n<<1,0;
                vN.push_back(n);
            }
            //xminus
            if (x==0||OccupancyMatrix.coeff(x-1,y)==0)
            {
                RowVector2i a;
                cout<<"ArbitraryQuadMesh::setCircumferences(30)"<<endl;
                a<< MapAll2Active[(ny+1)*(x)+y], MapAll2Active[(ny+1)*(x)+y+1];
                cout<<"ArbitraryQuadMesh::setCircumferences(31)"<<endl;
                vR.push_back(a);
                cout<<"ArbitraryQuadMesh::setCircumferences(32)"<<endl;
                RowVector2d n;
                n<<-1,0;
                vN.push_back(n);
                cout<<"ArbitraryQuadMesh::setCircumferences(3)"<<endl;
            }
            //yplus
            if (y+1==ny||OccupancyMatrix.coeff(x,y+1)==0)
            {
                RowVector2i a;
                a<< MapAll2Active[(ny+1)*(x)+y+1], MapAll2Active[(ny+1)*(x+1)+y+1];
                vR.push_back(a);
                cout<<"ArbitraryQuadMesh::setCircumferences(4)"<<endl;
                RowVector2d n;
                n<<0,1;
                vN.push_back(n);
            }
            //yminus
            if (y==0||OccupancyMatrix.coeff(x,y-1)==0)
            {
                RowVector2i a;
                a<< MapAll2Active[(ny+1)*(x)+y], MapAll2Active[(ny+1)*(x+1)+y];
                vR.push_back(a);
                RowVector2d n;
                n<<0,-1;
                cout<<"ArbitraryQuadMesh::setCircumferences(5)"<<endl;
                vN.push_back(n);
            }
        }
    }
    Circumferences.resize(vR.size(),2);
    normalVector.resize(vN.size(),2);
    cout<<"ArbitraryQuadMesh::setCircumferences(6)"<<endl;
    for(int i=0;i<vR.size();i++)
    {
        Circumferences.row(i)=vR[i];
        cout<<"ArbitraryQuadMesh::setCircumferences(7)"<<endl;
        normalVector.row(i)=vN[i];
    }


}



