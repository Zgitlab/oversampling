#pragma once
#include <Eigen/Core>
#include <iostream>
#include <mains/2DFEM/Material.hpp>

class Material_StVK: public Material
{
            
    public:
        Material_StVK() {};
        void getEnergyDensity(Matrix2d F, double& e) override;//F deformation gradient
        void getPiolaStress(Matrix2d F, Matrix2d& P) override;// the first Piola Kirchhoff stress
        void getSecondOrderDerivativeOfEnergy(Matrix2d F, MatrixXd& S) override;//9*9 Matrix

    private:
        


};
void Material_StVK::getEnergyDensity(Matrix2d F, double& e)
{   using namespace std;

    Matrix2d I2;
    I2.setIdentity();
    Matrix2d E= 0.5*(F.transpose()*F-I2);
    
    //https://en.wikipedia.org/wiki/Hyperelastic_material
    e= 0.5*Lame1*E.trace()*E.trace()+ Lame2* ((E*E).trace());

}
void Material_StVK::getPiolaStress(Matrix2d F, Matrix2d& P)
{
    Matrix2d I2;
    I2.setIdentity();
    Matrix2d E= 0.5*(F.transpose()*F-I2);
    P=(Lame1* E.trace())*F+ 2* Lame2*F* E;

}
void Material_StVK::getSecondOrderDerivativeOfEnergy(Matrix2d F, MatrixXd& S)
{
    MatrixXd _S(4,4);
    double F11=F(0,0);
    double F12=F(0,1);
    double F21=F(1,0);
    double F22=F(1,1);
    double P11=F11*F11;
    double P12=F12*F12;
    double P21=F21*F21;
    double P22=F22*F22;

    _S<< 
    P11*Lame1 + ((-1 + P11 + P21)/2. + (-1 + P12 + P22)/2.)*Lame1 + (-1 + 3*P11 + P12 + P21)*Lame2,
    F11*F12*Lame1 + (2*F11*F12 + F21*F22)*Lame2,
    F11*F21*Lame1 + (2*F11*F21 + F12*F22)*Lame2,
    F11*F22*Lame1 + F12*F21*Lame2,
    F11*F12*Lame1 + (2*F11*F12 + F21*F22)*Lame2,
    P12*Lame1 + ((-1 + P11 + P21)/2. + (-1 + P12 + P22)/2.)*Lame1 + (-1 + P11 + 3*P12 + P22)*Lame2,
    F12*F21*Lame1 + F11*F22*Lame2,
    F12*F22*Lame1 + (F11*F21 + 2*F12*F22)*Lame2,
    F11*F21*Lame1 + (2*F11*F21 + F12*F22)*Lame2,
    F12*F21*Lame1 + F11*F22*Lame2,
    P21*Lame1 + ((-1 + P11 + P21)/2. + (-1 + P12 + P22)/2.)*Lame1 + (-1 + P11 + 3*P21 + P22)*Lame2,
    F21*F22*Lame1 + (F11*F12 + 2*F21*F22)*Lame2,
    F11*F22*Lame1 + F12*F21*Lame2,
    F12*F22*Lame1 + (F11*F21 + 2*F12*F22)*Lame2,
    F21*F22*Lame1 + (F11*F12 + 2*F21*F22)*Lame2,
    P22*Lame1 + ((-1 + P11 + P21)/2. +  (-1 + P12 + P22)/2.)*Lame1 + (-1 + P12 + P21 + 3*P22)*Lame2;

    S=_S;
  
}