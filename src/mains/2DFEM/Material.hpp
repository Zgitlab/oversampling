#pragma once
#include <Eigen/Sparse>
#include <iostream>

using namespace std;
using namespace Eigen;

class Material
{   
    public:
        virtual void getEnergyDensity(Matrix2d F, double& e) = 0;//F deformation gradient
        virtual void getPiolaStress(Matrix2d F, Matrix2d& P) = 0;
        virtual void getSecondOrderDerivativeOfEnergy(Matrix2d F, MatrixXd& S)=0;//4*4 Matrix
        void setMaterialFromYP(double _YoungsModulus, double _PoissonRatio, double _Density);

        double Density=0;
        double YoungsModulus=0;
        
        double PoissonRatio=0;
        double Lame1=0;
        double Lame2=0;

};

void Material::setMaterialFromYP(double _YoungsModulus, double _PoissonRatio, double _Density)
{
    YoungsModulus= _YoungsModulus;
    PoissonRatio = _PoissonRatio;
    Lame1= (_YoungsModulus * _PoissonRatio)/((1+_PoissonRatio)*(1-2*_PoissonRatio));
    Lame2= _YoungsModulus/(2*(1+_PoissonRatio));
    Density= _Density;    
}