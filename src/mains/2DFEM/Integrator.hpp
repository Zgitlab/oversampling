#pragma once
#include <Eigen/Sparse>
#include <mains/2DFEM/System.hpp>
#include <mains/2DFEM/Boundary.hpp>

class Integrator
{
    public:
        Integrator(System* _pSystem);
        void ImplicitStep();

        void NewtonStatic();
        void NewtonStaticInvariant(double& _invariant);
        
        void NewtonLineSearch();
        void SingleNewtonStep(VectorXd xn,VectorXd x0,VectorXd v0,VectorXd f_ext, VectorXd& dx);
        void NewtonInvariant(VectorXd xn, VectorXd x0,VectorXd v0,VectorXd& f_ext, double &_invariant);

        void setBC();
        vector<Boundary*> v_pBoundaryConditions;

    private:
        System* pSystem;
        VectorXd RHS;
        SpMat LHS;
        double dt=5e-2; // time step

};

Integrator::Integrator(System* _pSystem)
{
    pSystem= _pSystem;
}

void Integrator::setBC()// only for implicit step
{   
    SpMat M;
    pSystem->getMass(M);
    // Management of the Boundary condition
    for (int i=0; i<v_pBoundaryConditions.size();i++)
    {
       auto pBC=v_pBoundaryConditions[i];
       if (pBC->Type=='G')// Gravity
            {
                double g =pBC->valueDOF[0];
                Vector2d vg;
                vg<<0.0,g;
                VectorXd VG=vg.replicate(pSystem->n_Nodes,1);
                RHS+= dt* M* VG;
            }
        else if (pBC->Type=='D')// Dirichlet Boundary Condition
            {   
                auto valueDOF=pBC->valueDOF;//todo
                auto vFixIndices=pBC->indexDOF;
                auto vStencil=pBC->vStencil;
                VectorXd vzero;
                vzero.resize(vFixIndices.size());
                vzero.setZero();
                RHS(vFixIndices)=vzero;
                for (int k = 0; k < LHS.outerSize(); ++k)
                {
                    for (SpMat::InnerIterator it(LHS, k); it; ++it)
                    {
                        if (vStencil[it.row()] || vStencil[it.col()])
                        {
                            if (it.row() == it.col())
                            {
                                it.valueRef() = 1.0;
                            }    
                            else
                            {
                                it.valueRef() = 0.0;
                            }; 
                        };
                    };                
                }       
            }

    };
}

void Integrator::ImplicitStep()
{   
    SpMat dfdx,M;
    pSystem->getHessian(dfdx);
    pSystem->getMass(M);
    LHS=M- dt*dt* dfdx;
    
    VectorXd f0,v0,v1,x0,x1;
    pSystem->getForce(f0);
    pSystem->getVelocity(v0);
    pSystem->getPosition(x0);
    RHS= dt*f0+dt*dt*dfdx*v0;
    
    setBC();

	Eigen::SimplicialLDLT<SpMat> ldlt(LHS);
	Eigen::VectorXd dv = ldlt.solve(RHS);


	v1= v0+dv;
	x1= x0+ dt* v1;

    pSystem->setPosition(x1);
    pSystem->setVelocity(v1);
} 
void Integrator::NewtonStatic()
{
    double Invariant, Invariant0;
    VectorXd x0,x1,f_ext,f_int,dx;
    SpMat H,M;
    double step_size=1.0;
    pSystem->getPosition(x0);
    pSystem->getHessian(H);
    pSystem->getForce(f_int);
    pSystem->getMass(M);
    
    
    f_ext.resize(pSystem->n_DOF);
    for (int i=0; i<v_pBoundaryConditions.size();i++)
    {   
       auto pBC=v_pBoundaryConditions[i];
       if (pBC->Type=='G')// Gravity
            {
                double g =pBC->valueDOF[0];
                Vector2d vg;
                vg<<0.0,g;
                VectorXd VG=vg.replicate(pSystem->n_Nodes,1);
                f_ext+= M* VG;
            }
    }

    RHS= -f_int-f_ext;

    LHS= -H;

    for (int i=0; i<v_pBoundaryConditions.size();i++)
    {
        auto pBC=v_pBoundaryConditions[i];
        if (pBC->Type=='D')// Dirichlet Boundary Condition
                {   
                    auto valueDOF=pBC->valueDOF;//todo
                    auto vFixIndices=pBC->indexDOF;
                    auto vStencil=pBC->vStencil;
                    VectorXd vzero;
                    vzero.resize(vFixIndices.size());
                    vzero.setZero();
                    RHS(vFixIndices)=vzero;
                    for (int k = 0; k < LHS.outerSize(); ++k)
                    {
                        for (SpMat::InnerIterator it(LHS, k); it; ++it)
                        {
                            if (vStencil[it.row()] || vStencil[it.col()])
                            {
                                if (it.row() == it.col())
                                {
                                    it.valueRef() = 1.0;
                                }    
                                else
                                {
                                    it.valueRef() = 0.0;
                                }; 
                            };
                        };                
                    }       
                }
    }
    Eigen::SimplicialLDLT<SpMat> LDLT(LHS);
	dx = LDLT.solve(RHS);  
    //cout<<"dx"<<dx<<endl;
   
    double U0,U1;    
    NewtonStaticInvariant(U0);
    for(int i=0; i<20;i++)
    {
        x1=x0+step_size*dx;
        pSystem->setPosition(x1);
        NewtonStaticInvariant(U1);
        //cout<<"Invariant difference  "<<U1-U0<<endl;
        if(U1>U0) {step_size*=0.5;}
        else{ break;
            };

    }
}

void Integrator::NewtonStaticInvariant(double& _invariant)
{   VectorXd x;
    SpMat M;
    pSystem->getMass(M);
    VectorXd f_ext;
    for (int i=0; i<v_pBoundaryConditions.size();i++)
    {
       auto pBC=v_pBoundaryConditions[i];
       if (pBC->Type=='G')// Gravity
            {
                double g =pBC->valueDOF[0];
                Vector2d vg;
                vg<<0.0,g;
                VectorXd VG=vg.replicate(pSystem->n_Nodes,1);
                f_ext+= M* VG;
            }
    }
    double U;
    pSystem->getPosition(x);
    pSystem->getEnergy(U);
    _invariant=U-x.dot(f_ext);


}

void Integrator::NewtonLineSearch()// set position of system here
{   

    double Invariant, Invariant0;
    VectorXd v0,v1,x0,x1,f_int, f_ext, xn, xn1,dx;


    double step_size=1.0;

    pSystem->getVelocity(v0);
    pSystem->getPosition(x0);

    xn=x0;

    NewtonInvariant(x0,x0,v0,f_ext,Invariant0);
    //cout<<"f_ext"<< f_ext<<endl;
    for(int i=0; i<100; i++)
    {   
        //cout<<"Integrator::NewtonLineSearch(4)"<<endl;
        SingleNewtonStep(xn,x0,v0,f_ext,dx);
        xn1= xn+step_size* dx;
        //cout<<"Integrator::NewtonLineSearch(5)"<<endl;
        pSystem->setPosition(xn1);
        NewtonInvariant(xn1,x0,v0,f_ext,Invariant);
        //cout<<"Integrator::NewtonLineSearch(6)"<<endl;
        //cout<<"Invariant Change: "<< Invariant- Invariant0<<endl;
        if ((Invariant>Invariant0)) 
            {
             step_size*=0.5;
             pSystem->setPosition(xn);
            }
        else {xn=xn1;
             step_size=1.0;
             Invariant0=Invariant;
             };
    }

    v1= (xn-x0)/dt;
    x1=xn;
    pSystem->setPosition(x1);
    pSystem->setVelocity(v1);


}
void Integrator::NewtonInvariant(VectorXd xn, VectorXd x0,VectorXd v0,VectorXd& f_ext, double &_invariant)// do not touch the position of system
{   SpMat M;
    f_ext.resize(pSystem->n_DOF);
    pSystem->getMass(M);
    for (int i=0; i<v_pBoundaryConditions.size();i++)
    {
       auto pBC=v_pBoundaryConditions[i];
       if (pBC->Type=='G')// Gravity
            {
                double g =pBC->valueDOF[0];
                Vector2d vg;
                vg<<0.0,g;
                VectorXd VG=vg.replicate(pSystem->n_Nodes,1);
                f_ext+= M* VG;
            }
    }

    double U;
    pSystem->getEnergy(U);
    _invariant= 0.5*xn.dot(M*xn)- xn.dot(M*(x0+ v0*dt))+dt*dt*U-dt*dt*f_ext.dot(xn);

}

void Integrator::SingleNewtonStep(VectorXd xn,VectorXd x0,VectorXd v0,VectorXd f_ext,VectorXd& dx )// do not touch the position of the system
{   
    

    SpMat dfdx,M;
    pSystem->getHessian(dfdx);//dfdx at the new hypothetical position xn
    pSystem->getMass(M);

   
    VectorXd Jacobian;
    Jacobian.resize(pSystem->n_DOF);
    SpMat Hessian;
    Hessian.resize(pSystem->n_DOF, pSystem->n_DOF);

    VectorXd f_int;
    pSystem->getForce(f_int);
   

    //cout<<"Integrator::SingleNewtonStep0"<<endl;

    Jacobian=M*(xn-x0-v0*dt)-dt*dt*(f_int+f_ext);
    //cout<<"xn"<<xn<<"\n x0"<<x0<<"\n v0 " <<v0<<"\n f_int"<<f_int<<"\n f_ext"<<f_ext<<endl;
    //cout<<"Jacobian"<<Jacobian<<endl;
    Hessian=M-dt*dt*dfdx;
    //cout<<"Integrator::SingleNewtonStep1"<<endl;
    LHS=Hessian;
    RHS=-Jacobian;
    
    //cout<<"Integrator::SingleNewtonStep2"<<endl;
    
    for (int i=0; i<v_pBoundaryConditions.size();i++)
    {
        auto pBC=v_pBoundaryConditions[i];
        if (pBC->Type=='D')// Dirichlet Boundary Condition
                {   
                    auto valueDOF=pBC->valueDOF;//todo
                    auto vFixIndices=pBC->indexDOF;
                    auto vStencil=pBC->vStencil;
                     //cout<<"Integrator::SingleNewtonStep2.1"<<endl;
                    VectorXd vzero;
                    vzero.resize(vFixIndices.size());
                    vzero.setZero();

                    //cout<<"RHS"<<RHS<<endl;
                    RHS(vFixIndices)=vzero;
                    
                    //cout<<"Integrator::SingleNewtonStep2.2"<<endl;
                    for (int k = 0; k < LHS.outerSize(); ++k)
                    {
                        for (SpMat::InnerIterator it(LHS, k); it; ++it)
                        {
                            if (vStencil[it.row()] || vStencil[it.col()])
                            {
                                if (it.row() == it.col())
                                {
                                    it.valueRef() = 1.0;
                                }    
                                else
                                {
                                    it.valueRef() = 0.0;
                                }; 
                            };
                        };                
                    }       
                }
    }
    //cout<<"Integrator::SingleNewtonStep3"<<endl;
    Eigen::SimplicialLDLT<SpMat> LDLT(LHS);
    //cout<<"Integrator::SingleNewtonStep4"<<endl;
	dx = LDLT.solve(RHS);  

};
