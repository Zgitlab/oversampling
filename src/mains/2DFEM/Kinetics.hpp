#pragma once
#include <Eigen/Core>
#include <iostream>
#include <mains/2DFEM/Material.hpp>
#include <mains/2DFEM/Quadrilateral.hpp>
using namespace std;
using namespace Eigen;
typedef Eigen::Triplet<double> T;

class Kinetics
{
    public:

        Kinetics( Material* _pMaterial, Quadrilateral* _pQuad);


        void setEnergy();
        void setForce();
        void setHessian();
        void setMass();

        void getEnergy(double& e);
        void getForce(VectorXd& vf); 
        void getHessian(MatrixXd& H);
        void getMass(double& m);

        

    
    private:
        Material* pMaterial;
        Quadrilateral* pQuad;

        double Energy;
        double Mass;    
        VectorXd vForce;
        MatrixXd Hessian;

};

Kinetics::Kinetics( Material* _pMaterial, Quadrilateral* _pQuad)
{
    cout<<"in kinetics 1"<<endl;
    pMaterial=_pMaterial;

    pQuad=_pQuad;
    cout<<"in kinetics 2"<<endl;
    setEnergy();
    cout<<"in kinetics 3"<<endl;
    setForce();
    setHessian();
    cout<<"in kinetics 4"<<endl;
    setMass();


};

void Kinetics::setMass()
{
    double rho= pMaterial->Density;

    double A;
    pQuad->getArea(A);
    Mass=rho*A;

};

void Kinetics::setEnergy()
{
    vector<Matrix2d> vF;
    cout<<"Kinetics::setEnergy(1)"<<endl;

    pQuad->getvDeformationGradient(vF);
    double ED=0;
    cout<<"Kinetics::setEnergy(2)"<<endl;
    for (int i=0;i< vF.size();i++)
    {   
        cout<<"Kinetics::setEnergy(2.1)"<<endl;
        double ed;
        cout<<"Kinetics::setEnergy(2.2)"<<endl;
        cout<<vF[i]<<endl;
        
        pMaterial->getEnergyDensity(vF[i],ed);
        ED+=ed/vF.size();
    }
    cout<<"Kinetics::setEnergy(3)"<<endl;
    double Area;
    pQuad->getArea(Area);

    Energy= Area* ED;
    
}

void Kinetics::setForce()
{
    vector<Matrix2d> vF;
    vector<MatrixXd> vB;
    pQuad->getvDeformationGradient(vF);
    pQuad->getvBlockB(vB);

    double Area;
    pQuad->getArea(Area);

    VectorXd _Force(8);//not general
    _Force.setZero();

    for (int i=0;i< vF.size();i++)
    {
        Matrix2d F,P;
        F=vF[i];
     
        pMaterial->getPiolaStress(F,P);
   
        VectorXd VecP(Map<VectorXd>(P.data(),P.size()));
        _Force-=0.25*Area*vB[i].transpose()*VecP;//0.25 quadrature weight
        
      
    }
    vForce=_Force;
}

void Kinetics::setHessian()
{   
    vector<Matrix2d> vF;
    vector<MatrixXd> vB;
    pQuad->getvDeformationGradient(vF);
    pQuad->getvBlockB(vB);

    MatrixXd _Hessian(8,8);
    _Hessian.setZero();
    double Area;
    pQuad->getArea(Area);
    for(int i=0;i<vF.size();i++)
    {   
        MatrixXd S;
        pMaterial->getSecondOrderDerivativeOfEnergy(vF[i],S);
        _Hessian+= -Area* vB[i].transpose()* S * vB[i];
    }
    Hessian=_Hessian;


}

void Kinetics::getMass(double& m)
{   //setMass(); consider mass is a constant
    m= Mass;
}

void Kinetics::getEnergy(double& e)
{   setEnergy();
    e= Energy;
}

void Kinetics::getForce(VectorXd& vf)
{   setForce();
    vf=vForce;
}

void Kinetics::getHessian(MatrixXd& H)
{   setHessian();
    H=Hessian;
}