#pragma once
#include <Eigen/Sparse>
#include <iostream>
using namespace std;
using namespace Eigen;

class Boundary
{
    public: 
        vector<int> indexDOF;
        vector<bool> vStencil;
        vector<double> valueDOF;
        bool isSet=false;
        char Type;


};