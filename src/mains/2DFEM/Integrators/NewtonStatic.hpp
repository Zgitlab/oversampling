#pragma once
#include <Eigen/Sparse>
#include <mains/2DFEM/System.hpp>
#include <mains/2DFEM/Boundary.hpp>

class NewtonStaticSolver
{
    public:
        NewtonStaticSolver(System* _pSystem);
        vector<Boundary*> v_pBoundaryConditions;
        SpMat LHS;
        VectorXd RHS;
        VectorXd F_ext;
        void OptimisationQuantity(double& _U);
        void SingleStep();
        void setLHS();
        void setRHS();
        void setF_ext();
    private:
        System* pSystem;


};

NewtonStaticSolver::NewtonStaticSolver(System* _pSystem)
{
    pSystem=_pSystem;
  
}

void NewtonStaticSolver::setF_ext()
{
    F_ext.resize(pSystem->n_DOF);
    SpMat M;
    pSystem->getMass(M);
    cout<<"v_pBoundaryConditions.size()"<<v_pBoundaryConditions.size()<<endl;
    for (int i=0; i<v_pBoundaryConditions.size();i++)
    {
       auto pBC=v_pBoundaryConditions[i];
       if (pBC->Type=='G')// Gravity
            {
                double g =pBC->valueDOF[0];
                Vector2d vg;
                vg<<0.0,g;
                VectorXd VG=vg.replicate(pSystem->n_Nodes,1);
                cout<<"VG"<< VG<<endl;
                F_ext+= M*VG;
                cout<<"F ext"<< F_ext<<endl;
            };
    };

}

void NewtonStaticSolver::setLHS()
{   
    MatrixXd L;
    VectorXd R;
    SpMat dfdx;
    pSystem->FiniteDifferenceMethod(R,L);
    pSystem->getHessian(dfdx);
    LHS=-dfdx;
    cout<<"H1"<<L<<"\n H2"<<LHS<<endl;

    for (int i=0; i<v_pBoundaryConditions.size();i++)
    {
        auto pBC=v_pBoundaryConditions[i];
        if (pBC->Type=='D')// Dirichlet Boundary Condition
                {   
                    auto vFixIndices=pBC->indexDOF;
                    auto vStencil=pBC->vStencil;
                    for (int k = 0; k < LHS.outerSize(); ++k)
                    {
                        for (SpMat::InnerIterator it(LHS, k); it; ++it)
                        {
                            if (vStencil[it.row()] || vStencil[it.col()])
                            {
                                if (it.row() == it.col())
                                {
                                    it.valueRef() = 1.0;
                                }    
                                else
                                {
                                    it.valueRef() = 0.0;
                                }; 
                            };
                        };                
                    }       
                }
    }
}

void NewtonStaticSolver::setRHS()
{   
    setF_ext();
    VectorXd f;
    MatrixXd L;
    VectorXd R;
    pSystem->FiniteDifferenceMethod(R,L);
    
    pSystem->getForce(f);
    cout<<"f1"<<R<<"\n f2"<<f<<endl;
    cout<<"external force"<<F_ext<<endl;
    RHS=f+F_ext;
    cout<<"total force"<<RHS<<endl;
    for (int i=0; i<v_pBoundaryConditions.size();i++)
    {
        auto pBC=v_pBoundaryConditions[i];
        if (pBC->Type=='D')// Dirichlet Boundary Condition
                {   
                    auto vFixIndices=pBC->indexDOF;
                    VectorXd vzero;
                    vzero.resize(vFixIndices.size());
                    vzero.setZero();
                    RHS(vFixIndices)=vzero;
                }
    };
}


void NewtonStaticSolver::OptimisationQuantity(double& _U)
{
    pSystem->getEnergy(_U);
    cout<<_U<<"Internal Energy"<<endl;
    VectorXd x;
    pSystem->getPosition(x);
    _U-= F_ext.dot(x);
}

void NewtonStaticSolver::SingleStep()
{   
    setLHS();
    setRHS();
    double step_size=1.0;
    Eigen::SimplicialLDLT<SpMat> LDLT(LHS);
	VectorXd dx = LDLT.solve(RHS);
    cout<<"dx"<< dx<<endl;
    VectorXd x;

    for(int i=0; i<20; i++)
    {   
        cout<<"round"<<i<<endl;
        cout<<"stepsize"<<step_size<<endl;  
        pSystem->getPosition(x);
        double U0;
        OptimisationQuantity(U0);
        VectorXd x1= x+step_size*dx;
        pSystem->setPosition(x1);
        double U1;
        cout<<"U0  "<<U0<<"\n U1  "<<U1<<endl;
        OptimisationQuantity(U1);
        if(U0<U1)
        {
            step_size*=0.5;
            pSystem->setPosition(x);
        }
        else
        {   
            cout<<"find a good one"<<U1<<endl;
            x=x1;
            break;
        };       

    }



    
};