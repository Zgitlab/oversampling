#pragma once
#include <Eigen/Sparse>
#include <mains/2DFEM/System.hpp>
#include <mains/2DFEM/Boundary.hpp>

class NB
{
    public:
        NB(System* _pSystem);
        void SingleStep();

        void setBC();
        void Energy(double& U);
        vector<Boundary*> v_pBoundaryConditions;
        VectorXd MG;

    private:
        System* pSystem;
        VectorXd RHS;
        SpMat LHS;
        double dt=5e-2; // time step

};

NB::NB(System* _pSystem)
{
    pSystem= _pSystem;
}

void NB::setBC()
{   
    cout<<"setBC(0)"<<endl;
    SpMat M;
    pSystem->getMass(M);
    cout<<"setBC(1)"<<endl;
    // Management of the Boundary condition
    for (int i=0; i<v_pBoundaryConditions.size();i++)
    {
       auto pBC=v_pBoundaryConditions[i];
       if (pBC->Type=='G')// Gravity
            {
                double g =pBC->valueDOF[0];
                Vector2d vg;
                vg<<0.0,g;
                VectorXd VG=vg.replicate(pSystem->n_Nodes,1);
                MG=M*VG;
                RHS-= M* VG;
                cout<<"setBC(2)"<<endl;
            }
        else if (pBC->Type=='N')
            {

                if (pBC->indexDOF.size()==pBC->valueDOF.size())
                {
                    VectorXd NForce(pBC->indexDOF.size());
                    for(int i=0;i<pBC->indexDOF.size();i++)
                    {
                        NForce(i)=pBC->valueDOF[i];
                    }
                    RHS(pBC->indexDOF)-=NForce;
                    MG(pBC->indexDOF)+=NForce;
                }              

            }
        else if (pBC->Type=='D')// Dirichlet Boundary Condition
            {   
                auto valueDOF=pBC->valueDOF;//todo
                auto vFixIndices=pBC->indexDOF;
                auto vStencil=pBC->vStencil;
                cout<<"vStencil"<<vStencil[4]<<endl;
                VectorXd vzero;
                vzero.resize(vFixIndices.size());
                vzero.setZero();
                RHS(vFixIndices)=vzero;
                cout<<"setBC(3)"<<endl;
              
                for (int k = 0; k < LHS.outerSize(); ++k)
                {
                    for (SpMat::InnerIterator it(LHS, k); it; ++it)
                    {
                        if (vStencil[it.row()] || vStencil[it.col()])
                        {
                            if (it.row() == it.col())
                            {
                                it.valueRef() = 1.0;
                            }    
                            else
                            {
                                it.valueRef() = 0.0;
                            }; 
                        };
                    };                
                }       
            }

    };
}

void NB::SingleStep()
{   
    SpMat dfdx,M;
    pSystem->getHessian(dfdx);
    pSystem->getMass(M);
    LHS=-dfdx;
    
    VectorXd f0,x0,x1;
    double U0,U1,step_size;
    step_size=1.0;

    pSystem->getForce(f0);
    pSystem->getPosition(x0);
   
    RHS=f0;
    cout<<"singleStep(0)"<<endl;
    setBC();
    cout<<"singleStep(0.5)"<<endl;
    Energy(U0);
    cout<<"singleStep(1)"<<endl;
	Eigen::SimplicialLDLT<SpMat> ldlt(LHS);
    cout<<"singleStep(2)"<<endl;
	Eigen::VectorXd dx = ldlt.solve(RHS);

    for(int i=0;i<20;i++)
    {
        x1= x0- step_size* dx;
        pSystem->setPosition(x1);
        Energy(U1);
        if(U0>U1)
        {   
            cout<<"round "<<i<<"\n U0   "<<U0<<"\n U1   "<<U1<<"\n Good one!"<<endl;
            break;
        }
        else
        {   
            cout<<"round "<<i<<"\n U0   " <<U0<<"\n U1   "<<U1<<endl;
            step_size*=0.5;
        }

    }

} 

void NB::Energy(double& U)
{
    pSystem->getEnergy(U);
    cout<<"Elastic Energy  "<<U<<endl;
    VectorXd x;
    pSystem->getPosition(x);
    cout<<"Gravitational Energy"<<MG.dot(x)<<endl;
    U-=MG.dot(x);


};



