#pragma once
#include <Eigen/Eigen>
#include <iostream>
#include <mains/2DFEM/Node.hpp>
using namespace std;
using namespace Eigen;

class Quadrilateral
{   
    public:
        Quadrilateral(vector<Node*> _v_pNodes);
        void setArea();
        void setQuadrature();//default setting of quadrature
        void setB();

        void getArea(double &a);
        void getvDeformationGradient(vector<Matrix2d>& vF);
        void getvBlockB(vector<MatrixXd>& _vBlockB);

    private:
        static const int n_Nodes=4;
        vector<Node*> v_pNodes;    
        double Area;


        // Quadrature related
        int n_Quadrature;
        vector<Vector2d> vQuadraturePoints;
        vector<double> vQuadratrureWeights;
        vector<MatrixXd> vB; // vector of pre-computed B matrix;
        vector<MatrixXd> vBlockB;
        MatrixXd ParametricCoordinatesForFourNodes;
};




Quadrilateral::Quadrilateral(vector<Node*> _v_pNodes)
{   
    cout<<"Quad 1"<<endl;
    v_pNodes=_v_pNodes;

    MatrixXd P(4,2);
 
    P<< -1,-1,
         1,-1,
         1, 1,
        -1, 1;

    ParametricCoordinatesForFourNodes=P;
    cout<<"Quad2"<<endl;
    setArea();
    cout<<"Quad3"<<endl;

    setQuadrature();
    cout<<"Quad4"<<endl;
    setB();



}


void Quadrilateral::setArea()
{   
    Vector2d x0 =v_pNodes[0]->getInitialPosition();
    Vector2d x1 =v_pNodes[1]->getInitialPosition();
    Vector2d x2 =v_pNodes[2]->getInitialPosition();
    Vector2d x3 =v_pNodes[3]->getInitialPosition();
    cout<<"x0  "<< x0<<endl;
    cout<<"x1  "<< x1<<endl;
    cout<<"x2  "<< x2<<endl;
    cout<<"x3  "<< x3<<endl;
    Vector2d a= (x1-x0);
    Vector2d b= (x3-x0);
    Vector2d c= (x3-x2);
    Vector2d d= (x1-x2);

    Area= 0.5* abs(a(0)*b(1)-a(1)*b(0))+ 0.5* abs(c(0)*d(1)-c(1)*d(0)) ;
}

void Quadrilateral::setQuadrature()
{   
    n_Quadrature=4;
    for(int i=0; i<n_Quadrature;i++)
    {
        Vector2d v= 0.57735026919* ParametricCoordinatesForFourNodes.row(i);//1/(sqrt(3))
        vQuadraturePoints.push_back(v);
        vQuadratrureWeights.push_back(0.25);
    }
    

}

void Quadrilateral::setB()
{ 
    MatrixXd dNdp(4,2);
    MatrixXd N_X(2,4);

    for (int i=0; i<n_Nodes;i++)
    {
        N_X.col(i)=v_pNodes[i]->getInitialPosition();

    }

    MatrixXd P= ParametricCoordinatesForFourNodes;

    for(int i=0; i<n_Quadrature;i++)
    {
        Vector2d v=vQuadraturePoints[i];
        dNdp<< P(0,0)*P(0,1)*v(1), P(0,0)*P(0,1)*v(0),
               P(1,0)*P(1,1)*v(1), P(1,0)*P(1,1)*v(0),
               P(2,0)*P(2,1)*v(1), P(2,0)*P(2,1)*v(0),
               P(3,0)*P(3,1)*v(1), P(3,0)*P(3,1)*v(0);
        
        dNdp+=P;
      
        dNdp*=0.25;
        MatrixXd B;
        Matrix2d dXdp;
        dXdp= N_X*dNdp;
    
        B=dNdp*(dXdp.inverse());
      
        vB.push_back(B);

        MatrixXd BlockB(4,8);
        BlockB.setZero();
        for(int ii=0;ii<2;ii++)
        {
            for(int j=0;j<2;j++)
            {   
                for(int k=0;k<4;k++)
                {
                    BlockB(ii+2*j,2*k+ii)=B(k,j);
                }                
            };
        };
        vBlockB.push_back(BlockB);
    }

};

void Quadrilateral::getArea(double &a)
{
    a=Area;
}

void Quadrilateral::getvDeformationGradient(vector<Matrix2d>& vF)
{
    vector<Matrix2d> _vf;
    MatrixXd N_x(2,4);
    for (int i=0; i<n_Nodes;i++)
    {
        N_x.col(i)=v_pNodes[i]->getPosition();
    }
    for (int i=0; i<n_Quadrature;i++)
    {
        _vf.push_back(N_x*vB[i]);
    }

    vF=_vf;
    
}

void Quadrilateral::getvBlockB(vector<MatrixXd>& _vBlockB)
{
    _vBlockB=vBlockB;
}