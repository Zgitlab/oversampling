
#pragma once
#include <Eigen/Sparse>
#include <iostream>
using namespace std;
using namespace Eigen;

class Node {
public:

	Node(Vector2d v);

	Vector2d getPosition();
	void setPosition(Vector2d pos);

	Vector2d getInitialPosition();
	void setInitialPosition(Vector2d pos);

	Vector2d getVelocity();
	void setVelocity(Vector2d Vel);

private:
	Vector2d position;
	Vector2d velocity;
	Vector2d InitialPosition;

};

Node::Node(Vector2d v)
{
	position=v; 
	InitialPosition=v; 
	velocity.setZero();
}

void Node::setPosition(Vector2d pos)
{
	position= pos;
};


Vector2d Node::getPosition()
{
	return position;
};


void Node::setInitialPosition(Vector2d pos)
{
	InitialPosition = pos;
};

Vector2d Node::getInitialPosition()
{
	return InitialPosition;
};



Vector2d Node::getVelocity()
{
	return velocity;
};


void Node::setVelocity(Vector2d vel)
{
	velocity=vel;
};