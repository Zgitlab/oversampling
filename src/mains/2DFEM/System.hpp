#pragma once
#include <Eigen/Sparse>
#include <iostream>
#include <mains/2DFEM/Node.hpp>
#include <mains/2DFEM/Mesh.hpp>
#include <mains/2DFEM/Kinetics.hpp>

using namespace std;
using namespace Eigen;


typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> T;


class System
{
    public:

        System(Mesh* _pMesh, Material* _Material);
        System(Mesh* _pMesh,vector<Material*> _vMaterial);
        void AssembleEnergy();
        void AssembleForce();
        void AssembleHessianAndMass();

        void AssemblePosition();
        void setPosition(VectorXd _p);

        void AssembleVelocity();
        void setVelocity(VectorXd _v);
        void AssembleMassMatrix();//Shape function dependent

        void getEnergy(double& e);
        void getForce(VectorXd& f);
        void getMass(SpMat& M);
        void getHessian(SpMat& H);
        void getPosition(VectorXd& p);
        void getVelocity(VectorXd& v);
        void FiniteDifferenceMethod(VectorXd& f, MatrixXd& H);


        int n_Nodes;// number of Nodes 
        int n_DOF;//  number of degree of freedom
        int n_Quads;// number of Quadrilateral Elements

    private:       

        Mesh* pMesh;
        vector<Kinetics*> v_pKineticElements;
        
        double Energy;

        VectorXd Force;
        SpMat Mass;
        SpMat Hessian;
        VectorXd Position;
        VectorXd Velocity;
};
System::System(Mesh* _pMesh,Material* _Material)
{
    pMesh=_pMesh;
    MatrixXi C=pMesh->ConnectivityMatrix;
    n_Quads=C.rows();
    n_Nodes=pMesh->v_pCompleteListOfNodes.size();
    n_DOF=2*n_Nodes;
    
    for(int i=0;i<C.rows(); i++)
    {   
        vector<Node*> vq;
        for(int j=0;j<C.cols(); j++)
        {
            cout<<"i "<< i<<endl;
            cout<<"j "<< j<<endl;
            cout<<C(i,j)<<endl;
            vq.push_back(pMesh->v_pCompleteListOfNodes[C(i,j)]);           
        }
        cout<<"system 1"<<endl;
        v_pKineticElements.push_back(new Kinetics(_Material, new Quadrilateral(vq)));
        cout<<"system 2"<<endl;
    }   
    AssembleHessianAndMass();
    cout<<"system 3"<<endl;
    AssemblePosition();
    cout<<"system 4"<<endl;
    AssembleVelocity();

}

System::System(Mesh* _pMesh,vector<Material*> _vMaterial)
{
    pMesh=_pMesh;
    MatrixXi C=pMesh->ConnectivityMatrix;
    n_Quads=C.rows();
    n_Nodes=pMesh->v_pCompleteListOfNodes.size();
    n_DOF=2*n_Nodes;
    cout<<"here in system1"<<endl;
    for(int i=0;i<C.rows(); i++)
    {   
        vector<Node*> vq;
        for(int j=0;j<C.cols(); j++)
        {
            vq.push_back(pMesh->v_pCompleteListOfNodes[C(i,j)]);           
        }
        cout<<"in loop 1"<<endl;
        v_pKineticElements.push_back(new Kinetics(_vMaterial[i], new Quadrilateral(vq)));
    }   
    cout<<"here in system2"<<endl;
    AssembleHessianAndMass();

    AssemblePosition();
 
    AssembleVelocity();

}

void System::AssembleEnergy()
{   
    Energy=0;
    for(int i=0; i<n_Quads; i++)
    {   double e;
        v_pKineticElements[i]->getEnergy(e);
        Energy+=e;
    }

}
void System::AssembleForce()
{   
    MatrixXi C= pMesh->ConnectivityMatrix;
    VectorXd _Force(n_DOF);
    _Force.setZero();
    for(int i=0; i<n_Quads; i++)
    {
        vector<int> vDOF{   2*C(i,0),2*C(i,0)+1,
                            2*C(i,1),2*C(i,1)+1,
                            2*C(i,2),2*C(i,2)+1,
                            2*C(i,3),2*C(i,3)+1,
                        };
        VectorXd f;
        v_pKineticElements[i]->getForce(f);
        _Force(vDOF)+=f;// Vector Indexing New Supported Feature;
    }

    Force=_Force;
}

void System::AssembleHessianAndMass()
{   
  
    MatrixXi C= pMesh->ConnectivityMatrix;
    vector<T> list_Hessian,list_Mass;

    for(int i=0; i<n_Quads; i++)
    {
        vector<int> vDOF{   2*C(i,0),2*C(i,0)+1,
                            2*C(i,1),2*C(i,1)+1,
                            2*C(i,2),2*C(i,2)+1,
                            2*C(i,3),2*C(i,3)+1,
                        };

        MatrixXd h;
        double m;
        v_pKineticElements[i]->getHessian(h);
        v_pKineticElements[i]->getMass(m);
  
        m*=0.25;
        for(int ii=0; ii<vDOF.size();ii++)
        {   
            for(int j=0; j<vDOF.size();j++)
            {   
                if(abs(h(ii,j))> 1e-15)
                {
                      T t(vDOF[ii],vDOF[j],0.25*h(ii,j));               
                      list_Hessian.push_back(t);
                }
              
            }
        };

        Matrix4d MassDistribution;
        MassDistribution<< 4.0,2.0,1.0,2.0,
                           2.0,4.0,2.0,1.0,
                           1.0,2.0,4.0,2.0,
                           2.0,1.0,2.0,4.0;
        MassDistribution/=9.0;

        for(int ii=0; ii<4; ii++)
        {
            for(int j=0; j<4; j++)
            {
                T t1(vDOF[2*ii],  vDOF[2*j],  MassDistribution(ii,j)*m);
                T t2(vDOF[2*ii+1],vDOF[2*j+1],MassDistribution(ii,j)*m);
                list_Mass.push_back(t1);
                list_Mass.push_back(t2);
            }

        }         

    }
    SpMat _Hessian(n_DOF,n_DOF);
    SpMat _Mass(n_DOF,n_DOF);
    _Hessian.setFromTriplets(list_Hessian.begin(),list_Hessian.end());
    _Mass.setFromTriplets(list_Mass.begin(),list_Mass.end());

    Hessian= _Hessian;
    Mass=_Mass;

}

void System::AssemblePosition()
{
    VectorXd _Position(n_DOF);
    for(int i=0; i<n_Nodes;i++)
    {  
        Vector2d v;
        v=pMesh->v_pCompleteListOfNodes[i]->getPosition();
        _Position.segment(2*i,2)=v;
    }
    Position=_Position;
}

void System::setPosition(VectorXd _p)
{
    Position=_p;
    for(int i=0; i<n_Nodes;i++)
    {  
        Vector2d v;
        v=_p.segment(2*i,2);   
        pMesh->v_pCompleteListOfNodes[i]->setPosition(v);
    }
}

void System::AssembleVelocity()
{
    VectorXd _Velocity(n_DOF);
    for(int i=0; i<n_Nodes;i++)
    {  
        Vector2d v;
        v=pMesh->v_pCompleteListOfNodes[i]->getVelocity();
        _Velocity.segment(2*i,2)=v;
    }
    Velocity=_Velocity;
}

void System::setVelocity(VectorXd _v)
{
    Velocity=_v;
    for(int i=0; i<n_Nodes;i++)
    {  
        Vector2d v;
        v=_v.segment(2*i,2);   
        pMesh->v_pCompleteListOfNodes[i]->setVelocity(v);
    }
}

void System::getEnergy(double& e)
{   
    AssembleEnergy();
    e=Energy;

}
void System::getForce(VectorXd& f)
{   
    // VectorXd f1;
    // MatrixXd H1;
    // FiniteDifferenceMethod(f1,H1);

    AssembleForce();
    f=Force;
   
}

void System::getHessian(SpMat& H)
{   
    // MatrixXd H1;
    // VectorXd f;
    // FiniteDifferenceMethod(f,H1);
    // H=H1.sparseView();
    AssembleHessianAndMass();
    H=Hessian;
}

void System::getMass(SpMat& M)
{
    M=Mass;
}

void System::getPosition(VectorXd& p)
{
    p=Position;
}
void System::getVelocity(VectorXd& v)
{
    v=Velocity;
}

void System::FiniteDifferenceMethod(VectorXd& f, MatrixXd& H)
{   
    VectorXd p0=Position;
    double epsilon;
    epsilon= 1e-4;
    VectorXd dp(n_DOF);

    dp.setZero();

    f.resize(n_DOF);
    H.resize(n_DOF,n_DOF);
    double e0;
    getEnergy(e0);

    for(int i=0;i<n_DOF;i++)
    {
        dp(i)+=epsilon;
        setPosition(p0+dp);
        double e1;
        getEnergy(e1);
        f(i)=(e1-e0)/epsilon;
        dp.setZero();
    }

    for (int i=0; i<n_DOF; i++)
    {
        for(int j=0; j< n_DOF; j++)
        {
            double e1,e2,e12;

            dp(i)+=epsilon;
            setPosition(p0+dp);
            getEnergy(e1);
            dp.setZero();

            dp(j)+=epsilon;
            setPosition(p0+dp);
            getEnergy(e2);
            dp.setZero();

            dp(i)+=epsilon;
            dp(j)+=epsilon;
            setPosition(p0+dp);
            getEnergy(e12);
            dp.setZero();

            setPosition(p0);
            H(i,j)= (e12-e1-e2+e0)/(epsilon*epsilon);
    
        }

    }
}