#pragma once
#include<mains/ADMM/ADMM.hpp>
#include<mains/ADMM/util.hpp>
#include<utils/timing.hpp>
#include<utils/Render.hpp>
namespace ADMM
{
    using namespace std;
    using namespace Eigen;
    class WholeDomainProcessing
    {
        public: 
            vector<Subsystem*> vSS;
            SpMat SpA;
            int OccupiedCoarseElementCount;
            vector<int> vcx,vcy,vc;//vc global index of a submesh c=ny*x+y
            map<int,int> MapGlobalSubmeshIndexToRealIndex;
            vector<map<int,int>> vAll2Active;
            vector<map<int,int>> vActive2All;

            vector<Subsystem*> SubsystemInThis;//handle for a single oversampling

            int Nx,Ny;
            int fpc;

            WholeDomainProcessing(vector<Subsystem*> _vSS, SpMat _SpA,int _fpc,
            vector<map<int,int>>_vAll2Active, vector<map<int,int>>_vActive2All)
            {
                vSS=_vSS;
                SpA=_SpA;

                fpc=_fpc;

                vAll2Active=_vAll2Active;
                vActive2All=_vActive2All;
                OccupiedCoarseElementCount= SpA.nonZeros();
                Nx=SpA.rows();
                Ny=SpA.cols();
                for (int i=0; i<OccupiedCoarseElementCount;i++)
                {
                    int row,col;
                    int Ny=SpA.cols();
                    RowAndColForAnEntry(SpA, i, row,  col);
                    vcx.push_back(row);
                    vcy.push_back(col);
                    vc.push_back(Ny*row+col);
                } 
                        
                for (int i = 0; i < vc.size(); ++i)
                {
                    MapGlobalSubmeshIndexToRealIndex[vc[i]]=i;            
                }
            }

            DirichletInterface* C2D(CrossElement* C, Subsystem* pSS)
            {
                auto D=new DirichletInterface(pSS, C->mapDOF[pSS], C->globalDOF );
                D->pC=C;
                return D;

            }

            void InterfaceChange(Subsystem* ss, int InterfaceNumber)
            {   
                pair<DirichletInterface*,CrossElement*> pairForRestore;                
                ss->vInter[InterfaceNumber]=C2D(static_cast<CrossElement*> (ss->vInter[InterfaceNumber])
                , ss);
            }

            void InterpolateDisplacement(int ix, int iy,// ix,iy {-1,0,1} 
            int ifx, int ify,int fepc, Vector2d& u, vector<Vector2d> vD )// node ifx,ify location in the submesh
            {
                MatrixXd P(4,2);
                P<<-1,-1,
                    1,-1,
                    1, 1,
                   -1, 1;
                //cout<<"Interpolate displacement 1"<<endl;
                double ksee, ita;
                
                ksee= ((double)ix*fepc+ifx-0.5*fepc)/(1.5*fepc);
                ita=  ((double)iy*fepc+ify-0.5*fepc)/(1.5*fepc);
                //cout<<"Interpolate displacement 2"<<endl;
                //cout<<"ksee  " << ksee << "  ita  "<< ita <<endl; 
                u.setZero();
                for(int i=0; i<4; i++)
                {
                    u+=(1+P(i,0)*ksee)*(1+P(i,1)*ita)*vD[i]/4.0;
                }
            }
            void InterpolateDisplacement(int ig, int fepc, Vector2d& u, vector<Vector2d> vD )// for oversampling area global numbering 
            {
                   int ixg= ig/(3*fepc+1);
                   int iyg= ig%(3*fepc+1);
                   int ix= ixg/fepc;
                   int iy= iyg/fepc;
                   int ifx= ixg-fepc*ix;
                   int ify= iyg-fepc*iy;
                   InterpolateDisplacement(ix-1,  iy-1, ifx, ify,fepc,u,vD );



            };
            void QuadraticDisplacement(int ix, int iy,// ix,iy {-1,0,1} 
            int ifx, int ify,int fepc, Vector2d& u, int Q, double max_u)//Q: case # , u: max displacement
            {
                double ksee= ((double)ix*fepc+ifx-0.5*fepc)/(3.0*fepc);
                double ita=  ((double)iy*fepc+ify-0.5*fepc)/(3.0*fepc);
                u.setZero();
                switch(Q) {
                        case 1 :
                            u(0)= max_u* ksee * ksee;
                            break;
                        case 2 :
                            u(1)= max_u* ksee * ksee;
                            break;
                        case 3 :
                            u(0)= max_u* ita  * ita;
                            break;
                        case 4 :
                            u(1)= max_u* ita  * ita;
                            break;                            
                        case 5:
                            u(0)= max_u* ksee * ita;
                            break;
                        case 6 :
                            u(1)= max_u* ksee * ita;
                            break;
                        default :
                            cout << "Invalid case number" << endl;
                }
            }
            
            bool IsActive(int n_Submesh, int n_Node)
            {
                map<int,int>::const_iterator it = vAll2Active[n_Submesh].find(n_Node);
                return it!=vAll2Active[n_Submesh].end();
            }

            void OversampleAnElement( int i,vector<Vector2d> vD,VectorXd& FineNodeDisplacement,vector<Vector2d>& CoarseNodeDisplacement)//linear interpolation, parametric order for coarse node
            {
                timing t0("preprocessing oversample an element");
                int x=vcx[i];
                int y=vcy[i];
                vector<bool> vIsCoarseElementActivated;
                vector<int> Sequence;
                vector<int> vReal;
                vector<int> v={-1,0,1};
                vector<int> CoarseElementNumberInSubmesh={0,fpc*(fpc-1), fpc*fpc-1,fpc-1};
                
                Vector2d v2;
                //cout<<"Oversample an element 0"<<endl;
                InterpolateDisplacement(0,0,0,0,fpc-1,v2,vD);
                CoarseNodeDisplacement.push_back(v2);
                InterpolateDisplacement(0,0,fpc-1,0,fpc-1,v2,vD);
                CoarseNodeDisplacement.push_back(v2);
                //cout<<"Oversample an element 1"<<endl;
                InterpolateDisplacement(0,0,fpc-1,fpc-1,fpc-1,v2,vD);
                CoarseNodeDisplacement.push_back(v2);
                InterpolateDisplacement(0,0,0,fpc-1,fpc-1,v2,vD);
                CoarseNodeDisplacement.push_back(v2);
                // init coarse nodes displacement;

                SubsystemInThis.clear();
                int count=0;
                int center;
                for (int ix:v)
                {
                    for(int iy:v)
                    {   //cout<<"ix  "<< ix << "   iy  "<<iy<<endl;
                    
                        if(SpA.coeff(x+ix,y+iy)!=0)
                        {
    
                            int ig = Ny*(x+ix)+y+iy;
                            int ir= MapGlobalSubmeshIndexToRealIndex[ig];
                            SubsystemInThis.push_back(vSS[ir]);
                            vReal.push_back(ir);
                            //vSS[ir]->Position.setZero();
                            Sequence.push_back(count);
                            if((ix==0)&(iy==0)) center=count;

                            count++;
                            //cout<<"Oversample an element 2"<<endl;           
                            //exchange boundary condition
                            
                            if ((ix==1) & (vSS[ir]->vInter[0]->flag=='C'))
                                InterfaceChange(vSS[ir], 0);
                            if ((ix==-1) & (vSS[ir]->vInter[1]->flag=='C'))
                                InterfaceChange(vSS[ir], 1);
                            if ((iy==1) & (vSS[ir]->vInter[2]->flag=='C'))
                                InterfaceChange(vSS[ir], 2);
                            if ((iy==-1) & (vSS[ir]->vInter[3]->flag=='C'))
                                InterfaceChange(vSS[ir], 3);
                            //cout<<"Oversample an element 3"<<endl;


                            //set dirichlet boundary condition
                        
                            for(auto pInter:vSS[ir]->vInter)
                            {   //cout<<"Oversample an element 3.1"<<endl;
                                //cout<<pInter->flag<<endl;
                                if (pInter->flag=='D')
                                {
                                    auto pD=static_cast<DirichletInterface*>(pInter);
                                    //cout<<"Oversample an element 3.2"<<endl;
                                    for(int l=0; l<pD->vDOF.size(); l=l+2)
                                    {   
                                        int i_node=pD->vDOF[l]/2;
                                        
                                        auto myMap= vActive2All[ir];
                                        
                                        int i_nodeInAll=vActive2All[ir][i_node];
                                        int ifx=i_nodeInAll/fpc;
                                        
                                        int ify=i_nodeInAll%fpc;

                                        Vector2d a;
                                        InterpolateDisplacement(ix, iy,
                                        ifx, ify,fpc-1, 
                                        a ,vD);
                                        pD->DirichletBoundary.segment(l,2)=a;
                                        cout<<a<<endl;
                                        //QuadraticDisplacement(ix,iy,ifx,ify,fpc-1,a,3,100);
                            
                                    } 

                                }

                            }                                
                        }else{
                            //cout<<"Oversample an element 5"<<endl;
                            vIsCoarseElementActivated.push_back(false);
                        }       
                    }
                } 
                
                //modify the sequence by shifting the center to end
                int a= Sequence.back();
                Sequence.pop_back();
                Sequence.push_back(center);
                Sequence[center]=a;
                //cout<<"Oversample an element 6"<<endl;
                //All boundary condition been set up, start oversampling
                unique_ptr<NewOversampling> pN (new NewOversampling(SubsystemInThis,Sequence));
                //cout<<"Oversample an element 6.5"<<endl;

                int interest=center;
                int actual=vReal[interest];
                auto syst= SubsystemInThis[interest];              
                t0.end();
                
                timing t1("200 iteration");
                for(int i=0;i<100;i++)
                {
                    pN->runADMMIteration();
                }
                t1.end();
                
                timing t2("postprocessing");
                FineNodeDisplacement=syst->Position;
                cout<<syst->Position<<endl;
                for(int k=0; k<4;k++)
                {   int n_Node=CoarseElementNumberInSubmesh[k];
                    if (IsActive(i,n_Node))
                        {
                            int n_node_Active= vAll2Active[i][n_Node];
                            n_Node=n_node_Active;
                        }                       


                    auto DOF= Node2DOF(n_Node);
                    CoarseNodeDisplacement[k]=syst->Position(DOF);
                }
                t2.end();
            }

            void OversampleAnElementQ( int i, int Scenario, double max_u,VectorXd& FineNodeDisplacement,vector<Vector2d>& CoarseNodeDisplacement)//Quadratic interpolation
            {
                int x=vcx[i];
                int y=vcy[i];
                vector<bool> vIsCoarseElementActivated;
                vector<int> Sequence;
                vector<int> vReal;
                const vector<int> v={-1,0,1};
                const vector<int> CoarseElementNumberInSubmesh={0,fpc*(fpc-1), fpc*fpc-1,fpc-1};
                Vector2d v2;

                QuadraticDisplacement(0,0,0,0,fpc-1,v2,Scenario, max_u);
                CoarseNodeDisplacement.push_back(v2);
                QuadraticDisplacement(0,0,fpc-1,0,fpc-1,v2,Scenario, max_u);
                CoarseNodeDisplacement.push_back(v2);
                QuadraticDisplacement(0,0,fpc-1,fpc-1,fpc-1,v2,Scenario, max_u);
                CoarseNodeDisplacement.push_back(v2);
                QuadraticDisplacement(0,0,0,fpc-1,fpc-1,v2,Scenario, max_u);
                CoarseNodeDisplacement.push_back(v2);
                // init coarse nodes displacement;

                SubsystemInThis.clear();
                int count=0;
                int center;
                for (int ix:v)
                {
                    for(int iy:v)
                    {   //cout<<"ix  "<< ix << "   iy  "<<iy<<endl;
                    
                        if(SpA.coeff(x+ix,y+iy)!=0)
                        {
                            vIsCoarseElementActivated.push_back(true);
                            int ig = Ny*(x+ix)+y+iy;
                            int ir= MapGlobalSubmeshIndexToRealIndex[ig];
                            SubsystemInThis.push_back(vSS[ir]);
                            vReal.push_back(ir);
                            //vSS[ir]->Position.setZero();
                            Sequence.push_back(count);
                            if((ix==0)&(iy==0)) center=count;

                            count++;
                            //cout<<"Oversample an element 2"<<endl;           
                            //exchange boundary condition
                            
                            if ((ix==1) & (vSS[ir]->vInter[0]->flag=='C'))
                                InterfaceChange(vSS[ir], 0);
                            if ((ix==-1) & (vSS[ir]->vInter[1]->flag=='C'))
                                InterfaceChange(vSS[ir], 1);
                            if ((iy==1) & (vSS[ir]->vInter[2]->flag=='C'))
                                InterfaceChange(vSS[ir], 2);
                            if ((iy==-1) & (vSS[ir]->vInter[3]->flag=='C'))
                                InterfaceChange(vSS[ir], 3);
                            //cout<<"Oversample an element 3"<<endl;


                            //set dirichlet boundary condition
                        
                            for(auto pInter:vSS[ir]->vInter)
                            {   //cout<<"Oversample an element 3.1"<<endl;
                                //cout<<pInter->flag<<endl;
                                if (pInter->flag=='D')
                                {
                                    auto pD=static_cast<DirichletInterface*>(pInter);
                                    //cout<<"Oversample an element 3.2"<<endl;
                                    for(int l=0; l<pD->vDOF.size(); l=l+2)
                                    {   
                                        int i_node=pD->vDOF[l]/2;
                                        
                                        auto myMap= vActive2All[ir];
                                        
                                        int i_nodeInAll=vActive2All[ir][i_node];
                                        int ifx=i_nodeInAll/fpc;
                                        //cout<<"Oversample an element 3.5"<<endl;
                                        int ify=i_nodeInAll%fpc;

                                        Vector2d a;
                                        QuadraticDisplacement(ix, iy,
                                        ifx, ify,fpc-1, 
                                        a ,Scenario, max_u);
                                        pD->DirichletBoundary.segment(l,2)=a;
                                        //QuadraticDisplacement(ix,iy,ifx,ify,fpc-1,a,3,100);
                            
                                    } 

                                }

                            }                                
                        }else{
                            //cout<<"Oversample an element 5"<<endl;
                            vIsCoarseElementActivated.push_back(false);
                        }       
                    }
                } 
                
                //modify the sequence by chaging the center a bit
                int a= Sequence.back();
                Sequence.pop_back();
                Sequence.push_back(center);
                Sequence[center]=a;
                //cout<<"Oversample an element 6"<<endl;
                //All boundary condition been set up, start oversampling
                unique_ptr<NewOversampling> pN (new NewOversampling(SubsystemInThis,Sequence));
                //cout<<"Oversample an element 6.5"<<endl;

                int interest=center;
                int actual=vReal[interest];
                auto syst= SubsystemInThis[interest];  
                auto p=static_cast<DirichletInterface*>(SubsystemInThis[0]->vInter[4]);        
                cout<< p->DirichletBoundary<<endl;
                for(int i=0;i<200;i++)
                {
                    pN->runADMMIteration();
                }
                FineNodeDisplacement=syst->Position;
            
                for(int k=0; k<4;k++)
                {   int n_Node=CoarseElementNumberInSubmesh[k];
                    if (IsActive(i,n_Node))
                        int n_node_Active= vAll2Active[i][n_Node];

                    auto DOF= Node2DOF(n_Node);
                    CoarseNodeDisplacement[k]=syst->Position(DOF);
                }
            }

            void RestoreChange()
            {
                //oversampling finished, restore the boundary condition
                for (auto &pSS: SubsystemInThis)
                {
                    for(auto &pInter: pSS->vInter)
                    {
                        if (pInter->flag=='D')
                        {
                            auto pD= static_cast<DirichletInterface*>(pInter);
                            if (pD->pC!=NULL)
                            {
                                pInter= pD->pC;
                                delete pD;
                            }
                                
                        }                            
                    }
                }
                // clear subsystem in this

                SubsystemInThis.clear();

                

            }

            void GoThroughScenario(int i, double max_u,vector<VectorXd>&FineNodeResult, vector<vector<Vector2d>>&CoarseNodeResult)
            {   //cout<<"GoThroughScenario(0)"<<endl;
                FineNodeResult.clear();
                CoarseNodeResult.clear();
                vector<Vector2d> vD(4);
                VectorXd F;
                vector<Vector2d> C;
                //1: x stretch
                //timing t0("set displacement of coarse nodes");                
                vD[0]<<-max_u,0;
                vD[1]<< max_u,0;
                vD[2]<< max_u,0;   
                vD[3]<<-max_u,0;
                //t0.end();        

                //timing t1("linear oversample ");
                OversampleAnElement(i, vD,F,C);
                //t1.end();

                //timing t2("push back result ");
                FineNodeResult.push_back(F);
                CoarseNodeResult.push_back(C);
                RestoreChange();
                //t2.end();
                //cout<<"GoThroughScenario(1)"<<endl;

                //2: y strech
                vD[0]<<0,-max_u;
                vD[1]<<0,-max_u;
                vD[2]<<0, max_u;    
                vD[3]<<0, max_u;           
                OversampleAnElement(i, vD,F,C);
                FineNodeResult.push_back(F);
                CoarseNodeResult.push_back(C);
                RestoreChange();

                //3: x shear
                vD[0]<<-max_u,0;
                vD[1]<<-max_u,0;
                vD[2]<< max_u,0;    
                vD[3]<< max_u,0; 
                OversampleAnElement(i, vD,F,C);
                FineNodeResult.push_back(F);
                CoarseNodeResult.push_back(C);
                RestoreChange();

                //4: y shear
                vD[0]<<0,-max_u;
                vD[1]<<0, max_u;
                vD[2]<<0, max_u;    
                vD[3]<<0,-max_u;           
                OversampleAnElement(i, vD,F,C);
                FineNodeResult.push_back(F);
                CoarseNodeResult.push_back(C);
                RestoreChange();
                
                //Quadratic
                for(int s=1;s<=6;s++)
                {   //timing tq("Quadratic oversample ");
                    OversampleAnElementQ(i,s,max_u,F,C);
                    //tq.end();
                    FineNodeResult.push_back(F);
                    CoarseNodeResult.push_back(C);
                    RestoreChange();

                }


                //restore everything
                //




                
            }
                
            

    };
};