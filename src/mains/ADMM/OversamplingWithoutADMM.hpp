#pragma once
#include<mains/MatrixCoefficients/HarmonicDisplacement.hpp>

#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>

#include <mains/2DFEM/Materials/Material_StVK.hpp>
#include <mains/2DFEM/Mesh.hpp>
#include <mains/2DFEM/System.hpp>
#include <mains/2DFEM/Integrator.hpp>
#include <mains/2DFEM/BoundaryConditions/Dirichlet.hpp>
#include <mains/2DFEM/BoundaryConditions/Neumann.hpp>
#include <mains/2DFEM/BoundaryConditions/Gravity.hpp>

#include <mains/2DFEM/Integrators/NewtonStaticB.hpp>
#include <mains/2DFEM/Integrators/NewtonStaticCoarse.hpp>
#include <mains/2DFEM/Integrators/GradientDescent.hpp>

#include <mains/MatrixCoefficients/HierachyMesh.hpp>
#include <mains/MatrixCoefficients/OnlyKD.hpp>

#include <mains/NumericalCoarsening/CoarseElement.hpp>
#include <mains/NumericalCoarsening/CoarseningSystem.hpp>

#include<mains/ADMM/ADMM.hpp>
#include<mains/ADMM/Oversampling2D.hpp>
#include<mains/ADMM/util.hpp>

#include<mains/LSQ/LagrangianLSQ.hpp>

#include <chrono> 
#include <utils/io/writeMatrix.hpp>
#include <utils/io/readCSV.hpp>
#include <utils/SparseMatrixIndex.hpp>
#include <utils/SparseMatrixSlicing.hpp>
#include <utils/timing.hpp>
#include <utils/SquashPositionVector.hpp>
#include <mains/ADMM/setSubSystem.hpp>

namespace OversamplingWithoutADMM
{   
    typedef  SparseMatrix<double> SpMat;
    using namespace Eigen;
    using namespace std;
    MatrixXd GlobalPattern()
    {
        string str2= "MatlabData/GlobalFineElementDistribution.csv";        
        MatrixXd m2 = utils::load_csv<MatrixXd>(str2);
        return m2;
    }
    MatrixXd GlobalCoarsePattern()
    {
        string str1= "MatlabData/CoarseElementDistribution.csv";        
        MatrixXd m1 = utils::load_csv<MatrixXd>(str1);
        return m1;
    }
    void AssembleSurrounding(int xc,int yc,
    SpMat& Hessian, vector<int>& vDOFCentralElement, vector<int>& xpDOF,
    vector<int>& ypDOF,vector<int>& xmDOF,vector<int>& ymDOF, int& Nx, int& Ny )
    {   
        MatrixXd C=GlobalCoarsePattern();
        MatrixXd G0=GlobalPattern();
        int Nxc=C.rows();
        int Nyc=C.cols();
        int fepc= G0.rows()/Nxc;
        cout<<fepc<<endl;

        int xmin=-1;
        int xmax= 1;
        int ymin=-1;
        int ymax= 1;
        if (xc==0) xmin++; 
        if (yc==0) ymin++; 
        if (xc==Nxc-1) xmax--; 
        if (yc==Nyc-1) ymax--;
        int Nex= (xmax-xmin+1)*fepc;//local
        int Ney= (ymax-ymin+1)*fepc;//local
        Nx=Nex+1;//local
        Ny=Ney+1;//local

        vector<int> vx,vy;

        for(int i=(xc+xmin)*fepc;i<(xc+xmax+1)*fepc;i++)
        {
            vx.push_back(i);
            cout<<i<<endl;
        }

        for(int i=(yc+ymin)*fepc;i<(yc+ymax+1)*fepc;i++)
        {
            vy.push_back(i);
            cout<<i<<endl;
        }
        MatrixXd G=GlobalPattern()(vx,vy);
        SpMat spG= (G).sparseView();
        ArbitraryQuadMesh mesh(1.0, spG);
        cout<<"A1"<<endl;
        vector<Material_StVK*> M;
        for(int i=0;i<4;i++)
        {
            M.push_back(new Material_StVK());
        }
        cout<<"A2"<<endl;
        M[0]->setMaterialFromYP(0  ,0   ,  0);
        M[1]->setMaterialFromYP(1e6,0.49,1e3);
        M[2]->setMaterialFromYP(1e6,0.49,1e3);
        M[3]->setMaterialFromYP(1e4,0.49,1e3);
        cout<<"A3"<<endl;

        vector<Material*> vMGlobal;
        cout<<spG<<endl;
        for(int i=0;i<spG.nonZeros();i++)
        {           
                int x,y;
                RowAndColForAnEntry(spG,i,x,y);
                int m= round(spG.coeff(x,y));
                vMGlobal.push_back(M[m]);
        }
        System syst(&mesh,vMGlobal);
        
        syst.getHessian(Hessian);
        Hessian*=-1;
        vector<int> centralIndex, xpIndex,xmIndex, ypIndex,ymIndex;
        for(int x=(-xmin)*fepc;x<(1-xmin)*fepc+1;x++)
        {
            for(int y=(-ymin)*fepc;y<(1-ymin)*fepc+1;y++)
            {
                centralIndex.push_back(Ny*x+y);
            }
        }

        for(int y=0; y<Ny;y++)
        {
            xpIndex.push_back(Ny*(Nx-1)+y);
            xmIndex.push_back(y);
        }
        for(int x=0; x<Nx;x++)
        {
            ypIndex.push_back(Ny*(x)+Ny-1);
            ymIndex.push_back(Ny*(x));
        }
        vDOFCentralElement=ADMM::Node2DOF(centralIndex);
        xpDOF=ADMM::Node2DOF(xpIndex);
        ypDOF=ADMM::Node2DOF(ypIndex);
        xmDOF=ADMM::Node2DOF(xmIndex);
        ymDOF=ADMM::Node2DOF(ymIndex);        
    }

    void Neumman()
    { 
        auto C=GlobalCoarsePattern();
        SpMat spC= C.sparseView();
        spC.makeCompressed();

        vector<vector<MatrixXd>> data2Chen(3);
        for (int k=0; k<spC.outerSize(); ++k)
        {
            for (SparseMatrix<double>::InnerIterator it(spC,k); it; ++it)
            {   
                int xc=it.row();
                int yc=it.col();
                SpMat Hessian;
                vector<int> vDOFCentralElement, xpDOF,ypDOF,xmDOF,ymDOF;
                int Nx,Ny;
                AssembleSurrounding(xc,yc,Hessian,vDOFCentralElement,xpDOF,ypDOF,xmDOF,ymDOF,Nx,Ny);
                auto LHS=Hessian;
                VectorXd RHS= VectorXd::Zero(Hessian.rows());
                VectorXd Rxp= VectorXd::Zero(xpDOF.size());
                VectorXd Rxm= VectorXd::Zero(xpDOF.size());
                for(int i=0;i<Rxp.size();i++)
                {   
                    if (i%2==0)
                    Rxp(i)=1;
                }
                for(int i=0;i<Rxm.size();i++)
                {   
                    if (i%2==0)
                    Rxm(i)=-1;
                }
                Rxp(0)*=0.5;
                Rxp.tail(1)*=0.5;
                Rxm(0)*=0.5;
                Rxm.tail(1)*=0.5;

                RHS(xpDOF)+=1e2*Rxp;
                RHS(xmDOF)+=1e2*Rxm;
                SparseQR<SpMat,COLAMDOrdering<int>> QR(LHS);
                VectorXd result=QR.solve(RHS);
                //Render::RenderRectangleMesh(1e2*result,Nx,Ny,1.0);
                data2Chen[0].push_back(utils::SquashPositionVector(result(vDOFCentralElement)));
            }
        }

        for (int k=0; k<spC.outerSize(); ++k)
        {
            for (SparseMatrix<double>::InnerIterator it(spC,k); it; ++it)
            {   
                int xc=it.row();
                int yc=it.col();
                SpMat Hessian;
                vector<int> vDOFCentralElement, xpDOF,ypDOF,xmDOF,ymDOF;
                int Nx,Ny;
                AssembleSurrounding(xc,yc,Hessian,vDOFCentralElement,xpDOF,ypDOF,xmDOF,ymDOF,Nx,Ny);
                auto LHS=Hessian;
                VectorXd RHS= VectorXd::Zero(Hessian.rows());
                VectorXd Rxp= VectorXd::Zero(xpDOF.size());
                VectorXd Rxm= VectorXd::Zero(xmDOF.size());
                for(int i=0;i<Rxp.size();i++)
                {   
                    if (i%2==1)
                    Rxp(i)=1;
                }
                for(int i=0;i<Rxm.size();i++)
                {   
                    if (i%2==1)
                    Rxm(i)=-1;
                }
                RHS(xpDOF)+=1e2*Rxp;
                RHS(xmDOF)+=1e2*Rxm;
                SparseQR<SpMat,COLAMDOrdering<int>> QR(LHS);
                VectorXd result=QR.solve(RHS);
                
                data2Chen[1].push_back(utils::SquashPositionVector(result(vDOFCentralElement)));
            }
        }

        for (int k=0; k<spC.outerSize(); ++k)
        {
            for (SparseMatrix<double>::InnerIterator it(spC,k); it; ++it)
            {   
                int xc=it.row();
                int yc=it.col();
                SpMat Hessian;
                vector<int> vDOFCentralElement, xpDOF,ypDOF,xmDOF,ymDOF;
                int Nx,Ny;
                AssembleSurrounding(xc,yc,Hessian,vDOFCentralElement,xpDOF,ypDOF,xmDOF,ymDOF,Nx,Ny);
                auto LHS=Hessian;
                VectorXd RHS= VectorXd::Zero(Hessian.rows());
                VectorXd Ryp= VectorXd::Zero(ypDOF.size());
                VectorXd Rym= VectorXd::Zero(ymDOF.size());
                for(int i=0;i<Ryp.size();i++)
                {   
                    if (i%2==0)
                    Ryp(i)=1;
                }
                for(int i=0;i<Rym.size();i++)
                {   
                    if (i%2==0)
                    Rym(i)=-1;
                }
                RHS(xpDOF)+=1e2*Ryp;
                RHS(xmDOF)+=1e2*Rym;
                SparseQR<SpMat,COLAMDOrdering<int>> QR(LHS);
                VectorXd result=QR.solve(RHS);
                
                data2Chen[2].push_back(utils::SquashPositionVector(result(vDOFCentralElement)));
            }
        }
        int fpc=11;
        int fepc=10;
        auto pChen= new ConstrainedOptimisation (data2Chen, fpc);
        
        pChen->buildAll();

        auto n_ij= pChen->get_n_ij();
        vector<vector<vector<Matrix2d>>> vW;// first index: the coarse element number, second index:coarse node number, third index fine node number

        vector<ADMM::Subsystem*> vADMMSubsystem;
        SpMat spA;
        
        vector<map<int,int>> vAll2Active;
        vector<map<int,int>> vActive2All;
        int OccupiedCoarseElementCount;
        vector<SpMat> vSubPattern;

        vector<Material_StVK> M(4);
        M[0].setMaterialFromYP(  0,   0,  0);
        M[1].setMaterialFromYP(1e6,0.49,1e3);
        M[2].setMaterialFromYP(1e6,0.49,1e3);
        M[3].setMaterialFromYP(1e4,0.49,1e3);

        ADMM::setSubSystem(M,vADMMSubsystem,spA,
            fpc, fepc,vAll2Active,
            vActive2All,OccupiedCoarseElementCount, vSubPattern);

        string str1= "MatlabData/CoarseElementDistribution.csv";        
        MatrixXd m1 = utils::load_csv<MatrixXd>(str1);
        SpMat GlobalCoarsePattern=m1.sparseView();

        
        auto pCoarseMesh=new ArbitraryQuadMesh((fpc-1)*1.0,GlobalCoarsePattern); 
        for(int k=0; k<pCoarseMesh->N_cell;k++)
        {   
            vector<vector<Matrix2d>> arrayW;
            VectorXd v= n_ij[k];

            //transport the coefficients
            for(int i=0;i<4 ;i++)
            {   vector<Matrix2d> vm1;
                for(int j=0; j<fpc*fpc; j++)
                {
                    VectorXd vs= v.segment(4*fpc*fpc*i+j*4,4);
                    Matrix2d m;
                    m<<vs(0), vs(1),
                    vs(2), vs(3);
                    vm1.push_back(m);
                };
                arrayW.push_back(vm1);
            };
        
            vW.push_back(arrayW);
        }
        for(int i=0; i<10;i++)
        {
            for(int k=0; k<9; k++)
            {
                for(int j=0; j<4;j++)
                {
                    cout<<"W"<< i<< j<< k<< ":\n"<< vW[i][j][k]<<endl;
                }
            }
        }

        auto pCMaterial= new CompositeMaterial();
        vector<CompositeMaterial*> v_pCMaterial;
        for(int i=0;i<GlobalCoarsePattern.nonZeros();i++)
        {
            auto pCMaterial= new CompositeMaterial();
            for(int j=0; j<fepc*fepc;j++)
            {   
                int x,y;
                x= j/fepc;
                y= j%fepc;
                int m= round(vSubPattern[i].coeff(x,y));
                pCMaterial->v_pMateiral.push_back(&M[m]);
            }
            v_pCMaterial.push_back(pCMaterial);
        }

        
        auto pCoarseningSystem= new CoarseningSystem(pCoarseMesh, v_pCMaterial,vW,fpc);
        VectorXd p;
        pCoarseningSystem->getPosition(p); 


        vector<int> indexDOF,indexDOFN,indexFineDOF,indexFineDOFN;
        vector<double> valueDOF,valueDOFN,valueFineDOFN;
        int n_dof_coarse=pCoarseningSystem->n_DOF;
        for(int i=0; i<4;i++)
        {
            indexDOF.push_back(2*i);
            indexDOF.push_back(2*i+1);
            // indexDOFN.push_back(8+2*i);            
            // valueDOFN.push_back(1e6);
            // indexDOFN.push_back(8+2*i+1);  
            // valueDOFN.push_back(3.0e6);
            // indexDOFN.push_back(n_dof_coarse-16+2*i+1);
            // valueDOFN.push_back(-2.0e6);

        }
       

        auto pDirichlet= new Dirichlet(indexDOF,valueDOF,pCoarseningSystem->n_DOF);
        auto pNeumann= new Neumann(indexDOFN,valueDOFN);
        
        auto pGravity= new Gravity(10);
        
        auto pIntegrator= new NewtonStaticForCoarsening(pCoarseningSystem);
        pIntegrator->v_pBoundaryConditions.push_back(pGravity);
        pIntegrator->v_pBoundaryConditions.push_back(pNeumann);
        pIntegrator->v_pBoundaryConditions.push_back(pDirichlet);


        cout<<"test coarse system 5"<<endl;
        
        DenseMatrixX V; // Vertices
        DenseMatrixXi E; // Edges, Face

        V.resize(pCoarseningSystem->n_Nodes,3);//+FineNodesNumber

        for(int i=0; i<40;i++)
        {
            pIntegrator->SingleStep();
            pCoarseningSystem->getPosition(p);
           
            for(int i=0; i< pCoarseningSystem->n_Nodes;i++)
            {
                V.row(i) = Vec3(p(2*i), 0.0, p(2*i+1));
		
            }           

            cout<<V<<endl;

        }
            
        

        Window window("Visualizer");
        window.setSize(1200, 700);
     

        pCoarseMesh->getEdges(E);   

        MeshTri3D mesh;
        int FineNodesInSingleCoarseElements=0;//todo
        int CoarseElementNumber=pCoarseMesh->N_cell;

        mesh.setName("test mesh");
        mesh.updatePointBuffer(V);
        mesh.addEdgeLayer("Edges")->updateEdgeBuffer(E);
        mesh.addPointMask("Coarse Nodes", 0, pCoarseningSystem->n_Nodes);
        mesh.initRenderable();

       
        
        while(!window.shouldClose())
        {   

            mesh.updatePointBuffer(V);

            window.clear();
            window.drawGrid();
            window.draw(&mesh);

            window.drawGui(&mesh);
            window.renderGui();
            window.swapBuffers();

            window.pollEvents();
            window.manageEvents();
        }

    } 

    void Dirichlet()
    {
        
    }
};