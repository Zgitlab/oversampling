#ifndef __ADMM_H__
#define __ADMM_H__

#pragma once

#include<Eigen/Sparse>
#include<Eigen/Core>
#include<iostream>
#include<utils/SparseMatrixBlockAddition.hpp>
#include<utils/io/writeMatrix.hpp>
#include<utils/SparseMatrixIndex.hpp>
#include<memory>
#include<utils/timing.hpp>
#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>

typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> T;

namespace ADMM{
    using namespace Eigen;
    using namespace std;
    
    class Interface;
    class Subsystem{
        public:
            int SingularLevel=0;// 2d 0-4 3d 0-6; The # of edges without neighbour
            int HessianModifiedLevel=0;   // 2d 0-4 3d 0-6 in total with singular level 4 or 6
            int dimension;
            int n_DOF;// number of DOF
            vector<Interface*> vInter;// 2*d, could be Neumman, Dirichlet or CrossElement
            SpMat Hessian, H0;
            SimplicialLDLT<SpMat> chol;
            SpMat L;
            SpMat LT;
            VectorXd Dinv;
            PermutationMatrix<-1,-1,int> P;
            PermutationMatrix<-1,-1,int> Pinv;

            VectorXd Position;//handle variable for the new oversampling 
            Subsystem(SpMat _Hessian,int _dimension);
            void BuildSolver(); 
            VectorXd solve(VectorXd RHS);
            VectorXd solveT(VectorXd RHS);//self designed solve                

    };
    Subsystem::Subsystem(SpMat _Hessian, int _dimension)
    {
        Hessian=_Hessian;
        H0=Hessian;
        dimension=_dimension;
        n_DOF=Hessian.rows();
        chol.analyzePattern(Hessian);  // the hessian diagnal is occupied, therefore modification doesn not change its pattern      
        P=chol.permutationP();
        Pinv=chol.permutationPinv();
        Position.resize(n_DOF);
        Position.setZero();
    }
    void Subsystem::BuildSolver()
    {
        // if ((SingularLevel+HessianModifiedLevel)==2*dimension)
        // {           
            chol.factorize(Hessian);        


        // }else{
        //     ////cout<<"Hessian is not correctly modified, Unable to build the solver"<<endl;
        // }

    }
    // VectorXd Subsystem::solveT(VectorXd RHS0)
    // {
    //     RowVectorXd RHS=RHS.transpose();
    //     RHS=RHS*Pinv;
    //     RHS-=RHS*LT;

    //     RHS=RHS.cwiseProduct(Dinv.transpose());
    //     RHS-=RHS*L;
    //     RHS=RHS*P;
    //     RHS0=RHS.transpose();
    //     return RHS0;
    // }
    // VectorXd Subsystem::solve(VectorXd RHS)
    // {
    //     RHS=P*RHS;
    //     RHS-=L*RHS;
    //     RHS=RHS.cwiseProduct(Dinv);
    //     RHS-=LT*RHS;
    //     RHS=Pinv*RHS;
    //     return RHS;
    // }

    class Interface
    {
        public:
            char flag='0';// D: Dirichlet N: Neumman C:Cross Element
            vector<int> globalDOF;//could be also used as a local DOF buffer
            int n_DOF=0;
            VectorXd PrimalResidual;
            VectorXd DualResidual;
            Interface(){};
            Interface(vector<int> _gDOF){
                                        globalDOF= _gDOF;
                                        n_DOF= _gDOF.size();
                                        PrimalResidual.resize(n_DOF);
                                        DualResidual.resize(n_DOF);
                                        };
           
    };

    class CrossElement: public Interface//in use it will represent all the internal interface of the deformable body
    {
        public:
            pair<Subsystem*,Subsystem*> pairSS;
            pair<vector<int>,vector<int>> pairDOF;
            pair<VectorXd, VectorXd> pairMultiplier;

            map<Subsystem*,Subsystem*> OppositeSubsystem;
            map<Subsystem*,vector<int>> mapDOF;//subsystem DOF
            map<Subsystem*,VectorXd*>  mapMultiplier;
            // the DOF in the global mesh
            CrossElement(Subsystem* s1, Subsystem* s2, 
                         vector<int> vDOF1, vector<int> vDOF2,
                         vector<int> gDOF);
            void ModifyHessian();//??

            double rho=1e7;
            SpMat W;// diagonal matrix?  at least symetic    
    };
    CrossElement::CrossElement(Subsystem* s1, Subsystem* s2, vector<int> vDOF1, 
    vector<int> vDOF2,vector<int> gDOF)//gDOF: global DOF
    {
        pairSS=pair<Subsystem*,Subsystem*> (s1,s2);
        pairDOF=pair<vector<int>,vector<int>> (vDOF1,vDOF2);
        n_DOF=vDOF1.size();
        W.resize(n_DOF,n_DOF);
        W.setIdentity();
        OppositeSubsystem={{s1,s2},{s2,s1}};
        mapDOF={{s1,vDOF1},{s2,vDOF2}};
        pairMultiplier.first.resize(n_DOF);
        pairMultiplier.second.resize(n_DOF);

        pairMultiplier.first.setZero();
        pairMultiplier.second.setZero();


        mapMultiplier={{s1,&pairMultiplier.first},{s2,&pairMultiplier.second}};
        flag='C';
        globalDOF=gDOF;

        //rho= s1->H0.norm()/s1->H0.rows();
    }
    void CrossElement::ModifyHessian()
    {
        MatrixAddition(rho*W*W, pairDOF.first ,  pairSS.first->Hessian);
        pairSS.first->HessianModifiedLevel++;
        MatrixAddition(rho*W*W, pairDOF.second,  pairSS.second->Hessian);    
        pairSS.second->HessianModifiedLevel++;   
    }

    class DirichletInterface: public Interface
    {
        public:
            Subsystem* pSS;// subsystem
            vector<int> vDOF;
            double rho=1e7;
            SpMat W;
            VectorXd DirichletBoundary;
            VectorXd Multiplier;
            DirichletInterface(Subsystem* _pss, vector<int> _DOF,vector<int> gDOF);
            void ModifyHessian();
            CrossElement* pC= NULL;

    };
    DirichletInterface::DirichletInterface(Subsystem* _pss, vector<int> _DOF,vector<int> gDOF)
    {
        pSS=_pss;
        vDOF=_DOF;
        globalDOF=gDOF;
        n_DOF=vDOF.size();
        W.resize(n_DOF,n_DOF);
        W.setIdentity();
        flag='D';
        DirichletBoundary.resize(n_DOF);
        DirichletBoundary.setZero();
        Multiplier.resize(n_DOF);
        Multiplier.setZero();
        //rho= _pss->H0.norm()/_pss->H0.rows();
    }

    void DirichletInterface::ModifyHessian()
    {
        MatrixAddition(rho*W*W, vDOF,  pSS->Hessian);
        pSS->HessianModifiedLevel++;
    }

    class NeummanInterface: public Interface{

        public:
            Subsystem* pSS;// subsystem
            vector<int> vDOF;
            VectorXd NeummanBoundary;
            NeummanInterface(Subsystem* _pss, vector<int> _DOF,vector<int> gDOF);
    };

    NeummanInterface:: NeummanInterface(Subsystem* _pss, vector<int> _DOF,vector<int> gDOF)
    {
        pSS=_pss;
        vDOF=_DOF;
        globalDOF=gDOF;
        n_DOF=vDOF.size();
        flag='N';
        NeummanBoundary.resize(n_DOF);
        NeummanBoundary.setZero();

    }

    class NewOversampling{
        public:
            vector<Subsystem*> vSS;
            vector<int> Sequence;
            NewOversampling(vector<Subsystem*> _vSS,vector<int> _Sequence)
            {
                vSS=_vSS;
                Sequence=_Sequence;
            }
            void getRHS(int EleNum,VectorXd& RHS);
            void updateMultiplier();
            void runADMMIteration();
            void updatePrimalResidual();
            void updateDualResidual();

            
    };
    void  NewOversampling::updatePrimalResidual()
    {
        for(auto pSS:vSS)
        {
            for(auto pInter: pSS->vInter)
            {
            if (pInter->flag=='C')
            {
                auto p=static_cast<CrossElement*>(pInter);
                auto iDOF=p->mapDOF[pSS];
                auto pSS2=p->OppositeSubsystem[pSS];
                auto iDOF2=p->mapDOF[pSS2];
                VectorXd newPrimalResidual;
                newPrimalResidual=pSS->Position(iDOF)-pSS2->Position(iDOF2);

                cout<<"New primal Residual for C: \n"<<newPrimalResidual.norm()<<endl;
                cout<<"Old Primal Residual for C: \n"<<pInter->PrimalResidual.norm()<<endl;           
                pInter->PrimalResidual=newPrimalResidual;

            }else if(pInter->flag=='D')
            {
                auto p=static_cast<DirichletInterface*>(pInter);
               
                auto iDOF= p->vDOF;
                
                
                VectorXd newPrimalResidual;
                newPrimalResidual=pSS->Position(iDOF)-p->DirichletBoundary;

                cout<<"New primal Residual for D: \n"<<newPrimalResidual.norm()<<endl;
                cout<<"Old Primal Residual for D: \n"<<pInter->PrimalResidual.norm()<<endl;           
                pInter->PrimalResidual=newPrimalResidual;
              

                
            }
            }
        }

    };
        
    void  NewOversampling::updateDualResidual()
    {

    };
    void NewOversampling::runADMMIteration()
    {   
        //cout<<"runADMMIteration(0)"<<endl;
        for (auto i:Sequence)
        {   
            //cout<<i<<endl;
            VectorXd RHS;
            timing t0("get RHS");
            getRHS(i,RHS);
            cout<<"RHS"<<i<<" :\n"<<RHS<<endl;

            t0.end();
            //cout<<"runADMMIteration(1)"<<endl;
            timing t1("chol solve");
            vSS[i]->Position=vSS[i]->chol.solve(RHS);
            cout<<"first solution: \n"<<vSS[i]->Position<<endl;
            t1.end();  
            
            
            //cout<<"runADMMIteration(2)"<<endl;

        }
        timing t("update multiplier");
        updatePrimalResidual();
        updateMultiplier();   
        t.end();
        //cout<<"runADMMIteration(3)"<<endl;     
    }

    void NewOversampling::getRHS(int EleNum, VectorXd& RHS)
    {   
        auto pSS=vSS[EleNum];
        RHS.resize(pSS->n_DOF);
        RHS.setZero();

        for (auto pInter:pSS->vInter)
        {
            if (pInter->flag=='C')
            {
                auto p=static_cast<CrossElement*>(pInter);
                auto iDOF=p->mapDOF[pSS];
                auto pSS2=p->OppositeSubsystem[pSS];
                auto iDOF2=p->mapDOF[pSS2];
                RHS(iDOF)+=p->rho* p->W*p->W*
                        (pSS2->Position(iDOF2)-*
                        (p->mapMultiplier[pSS]));

            }else if(pInter->flag=='D')
            {
                auto p=static_cast<DirichletInterface*>(pInter);
               
                auto iDOF= p->vDOF;
                
                
                RHS(iDOF)+=p->rho* p->W*p->W*
                        (p->DirichletBoundary-
                         p->Multiplier);
              

                
            }else if(pInter->flag=='N')
            {
                auto p=static_cast<NeummanInterface*>(pInter);
                auto iDOF=p->vDOF;
                RHS(iDOF)+=p->NeummanBoundary;

            };
            

        }

    };

    void NewOversampling::updateMultiplier()
    {   
        for(auto pS:vSS)
        { 
            for(auto pI:pS->vInter)
            {            
                
                if(pI->flag=='C')
                {
                 
                    auto p=static_cast<CrossElement*>(pI);
                    auto iDOF= p->mapDOF[pS];
                    auto pS2=p->OppositeSubsystem[pS];
            

                    auto iDOF2=p->mapDOF[pS2];
                  
                    auto X1=pS->Position(iDOF);
                    auto X2=pS2->Position(iDOF2);
                    

                    *p->mapMultiplier[pS]+=X1-X2;
                  
                  

                    
                }else if(pI->flag=='D')
                {
                    
                    auto p=static_cast<DirichletInterface*>(pI);
                    auto iDOF=p->vDOF;
                    auto X=pS->Position(iDOF);
                    p->Multiplier+=X-p->DirichletBoundary;
                };
                
            }
        }
        
    }

    

};
#endif // __ADMM_H__