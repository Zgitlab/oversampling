#pragma once
#include<mains/MatrixCoefficients/HarmonicDisplacement.hpp>

#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>

#include <mains/2DFEM/Materials/Material_StVK.hpp>
#include <mains/2DFEM/Mesh.hpp>
#include <mains/2DFEM/System.hpp>
#include <mains/2DFEM/Integrator.hpp>
#include <mains/2DFEM/BoundaryConditions/Dirichlet.hpp>
#include <mains/2DFEM/BoundaryConditions/Gravity.hpp>

#include <mains/2DFEM/Integrators/NewtonStaticB.hpp>
#include <mains/2DFEM/Integrators/NewtonStaticCoarse.hpp>
#include <mains/2DFEM/Integrators/GradientDescent.hpp>

#include <mains/MatrixCoefficients/HierachyMesh.hpp>
#include <mains/MatrixCoefficients/OnlyKD.hpp>

#include <mains/NumericalCoarsening/CoarseElement.hpp>
#include <mains/NumericalCoarsening/CoarseningSystem.hpp>

#include<mains/ADMM/ADMM.hpp>
#include<mains/ADMM/Oversampling2D.hpp>
#include<mains/ADMM/util.hpp>

#include<mains/LSQ/LagrangianLSQ.hpp>

#include <chrono> 
#include <utils/io/writeMatrix.hpp>
#include <utils/io/readCSV.hpp>
#include <utils/SparseMatrixIndex.hpp>
#include <utils/SparseMatrixSlicing.hpp>
#include <utils/timing.hpp>
namespace ADMM{
void setSubSystemNeuman(vector<Material_StVK> M, 
                  vector<ADMM::Subsystem*>& vADMMSubsystem,
                  SpMat& spA,
                  int&   fpc, int& fepc,
                  vector<map<int,int>>& vAll2Active,
                  vector<map<int,int>>& vActive2All,
                  int& OccupiedCoarseElementCount,
                  vector<SpMat>& vSubPattern)
{   
        MatrixXd A = utils::load_csv<MatrixXd>("MatlabData/CoarseElementDistribution.csv");
        spA= A.sparseView();
        OccupiedCoarseElementCount= spA.nonZeros();
        spA.makeCompressed();


        vector<int> vcx,vcy,vc;//vc global index of a submesh c=ny*x+y
        

        string str= "MatlabData/sub"+ to_string(0)+ ".csv";
        MatrixXd Sub = utils::load_csv<MatrixXd>(str);
        fepc=Sub.cols();
        fpc= fepc+1;

        int dim=2;

    //mapping of Active node index in submesh   
        vector<QuadRectangleMesh*> vpMesh;
        vector<System*> vSystem;
        vector<vector<int>> vvActiveNodes;

        cout<<"testCSV(2)"<<endl;

 //setting material and system etc.
        for (int i=0; i<OccupiedCoarseElementCount;i++)
        {   
            
            cout<<"testCSV(3)"<<endl;

            int row,col;
            int Ny=spA.cols();
            RowAndColForAnEntry(spA, i, row,  col);
            vcx.push_back(row);
            vcy.push_back(col);
            vc.push_back(Ny*row+col);
            cout<<"testCSV(4)"<<endl;

            string str= "MatlabData/sub"+ to_string(vc[i])+ ".csv";
            MatrixXd Sub = utils::load_csv<MatrixXd>(str);
            SpMat spSub= Sub.sparseView();
            vSubPattern.push_back(spSub);

            /////////// manipulation inside the submesh ////////////////
            ////////////////////////////////////////////////////////////
            vpMesh.push_back(new QuadRectangleMesh(fpc,fpc,1));
            vector<Material*> vMaterial;
            for(int j=0; j<fepc*fepc;j++)
            {
                auto  a= new Material_StVK();
                double Youngs=  0;
                double Poisson= 0;
                a->setMaterialFromYP(Youngs,Poisson, 0.0);
                vMaterial.push_back(a);   
                cout<<"testCSV(5)"<<endl;             
            };
            
            for (int k=0; k<spSub.outerSize(); ++k)
            {
                for (SparseMatrix<double>::InnerIterator it(spSub,k); it; ++it)
                {               
                    int x=it.row();   // row index
                    int y=it.col();  
                    int m= round( it.value());
                    vMaterial[fepc*x+y]=&M[m]; 
                }
            }
            cout<<vMaterial.size()<<endl;

            vSystem.push_back(new System(vpMesh[i],vMaterial));

            ///////Pass the FEM system of submesh to ADMM subsystem/////////
            ////////////////////////////////////////////////////////////////
            
            SpMat H,_H;
            vSystem[i]->getHessian(_H);
            vector<int> vA,vDOF,vxp,vxm,vyp,vym,
            vExtra,
            vDOFxp,vDOFxm,vDOFyp,vDOFym,
            vDOFExtra;
            map<int,int> Active2All;
            map<int,int> All2Active;
            ADMM::getActiveIndex(vA,spSub,Active2All, All2Active);
            vAll2Active.push_back(All2Active);
            vActive2All.push_back(Active2All);
            cout<<"testCSV(6)"<<endl;    

            ADMM::getActiveBoundaryNodesIndex(vxp,vxm,vyp,vym,spSub);
            ADMM::getInteranlBoundaryNodesIndex(vExtra,spSub);

            vDOF=ADMM::Node2DOF(vA);
            vDOFxp=ADMM::Node2DOF(vxp);
            vDOFxm=ADMM::Node2DOF(vxm);
            vDOFyp=ADMM::Node2DOF(vyp);
            vDOFym=ADMM::Node2DOF(vym);
            vDOFExtra=ADMM::Node2DOF(vExtra);
            cout<<"testCSV(8.1)"<<endl;  
           
            _H.makeCompressed();
         
            H=Eigen::MatrixSlicing(vDOF,vDOF,-_H);//remember that the matrix index is not
            // compatible with the nodes and DOF index
             
           
            cout<<"testCSV(9)"<<endl;    
            
            auto pSS=new ADMM::Subsystem(H,dim);
            vADMMSubsystem.push_back(pSS);       
            //till now, the interface is not yet set;
            //Interface rough setting (without matching) 
            cout<<"testCSV(10)"<<endl;    
            pSS->vInter.push_back(new ADMM::Interface(vDOFxp));//use the global as local DOF buffer
            pSS->vInter.push_back(new ADMM::Interface(vDOFxm));
            pSS->vInter.push_back(new ADMM::Interface(vDOFyp));//use the global as local DOF buffer
            pSS->vInter.push_back(new ADMM::Interface(vDOFym));
            pSS->vInter.push_back(new ADMM::Interface(vDOFExtra));

        }

      
        map<int,int> MapGlobalSubmeshIndexToRealIndex;
        for (int i = 0; i < vc.size(); ++i)
        {
            MapGlobalSubmeshIndexToRealIndex[vc[i]]=i;            
        }
        cout<<"vc"<<endl;
        for(int i: vc)
        {
            cout<<i<<endl;
        }
        cout<<"testCSV(11)"<<endl;    
        cout<<vADMMSubsystem.size()<<endl;
        auto a=(vADMMSubsystem[1]->vInter[1]);
        cout<<a->globalDOF.size()<<endl;

        cout<<"map "<<endl;
        auto myMap= MapGlobalSubmeshIndexToRealIndex;
        for(auto it = myMap.cbegin(); it != myMap.cend(); ++it)
        {
            std::cout << it->first << " " << it->second << "\n";
        }   

        cout<<"Local DOF1"<<endl;
        for(int k: a->globalDOF)
        {
            cout<<k<<endl;
        }

        //setting interface
        for (int k=0; k<spA.outerSize(); ++k)
        {   
            //Cross element interface setting
            for (SparseMatrix<double>::InnerIterator it(spA,k); it; ++it)
            {       
                    int xc, yc;
                    xc= it.row();
                    yc= it.col();
                    cout<<"xc "<<xc<<"yc "<<yc<<endl;

                    int Nx= spA.rows();
                    int Ny= spA.cols();
                    cout<<"Nx "<<Nx<<"Ny "<<Ny<<endl;
                    if ((xc+1<Nx)&(spA.coeff(xc+1,yc)!=0))
                    {   
                        int ig =Ny*(xc)+yc;
                        int ig1=Ny*(xc+1)+yc;
                        cout<<" ig  "<< ig<<endl;
                        cout<<"ig1 "<<ig1<<endl;
                        int ic=MapGlobalSubmeshIndexToRealIndex[ig];
                        int ic1=MapGlobalSubmeshIndexToRealIndex[ig1];
                        cout<<" ic  "<< ic<<endl;
                        cout<<"ic1 "<<ic1<<endl;
                        // build map between global index and local index of fine nodes on the boundary
                        auto LocalDOF=vADMMSubsystem[ic]->vInter[0]->globalDOF;//using the buffered local DOF
                        auto LocalDOF1=vADMMSubsystem[ic1]->vInter[1]->globalDOF;
                        
                        cout<<"Local DOF"<<endl;
                        for(int& k: LocalDOF)
                        {   
                            cout<<k<<endl;                            
                        }
                        cout<<"Local Node"<<endl;
                        auto LocalNode= ADMM::DOF2Node(LocalDOF);
                        for(int& k: LocalNode)
                        {   
                            cout<<"before"<<k<<endl;
                            k=vActive2All[ic][k];
                            cout<<"after"<<k<<endl;                       
                        }
                        LocalDOF=ADMM::Node2DOF(LocalNode);
                        for(int l: LocalDOF)
                        {
                            cout<<l<<endl;
                        }
                        cout<<"Local DOF1"<<endl;
                        auto LocalNode1= ADMM::DOF2Node(LocalDOF1);

                        for(int& k: LocalNode1)
                        {   cout<<"before"<<k<<endl;
                            k=vActive2All[ic1][k];
                            cout<<"after"<<k<<endl;
                            
                        }

                        LocalDOF1=ADMM::Node2DOF(LocalNode1);
                        

                        vector<int> GlobalDOF, GlobalDOF1,OverlapGlobal,
                        Difference,Difference1, newLocal,newLocal1,
                        LocalDifference,LocalDifference1;

                        map<int,int> mapG2L, mapG2L1;

                        ADMM::mapLocalDOFToGlobal(mapG2L,GlobalDOF, LocalDOF,
                                                    xc,yc,fpc,spA);
                        cout<<"Global DOF"<<endl;

                        for(int l: GlobalDOF)
                        {
                            cout<<l<<endl;
                        }

                        ADMM::mapLocalDOFToGlobal(mapG2L1,GlobalDOF1, LocalDOF1,
                                                    xc+1,yc,fpc,spA);
                        cout<<"Global DOF1"<<endl;
                        for(int l: GlobalDOF1)
                        {
                            cout<<l<<endl;
                        }

                        OverlapGlobal=ADMM::intersection(GlobalDOF,GlobalDOF1);
                        cout<<"OVerlap"<<endl;
                        for(int l: OverlapGlobal)
                        {
                            cout<<l<<endl;
                        }

                        std::set_difference(GlobalDOF.begin(), GlobalDOF.end(), 
                         OverlapGlobal.begin(), OverlapGlobal.end(),
                         std::inserter(Difference, Difference.begin()));

                        std::set_difference(GlobalDOF1.begin(), GlobalDOF1.end(), 
                         OverlapGlobal.begin(), OverlapGlobal.end(),
                         std::inserter(Difference1, Difference1.begin()));
                        
                        for(int i:OverlapGlobal)
                        newLocal.push_back(mapG2L[i]);     
                        
                        for(int i:OverlapGlobal)
                        newLocal1.push_back(mapG2L1[i]);
                        cout<<"GlobalOverlap size: "<< OverlapGlobal.size()<<endl;
                        cout<<"LocalDOF size: "<<newLocal.size()<<endl;
                        cout<<"LocalDOF1 size: "<<newLocal1.size()<<endl;
                        

                         cout<<"local DOF"<<endl;
                         auto newNode= ADMM::DOF2Node(newLocal);
                         auto newNode1= ADMM::DOF2Node(newLocal1);
                        for(int& l: newNode)
                        {
                            
                            l=vAll2Active[ic][l];
                            cout<<l<<endl;
                        }
                         cout<<"local DOF1"<<endl;
                        for(int& l: newNode1)
                        {
                            l=vAll2Active[ic1][l];
                            cout<<l<<endl;
                        }
                        newLocal=ADMM::Node2DOF(newNode);
                        newLocal1=ADMM::Node2DOF(newNode1);

                        auto CrossElement= new ADMM::CrossElement(vADMMSubsystem[ic],vADMMSubsystem[ic1],
                        newLocal,newLocal1,OverlapGlobal);

                        cout<<"testCSV(13.3)"<<endl; 
                        vADMMSubsystem[ic]->vInter[0]=CrossElement;
                        vADMMSubsystem[ic1]->vInter[1]=CrossElement;
                        cout<<"testCSV(13.4)"<<endl; 
                        CrossElement->ModifyHessian();
                        cout<<"testCSV(13.5)"<<endl; 

                        ADMM::GlobalDOF2LocalRealDOF(LocalDifference, Difference, mapG2L, vAll2Active[ic]);
                        ADMM::GlobalDOF2LocalRealDOF(LocalDifference1, Difference1, mapG2L1, vAll2Active[ic1]);
           
   

                        auto g= &vADMMSubsystem[ic]->vInter[4]->globalDOF;
                        g->insert(g->end(), 
                                LocalDifference.begin(),LocalDifference.end());  
                        
                        sort(g->begin(),g->end());
                        g->erase(unique(g->begin(),g->end()),g->end());

                        auto g1= &vADMMSubsystem[ic1]->vInter[4]->globalDOF;
                        g1->insert(g1->end(), 
                                LocalDifference1.begin(),LocalDifference1.end());                   

                        sort(g1->begin(),g1->end());
                        g1->erase(unique(g1->begin(),g1->end()),g1->end());
            

                    }else
                    {
                        int ig =Ny*(xc)+yc;
                        int ic=MapGlobalSubmeshIndexToRealIndex[ig];
                        auto p0= &vADMMSubsystem[ic]->vInter[0]->globalDOF;
                        auto p4= &vADMMSubsystem[ic]->vInter[4]->globalDOF;
                        p4->insert(p4->end(), p0->begin(), p0->end());
                        p0->clear();
                        
                    }
                    cout<<"testCSV(12)"<<endl;    

                    if ((yc+1<Ny)&(spA.coeff(xc,yc+1)!=0))
                    {  
                        int ig =Ny*(xc)+yc;
                        int ig1=Ny*(xc)+yc+1;
                        int ic=MapGlobalSubmeshIndexToRealIndex[ig];
                        int ic1=MapGlobalSubmeshIndexToRealIndex[ig1];
                        // build map between global index and local index of fine nodes on the boundary
                        auto LocalDOF=vADMMSubsystem[ic]->vInter[2]->globalDOF;//using the buffered local DOF
                        auto LocalDOF1=vADMMSubsystem[ic1]->vInter[3]->globalDOF;

                        cout<<"Local DOF"<<endl;
                        for(int& k: LocalDOF)
                        {   
                            cout<<k<<endl;                            
                        }
                        cout<<"Local Node"<<endl;
                        auto LocalNode= ADMM::DOF2Node(LocalDOF);
                        for(int& k: LocalNode)
                        {   
                            cout<<"before"<<k<<endl;
                            k=vActive2All[ic][k];
                            cout<<"after"<<k<<endl;                       
                        }
                        LocalDOF=ADMM::Node2DOF(LocalNode);
                        for(int l: LocalDOF)
                        {
                            cout<<l<<endl;
                        }
                        cout<<"Local DOF1"<<endl;
                        auto LocalNode1= ADMM::DOF2Node(LocalDOF1);

                        for(int& k: LocalNode1)
                        {   cout<<"before"<<k<<endl;
                            k=vActive2All[ic1][k];
                            cout<<"after"<<k<<endl;
                            
                        }

                        LocalDOF1=ADMM::Node2DOF(LocalNode1);

                        vector<int> GlobalDOF, GlobalDOF1,OverlapGlobal,
                        Difference,Difference1, newLocal,newLocal1,
                        LocalDifference,LocalDifference1;

                        map<int,int> mapG2L, mapG2L1;

                        ADMM::mapLocalDOFToGlobal(mapG2L,GlobalDOF, LocalDOF,
                                                    xc,yc,fpc,spA);

                        ADMM::mapLocalDOFToGlobal(mapG2L1,GlobalDOF1, LocalDOF1,
                                                    xc,yc+1,fpc,spA);
                        
                        cout<<"Global DOF"<<endl;
                        for(int l: GlobalDOF)
                        {
                            cout<<l<<endl;
                        }

                         cout<<"Global DOF1"<<endl;
                        for(int l: GlobalDOF1)
                        {
                            cout<<l<<endl;
                        }

                        OverlapGlobal=ADMM::intersection(GlobalDOF,GlobalDOF1);

                        std::set_difference(GlobalDOF.begin(), GlobalDOF.end(), 
                         OverlapGlobal.begin(), OverlapGlobal.end(),
                         std::inserter(Difference, Difference.begin()));

                        std::set_difference(GlobalDOF1.begin(), GlobalDOF1.end(), 
                         OverlapGlobal.begin(), OverlapGlobal.end(),
                         std::inserter(Difference1, Difference1.begin()));
                        cout<<"testCSV(13)"<<endl;    
                        for(int i:OverlapGlobal)
                        newLocal.push_back(mapG2L[i]);     
                        
                        for(int i:OverlapGlobal)
                        newLocal1.push_back(mapG2L1[i]);
                        cout<<"GlobalOverlap size: "<< OverlapGlobal.size()<<endl;
                        cout<<"LocalDOF size: "<<newLocal.size()<<endl;
                        cout<<"LocalDOF1 size: "<<newLocal1.size()<<endl;
                        

                         cout<<"local DOF"<<endl;
                         auto newNode= ADMM::DOF2Node(newLocal);
                         auto newNode1= ADMM::DOF2Node(newLocal1);
                        for(int& l: newNode)
                        {
                            
                            l=vAll2Active[ic][l];
                            cout<<l<<endl;
                        }
                         cout<<"local DOF1"<<endl;
                        for(int& l: newNode1)
                        {
                            l=vAll2Active[ic1][l];
                            cout<<l<<endl;
                        }
                        newLocal=ADMM::Node2DOF(newNode);
                        newLocal1=ADMM::Node2DOF(newNode1);

                        auto CrossElement= new ADMM::CrossElement(vADMMSubsystem[ic],vADMMSubsystem[ic1],
                        newLocal,newLocal1,OverlapGlobal);

                        cout<<"testCSV(13.3)"<<endl; 
                        vADMMSubsystem[ic]->vInter[2]=CrossElement;
                        vADMMSubsystem[ic1]->vInter[3]=CrossElement;
                        cout<<"testCSV(13.4)"<<endl; 
                        CrossElement->ModifyHessian();
                        cout<<"testCSV(13.5)"<<endl; 
                        ADMM::GlobalDOF2LocalRealDOF(LocalDifference, Difference, mapG2L, vAll2Active[ic]);
                        ADMM::GlobalDOF2LocalRealDOF(LocalDifference1, Difference1, mapG2L1, vAll2Active[ic1]);
           

                        auto g= &vADMMSubsystem[ic]->vInter[4]->globalDOF;
                        g->insert(g->end(), 
                                LocalDifference.begin(),LocalDifference.end());  
                        
                        auto g1= &vADMMSubsystem[ic1]->vInter[4]->globalDOF;
                        g1->insert(g1->end(), 
                                LocalDifference1.begin(),LocalDifference1.end());                   
                        sort(g->begin(),g->end());
                        g->erase(unique(g->begin(),g->end()),g->end());
                        sort(g1->begin(),g1->end());
                        g1->erase(unique(g1->begin(),g1->end()),g1->end());
            

                    }else
                    {
                        int ig =Ny*(xc)+yc;
                        int ic=MapGlobalSubmeshIndexToRealIndex[ig];
                        auto p2= &vADMMSubsystem[ic]->vInter[2]->globalDOF;
                        auto p4= &vADMMSubsystem[ic]->vInter[4]->globalDOF;
                        p4->insert(p4->end(), p2->begin(), p2->end());
                        p2->clear();
                        sort(p4->begin(),p4->end());
                        p4->erase(unique(p4->begin(),p4->end()),p4->end());
                        
                        
                    }
                    
                    cout<<"testCSV(15)"<<endl;    
                    if ((xc==0)||(spA.coeff(xc-1,yc)==0))
                    {
                        int ig =Ny*(xc)+yc;
                        int ic=MapGlobalSubmeshIndexToRealIndex[ig];
                        auto p1= &vADMMSubsystem[ic]->vInter[1]->globalDOF;
                        auto p4= &vADMMSubsystem[ic]->vInter[4]->globalDOF;
                        p4->insert(p4->end(), p1->begin(), p1->end());
                        p1->clear();
                        sort(p4->begin(),p4->end());
                        p4->erase(unique(p4->begin(),p4->end()),p4->end());
                        
                    }
                    if ((yc==0)||(spA.coeff(xc,yc-1)==0))
                    {
                        int ig =Ny*(xc)+yc;
                        int ic=MapGlobalSubmeshIndexToRealIndex[ig];
                        auto p3= &vADMMSubsystem[ic]->vInter[3]->globalDOF;
                        auto p4= &vADMMSubsystem[ic]->vInter[4]->globalDOF;
                        p4->insert(p4->end(), p3->begin(), p3->end());
                        p3->clear();
                        sort(p4->begin(),p4->end());
                        p4->erase(unique(p4->begin(),p4->end()),p4->end());
                        
                    }
                    // cout<<"testCSV(16)"<<endl;    
                    // int ig =Ny*(xc)+yc;
                    // cout<<"Ny "<<Ny<<endl;
                    // cout<<xc<<"  "<<yc<<endl;
                    // int ic=MapGlobalSubmeshIndexToRealIndex[ig];
                    // auto p4= &vADMMSubsystem[ic]->vInter[4]->globalDOF;
                    // auto pD4= new ADMM::DirichletInterface(
                    //              vADMMSubsystem[ic],*p4,*p4);

                    // cout<<"Extra DOF"<<endl;
                    // for(auto t: *p4)
                    // cout<<t<<endl;
                    // vADMMSubsystem[ic]->vInter[4]=pD4;
                    // pD4->ModifyHessian();      
                    // cout<<"testCSV(17)"<<endl;                                      
                
            }

        cout<<"testCSV(18)"<<endl;    

        }

        for(auto ss: vADMMSubsystem)
            {
                ss->BuildSolver();
                cout<<ss->Hessian<<endl;
            }
            
        
}};