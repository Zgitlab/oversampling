#pragma once
#include <vector>
#include <Eigen/Core>
namespace ADMM
{
    typedef SparseMatrix<double> SpMat;
    
    using namespace std;
    using namespace Eigen;
    vector<int> xplus(int fpc)// return the xplus DOF of a square mesh
    {   
        vector<int> v;
        for(int i= fpc*(fpc-1); i<fpc*fpc; i++)
        {
            v.push_back(2*i);
            v.push_back(2*i+1);
        };
        return v;
    };
    vector<int> xminus(int fpc)// return the xplus DOF of a square mesh
    {   
        vector<int> v;
        for(int i= 0; i<fpc; i++)
        {
            v.push_back(2*i);
            v.push_back(2*i+1);
        };
        return v;
    };
    vector<int> yplus(int fpc)
    {
        vector<int> v;
        for(int i= 0; i<fpc; i++)
        {
            int k=fpc*i+fpc-1;
            v.push_back(2*k);
            v.push_back(2*k+1);

            
        };
        return v;
        
    }

    vector<int> yminus(int fpc)
    {
        vector<int> v;
        for(int i= 0; i<fpc; i++)
        {
            int k=fpc*i;
            v.push_back(2*k);
            v.push_back(2*k+1);

        };
        return v;
        
    }
    void Crop(MatrixXd DirichletBoundary, VectorXd& Cropped, 
    int EleNum, int FaceNum,int fpc)//assign a piece of dirichlet Boundary into cropped
    {
        VectorXd D= DirichletBoundary.col(FaceNum);
        VectorXd D_Cropped(2*fpc);
        int iy=EleNum%3;
        int ix=EleNum/3;
        if (FaceNum<2)
        {
            D_Cropped= D.segment((2*(fpc-1)*iy),2*fpc);
        }
        else{
            D_Cropped= D.segment((2*(fpc-1)*ix),2*fpc);
        };
        Cropped=D_Cropped;
    }

    // function [DirichletBoundary ] = formDirichletShear(fpc, d)
    // %FORMDIRICHLET Summary of this function goes here
    // %   Detailed explanation goes here
    // DirichletBoundaryT=zeros((fpc-1)*3+1, 4);
    // DirichletBoundaryT(:,1)=d;
    // DirichletBoundaryT(:,3)=-d;
    // DirichletBoundaryT(:,2)=linspace(-d,d,(fpc-1)*3+1);
    // DirichletBoundaryT(:,4)=linspace(-d,d,(fpc-1)*3+1);

    // DirichletBoundary=zeros(2*((fpc-1)*3+1),4);
    // DirichletBoundary(2*(1:(fpc-1)*3+1)-1, :)=DirichletBoundaryT;
    // end

    MatrixXd formDirichletShear(int fpc, double d)
    {
        MatrixXd D;
        D.resize((fpc-1)*3+1, 4);
        D.setZero();
        VectorXd a=VectorXd::Ones((fpc-1)*3+1);
        D(all,2)=d*a;
        D(all,3)=-d*a;

        VectorXd b= VectorXd::LinSpaced((fpc-1)*3+1,-d,d);

        D(all,0)=b;
        D(all,1)=b;

        //Interleave D into DirichletBoundary DOF

        vector<int> v;
        for (int i=0; i<((fpc-1)*3+1); i++)
        {   
            v.push_back(2*i+1);
        }

        MatrixXd DirichletBoundary(2*(fpc-1)*3+2,4);
        DirichletBoundary.setZero();
        DirichletBoundary(v, all)= D;
        return DirichletBoundary;
    }   
    void getActiveIndex(vector<int>& ActiveIndex, SpMat A)
    // get the active nodes index given a sparse matrix representing the elements
    {
        vector<int> Ac;
        int fpc= A.cols()+1;   
        for (int k=0; k<A.outerSize(); ++k)
        {
            for (SparseMatrix<double>::InnerIterator it(A,k); it; ++it)
            {
               
                int x=it.row();   // row index
                int y=it.col();   // col index (here it is equal to k)
                            
                Ac.push_back(fpc*x+y);
                Ac.push_back(fpc*x+y+1);
                Ac.push_back(fpc*(x+1)+y);
                Ac.push_back(fpc*(x+1)+y+1);
            }
        }
        sort(Ac.begin(), Ac.end());
        Ac.erase(unique(Ac.begin(),Ac.end()),Ac.end());
        ActiveIndex= Ac;
    }

     void getActiveIndex(vector<int>& ActiveIndex, SpMat A, 
     map<int,int>& Active2All, map<int,int>& All2Active)//ActiveIndex2allindex 
     {
         getActiveIndex(ActiveIndex, A);
         for (int i=0; i<ActiveIndex.size(); i++)
         {
            Active2All[i]=ActiveIndex[i];
            All2Active[ActiveIndex[i]]=i;

         }

     }

    void getActiveBoundaryNodesIndex(vector<int>& xp, vector<int>& xm, 
    vector<int>& yp, vector<int>& ym, SpMat A)
    // get the active nodes Local index on the boundary of a given sparse matrix representing the elements
    {   
        int fpc= A.cols()+1;   
        for (int k=0; k<A.outerSize(); ++k)
        {
            for (SparseMatrix<double>::InnerIterator it(A,k); it; ++it)
            {
                int x=it.row();   // row index
                int y=it.col();   // col index (here it is equal to k)
                if (x==0) 
                {
                    xm.push_back(fpc*x+y);
                    xm.push_back(fpc*x+y+1);
                }
                if(x==fpc-2)
                {
                    xp.push_back(fpc*(fpc-1)+y);
                    xp.push_back(fpc*(fpc-1)+y+1);
                }
                if (y==0) 
                {
                    ym.push_back(fpc*x+y);
                    ym.push_back(fpc*(x+1)+y);
                }
                if(y==fpc-2)
                {
                    yp.push_back(fpc*x+y+1);
                    yp.push_back(fpc*(x+1)+y+1);
                }

            };
        };
        sort(xm.begin(), xm.end());
        xm.erase( unique( xm.begin(), xm.end() ), xm.end() );

        sort(xp.begin(), xp.end());
        xp.erase( unique( xp.begin(), xp.end() ), xp.end() );

        sort(yp.begin(), yp.end());
        yp.erase( unique( yp.begin(), yp.end() ), yp.end() );

        sort(ym.begin(), ym.end());
        ym.erase( unique( ym.begin(), ym.end() ), ym.end() );      
    }

    void CheckNeighbor(int x, int y,vector<int>& vN,SpMat A)
    {   
        int Ny= A.cols()+1;
        int Nx= A.rows()+1;

        if ((x+1)<Nx-1)
        {        if(A.coeff(x+1,y)==0)
        {
            vN.push_back((x+1)*Ny+y);
            vN.push_back((x+1)*Ny+y+1);
        };};
        if ((y+1)<Ny-1)
        {if(A.coeff(x,y+1)==0)
        {
            vN.push_back((x)*Ny+(y+1));
            vN.push_back((x+1)*Ny+y+1);
        };};
        if (x>0) 
        {if(A.coeff(x-1,y)==0)
        {
            vN.push_back((x)*Ny+y);
            vN.push_back((x)*Ny+y+1);
        };};

        if (y>0)
        {
        if(A.coeff(x,y-1)==0)
        {
            vN.push_back((x)*Ny+y);
            vN.push_back((x+1)*Ny+y);
        };};


        
    }
    void getInteranlBoundaryNodesIndex(vector<int>& vN, SpMat A)
    {
        
        for (int k=0; k<A.outerSize(); ++k)
        {
            for (SparseMatrix<double>::InnerIterator it(A,k); it; ++it)
            {
                int x=it.row();   // row index
                int y=it.col();   // col index (here it is equal to k)
                CheckNeighbor(x,y,vN,A);

            }
        };
        sort(vN.begin(), vN.end());
        vN.erase( unique( vN.begin(), vN.end() ), vN.end() );
    }
    
    void getGlobalBoundaryNodesIndex(vector<int>& vN1, SpMat A)
    //    A is the global patern sparse matrix
    {
        vector<int> xp,xm,yp,ym,vN;


        getActiveBoundaryNodesIndex(xp,xm,yp,ym,A);

        for (int k=0; k<A.outerSize(); ++k)
        {
            for (SparseMatrix<double>::InnerIterator it(A,k); it; ++it)
            {
                int x=it.row();   // row index
                int y=it.col();   // col index (here it is equal to k)
                CheckNeighbor(x,y,vN,A);

            }
        };

        vN.insert(vN.end(), make_move_iterator(xp.begin()), make_move_iterator(xp.end()));
        vN.insert(vN.end(), make_move_iterator(xm.begin()), make_move_iterator(xm.end()));
        vN.insert(vN.end(), make_move_iterator(yp.begin()), make_move_iterator(yp.end()));
        vN.insert(vN.end(), make_move_iterator(ym.begin()), make_move_iterator(ym.end()));

        sort(vN.begin(), vN.end());
        vN.erase( unique( vN.begin(), vN.end() ), vN.end() );
        vN1=vN;
        
        
    }
    void mapGlobalIndex2Local(int xg, int yg, int& xl, int& yl, int fpc)
    {
        xl=xg%fpc;
        yl=yg%fpc;
    }
    void mapGlobalIndex2Local(int g, int& l, int fpc)
    {
        l=g%(fpc*fpc);
    }
    void mapLocalIndexToGlobal(int& xg, int&yg,int xf, 
    int yf,int xcg, int ycg, int fpc)
    {
        xg= xcg*(fpc-1)+xf;
        yg= ycg*(fpc-1)+yf;
    }
    void mapLocalIndexToGlobal(int& GlobalIndex,int LocalIndex
    ,int xcg, int ycg, int fpc,SpMat A)// A is the global patern sparse matrix
    {
        int xf= LocalIndex/fpc;
        int yf= LocalIndex%fpc;
        int xg, yg;
        mapLocalIndexToGlobal( xg, yg,xf,  yf,xcg, ycg, fpc);
        int Nyg= A.cols();
        GlobalIndex=  (Nyg*(fpc-1)+1)*xg+yg;
    }
    void mapLocalDOFToGlobal(int& GlobalDOF,int LocalDOF
    ,int xcg, int ycg, int fpc,SpMat A)// A is the global patern sparse matrix
    {   
        int LocalIndex=LocalDOF/2;
        int shift = LocalDOF%2;
        int xf= LocalIndex/fpc;
        int yf= LocalIndex%fpc;
        int xg, yg;
        mapLocalIndexToGlobal( xg, yg,xf,  yf,xcg, ycg, fpc);
        int Nyg= A.cols();
        int GlobalIndex=  (Nyg*(fpc-1)+1)*xg+yg;
        GlobalDOF=2*GlobalIndex+shift;
    }

    void mapLocalDOFToGlobal(vector<int>& vGlobalDOF,vector<int> vLocalDOF
    ,int xcg, int ycg, int fpc,SpMat A)// A is the global patern sparse matrix
    {   
       
        vector<int> vG;
        for(auto LocalDOF:vLocalDOF) 
        {
            int GlobalDOF;
            mapLocalDOFToGlobal(GlobalDOF,LocalDOF,xcg, ycg,fpc,A);        
            vG.push_back(GlobalDOF);
        }
       
        vGlobalDOF=vG;
    }



    void mapLocalDOFToGlobal(map<int,int>& mapG2L,vector<int>& vGlobalDOF,vector<int> vLocalDOF
    ,int xcg, int ycg, int fpc,SpMat A)
    {
        mapLocalDOFToGlobal(vGlobalDOF,vLocalDOF,xcg, ycg, fpc,A);
        for (size_t i = 0; i < vGlobalDOF.size(); ++i)
            mapG2L[vGlobalDOF[i]] = vLocalDOF[i];
    }
    vector<int> DOF2Node(vector<int> vDOF)
    {
        vector<int> v;
        for(int i:vDOF)
        {
            v.push_back(i/2);
            

        }
        sort(v.begin(), v.end());
        v.erase(unique(v.begin(),v.end()),v.end());
        return v;

        
    }
    vector<int> Node2DOF(int Node)
    {   
        vector<int> v;
        v.push_back(2*Node);
        v.push_back(2*Node+1);
        return v;
    }

    vector<int> Node2DOF(vector<int> vNode)
    {   
        vector<int> v;
        for(int i: vNode)
        {
            v.push_back(2*i);
            v.push_back(2*i+1);
        }
        return v; 
    }
    
    vector<int> intersection(std::vector<int> &v1,
                                      std::vector<int> &v2){
    std::vector<int> v3;

    std::sort(v1.begin(), v1.end());
    std::sort(v2.begin(), v2.end());

    std::set_intersection(v1.begin(),v1.end(),
                          v2.begin(),v2.end(),
                          back_inserter(v3));
    return v3;
}

void GlobalDOF2LocalRealDOF(vector<int>& LocalRealDOF, vector<int>GlobalDOF, map<int,int> GlobalDOF2ALocalDOF, map<int,int> All2Active)
{
   
   for(auto& t:GlobalDOF)
   {
       t= GlobalDOF2ALocalDOF[t];      
   }
   auto v= DOF2Node(GlobalDOF);
   for(auto& t: v)
   {
       t=All2Active[t];
   }
   LocalRealDOF= Node2DOF(v);
}

// void LocalRealDOF2GlobalDOF(vector<int>& GlobalDOF, vector<int> LocalRealDOF,  map<int,int> ALocalNode2GlobalNode, map<int,int> Active2All)
// {
//    auto v= DOF2Node(LocalRealDOF);
//    for(auto& t:v)
//    {
//         t=Active2All[t];
//         t=ALocalNode2GlobalNode[t];
//    }
//    GlobalDOF= Node2DOF(v);
// }

};