#pragma once
#include<mains/DataGeneration/HarmonicDisplacement.hpp>

#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>

#include <mains/2DFEM/Integrators/NewtonStaticB.hpp>
#include <mains/2DFEM/NumericalCoarsening/NewtonStaticCoarse.hpp>
#include <mains/2DFEM/Integrators/GradientDescent.hpp>

#include <mains/MatrixCoefficients/OnlyKD.hpp>

#include <mains/NumericalCoarsening/CoarseElement.hpp>
#include <mains/NumericalCoarsening/CoarseningSystem.hpp>

#include<mains/ADMM/ADMM.hpp>
#include<mains/ADMM/Oversampling2D.hpp>
#include<mains/ADMM/setSubSystem.hpp>
#include<mains/ADMM/util.hpp>

#include<mains/MatrixCoefficients/LSQ/LeastSquare.hpp>

#include <chrono> 
#include <utils/io/writeMatrix.hpp>
#include <utils/io/readCSV.hpp>
#include <utils/SparseMatrixIndex.hpp>
#include <utils/SparseMatrixSlicing.hpp>
#include <utils/timing.hpp>

namespace ADMMExample{
using namespace Eigen;
using namespace std;
    void testCSV()
    {   vector<Material_StVK> M(4);
        M[0].setMaterialFromYP(  0,   0,  0);
        M[1].setMaterialFromYP(1e6,0.49,1e3);
        M[2].setMaterialFromYP(1e6,0.48,1e3);
        M[3].setMaterialFromYP(1e4,0.45,1e3);

        vector<ADMM::Subsystem*> vADMMSubsystem;
        SpMat spA;
        int fpc, fepc;
        vector<map<int,int>> vAll2Active;
        vector<map<int,int>> vActive2All;
        int OccupiedCoarseElementCount;
        vector<SpMat> vSubPattern;

        ADMM::setSubSystem(M,vADMMSubsystem,spA,
            fpc, fepc,vAll2Active,
            vActive2All,OccupiedCoarseElementCount, vSubPattern);
        

        

      // Oversampling
        unique_ptr<ADMM::WholeDomainProcessing> pW (
            new ADMM::WholeDomainProcessing(vADMMSubsystem,spA,
            fpc, vAll2Active, vActive2All));

        vector<VectorXd> RFine;
        vector<vector<Vector2d>> RCoarse;

        vector<vector<vector<Matrix2d>>> vvvW(OccupiedCoarseElementCount);// first index: the coarse element number, second index:coarse node number, third index fine node number

        for(int c=0; c<OccupiedCoarseElementCount;c++)
        {

            vector<vector<Matrix2d>> vvW(4);
            for(auto&_vW:vvW)
            {
                _vW.resize(fpc*fpc);
            }


            pW->GoThroughScenario(c,0.1,RFine,RCoarse);//RCoarse in parametric order

        
            // Constrained Least Square
            vector<int> vvv={0,2,3,1};//change naive to parametric order;
           
            auto QuadMeshCoarse= QuadRectangleMesh(2,2,fepc);
            auto QuadMeshFine  = QuadRectangleMesh(fpc,fpc,1);
            vector<int> vCoarse{0, fpc-1,  fpc*(fpc-1),fpc*fpc-1 };
        

            for (int nFineNode=0;nFineNode<QuadMeshFine.N_nodes;nFineNode++)//should be convert to the active one after some point
            {

                if (find(vCoarse.begin(),vCoarse.end(),nFineNode)==vCoarse.end()) // fine node is not coarse node;
                {
                    vector<Vector2d> vX_H;
                    Vector2d X_h;

                    for(int i: vvv)
                    vX_H.push_back(QuadMeshCoarse.v_pCompleteListOfNodes[i]->getInitialPosition());

                    X_h=QuadMeshFine.v_pCompleteListOfNodes[nFineNode]->getInitialPosition();

                    unique_ptr<LSQ::ConstrainedLeastSquare> pLS(new LSQ::ConstrainedLeastSquare(vX_H,X_h));
                    pLS->CoarseData=RCoarse;
                    for(auto f:RFine)
                    {   
                        auto a = ADMM::Node2DOF(nFineNode);
                        pLS->FineData.push_back(f(a));
                    }
                    
                    pLS->Optimize();
                    cout<<"Weighting factor:"<<pLS->W<<endl;   
                    for(int k=0;k<4;k++)
                    {   
                        for(int i=0;i<2;i++)
                        {
                            for(int j=0; j<2;j++)
                            {                                
                                vvW[vvv[k]][nFineNode](i,j)=pLS->W(4*k+2*i+j);
                            }
                        }
                            
                    }                    
                }else{
                    auto it =find(vCoarse.begin(),vCoarse.end(),nFineNode);
                    int iC=distance(vCoarse.begin(),it);
                    for(int k=0;k<4;k++)
                    {
                        if (k==iC) vvW[k][nFineNode]=Matrix2d::Identity();
                        else  vvW[k][nFineNode]=Matrix2d::Zero();
                    }


                }

            }
            vvvW[c]=vvW;
        }

        for(int i=0; i<10;i++)
        {
            for(int k=0; k<9; k++)
                {
            for(int j=0; j<4;j++)
            {
                
                    cout<<"W"<< i<< j<< k<< ":\n"<< vvvW[i][j][k]<<endl;
                }
            }
        }

    //weights generated, start simulation setup;

        SpMat GlobalCoarsePattern,GlobalPattern;

        string str1= "MatlabData/CoarseElementDistribution.csv";        
        MatrixXd m1 = utils::load_csv<MatrixXd>(str1);
        GlobalCoarsePattern=m1.sparseView();

        string str2= "MatlabData/GlobalFineElementDistribution.csv";        
        MatrixXd m2 = utils::load_csv<MatrixXd>(str2);
        GlobalPattern=m2.sparseView();

        
        cout<<"testCSV 11"<<endl;
        auto pCoarseMesh=new ArbitraryQuadMesh(fepc*1.0,GlobalCoarsePattern);

        
        vector<CompositeMaterial*> v_pCMaterial;
        for(int i=0;i<OccupiedCoarseElementCount;i++)
        {
            auto pCMaterial= new CompositeMaterial();
            for(int j=0; j<fepc*fepc;j++)
            {   
                int x,y;
                x= j/fepc;
                y= j%fepc;
                int m= round(vSubPattern[i].coeff(x,y));
                pCMaterial->v_pMateiral.push_back(&M[m]);
            }
            v_pCMaterial.push_back(pCMaterial);
        }
        
                
        cout<<"testcsv 13"<<endl;
        cout<<"vvvW size"<<vvvW.size()<<"  vvW size"<< vvvW[0].size()<<"\n  vW.size() "<< vvvW[0][0].size()<<endl;
        auto pCoarseningSystem= new CoarseningSystem(pCoarseMesh, v_pCMaterial,vvvW,fpc);
        VectorXd p;
        pCoarseningSystem->getPosition(p);
        vector<int> indexDOF,indexDOFN,indexFineDOF,indexFineDOFN;
        vector<double> valueDOF,valueDOFN,valueFineDOFN;
        for(int i=0; i<8;i++)
        {
            indexDOF.push_back(i);
            indexDOFN.push_back(8+i);
            valueDOFN.push_back(1e6);
        }       
       
        
        auto pDirichlet= new Dirichlet(indexDOF,valueDOF,pCoarseningSystem->n_DOF);
        auto pNeumann= new Neumann(indexDOFN,valueDOFN);
        
        auto pGravity= new Gravity(0);
        
        
        auto pIntegrator= new NewtonStaticForCoarsening(pCoarseningSystem);

        pIntegrator->v_pBoundaryConditions.push_back(pGravity);
        pIntegrator->v_pBoundaryConditions.push_back(pNeumann);
        pIntegrator->v_pBoundaryConditions.push_back(pDirichlet);

        cout<<"testcsv 15"<<endl;
        Window window("Visualizer");
        window.setSize(1200, 700);
        DenseMatrixX V; // Vertices
        V.resize(pCoarseningSystem->n_Nodes,3);
        DenseMatrixXi E; // Edges, Face
        pCoarseMesh->getEdges(E);   
        cout<<"testcsv 16"<<endl;
        MeshTri3D mesh;
        mesh.setName("test mesh");
        mesh.updatePointBuffer(V);
        mesh.addEdgeLayer("Edges")->updateEdgeBuffer(E);

        while(!window.shouldClose())
        {   

            pIntegrator->SingleStep();
            pCoarseningSystem->getPosition(p);

            for(int i=0; i< pCoarseningSystem->n_Nodes;i++)
            {
                V.row(i) = Vec3(p(2*i), 0.0, p(2*i+1));
		
            }
            cout<<V<<endl;


            mesh.updatePointBuffer(V);

            window.clear();
            window.drawGrid();
            window.draw(&mesh);

            window.drawGui(&mesh);
            window.renderGui();
            window.swapBuffers();

            window.pollEvents();
            window.manageEvents();
        }







    }




};