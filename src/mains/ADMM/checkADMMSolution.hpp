#pragma once
#include<mains/MatrixCoefficients/HarmonicDisplacement.hpp>

#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>

#include <mains/2DFEM/Materials/Material_StVK.hpp>
#include <mains/2DFEM/Mesh.hpp>
#include <mains/2DFEM/System.hpp>
#include <mains/2DFEM/Integrator.hpp>
#include <mains/2DFEM/BoundaryConditions/Dirichlet.hpp>
#include <mains/2DFEM/BoundaryConditions/Gravity.hpp>

#include <mains/2DFEM/Integrators/NewtonStaticB.hpp>
#include <mains/2DFEM/Integrators/NewtonStaticCoarse.hpp>
#include <mains/2DFEM/Integrators/GradientDescent.hpp>

#include <mains/MatrixCoefficients/HierachyMesh.hpp>
#include <mains/MatrixCoefficients/OnlyKD.hpp>

#include <mains/NumericalCoarsening/CoarseElement.hpp>
#include <mains/NumericalCoarsening/CoarseningSystem.hpp>

#include<mains/ADMM/ADMM.hpp>
#include<mains/ADMM/Oversampling2D.hpp>
#include<mains/ADMM/setSubSystem.hpp>
#include<mains/ADMM/util.hpp>

#include<mains/LSQ/LagrangianLSQ.hpp>

#include <chrono> 
#include <utils/io/writeMatrix.hpp>
#include <utils/io/readCSV.hpp>
#include <utils/SparseMatrixIndex.hpp>
#include <utils/SparseMatrixSlicing.hpp>
#include <utils/timing.hpp>
#include <utils/Render.hpp>

#include <set>

namespace ADMM{
using namespace std;
using namespace Eigen;

vector<int> set2vec(set<int> a) 
{
    vector<int> v;
    v.assign(a.begin(), a.end());
    return v;
}

void checkADMMSolution()
{
    string str2= "MatlabData/GlobalFineElementDistribution.csv";        
    MatrixXd m2 = utils::load_csv<MatrixXd>(str2);
    const int fepc=2;
    const int fpc=fepc+1;
    MatrixXd materialDistribution=m2.block<3*fepc,3*fepc>(0,0);
    vector<Material_StVK> M(4);
    M[0].setMaterialFromYP(  0,   0,  0);
    M[1].setMaterialFromYP(1e9,0.499,1e3);
    M[2].setMaterialFromYP(1e9,0.499,1e3);
    M[3].setMaterialFromYP(1e9,0.499,1e3);
    // M[2].setMaterialFromYP(1e6,0.48,1e3);
    // M[3].setMaterialFromYP(1e6,0.49,1e3);
    set<int> indexInTheMiddleBlock, indexOnBoundary,indexMinusBoundary,indexAll,dofOnBoundary, dofInTheMiddleBlock, dofMinusBoundary;

    vector<Material*> vM(materialDistribution.size());
    for(int x=0; x<materialDistribution.rows();x++)
    {
        for(int y=0; y<materialDistribution.cols(); y++)
        {
            vM[materialDistribution.cols()*x+y]=&M[round(materialDistribution(x,y))];

        }
    }
      
    
    QuadRectangleMesh WholeMesh(3*fepc+1,3*fepc+1,1.0);
    for(int i=0; i<WholeMesh.N_nodes;i++)
    {
        indexAll.insert(i);
    }

    MatrixXi C;
    WholeMesh.getCircumferences(C);
    for (int i=0;i<C.size();i++)
    {
        indexOnBoundary.insert(C(i));

    };
    for(int x=0; x<=fpc;x++)
    {
        for(int y=0; y<=fpc; y++)
        {
            indexInTheMiddleBlock.insert(WholeMesh.ny*(x+fpc-1)+y+fpc-1);  
            dofInTheMiddleBlock.insert(2*(WholeMesh.ny*(x+fpc-1)+y+fpc-1)-1);
            dofInTheMiddleBlock.insert(2*(WholeMesh.ny*(x+fpc-1)+y+fpc-1));

        }
    };
    set_difference(
                   indexAll.begin(), indexAll.end(),
                   indexOnBoundary.begin(), indexOnBoundary.end(),
                   inserter(indexMinusBoundary,indexMinusBoundary.end())
    );
    vector<int> dofB = ADMM::Node2DOF(set2vec(indexOnBoundary));
    vector<int> dofNB= ADMM::Node2DOF(set2vec(indexMinusBoundary));
    Mesh* pWholeMesh= &WholeMesh;
    System sys(pWholeMesh,vM);
    SpMat H;
    sys.getHessian(H);
    H*=-1;
    auto LHS= MatrixSlicing(dofNB,dofNB,H);



    vector<ADMM::Subsystem*> vADMMSubsystem;
    SpMat spA;
    int fpc1, fepc1;
    vector<map<int,int>> vAll2Active;
    vector<map<int,int>> vActive2All;
    int OccupiedCoarseElementCount;
    vector<SpMat> vSubPattern;

    ADMM::setSubSystem(M,vADMMSubsystem,spA,
            fpc1, fepc1,vAll2Active,
            vActive2All,OccupiedCoarseElementCount, vSubPattern);
        

        
    for(auto ss:vADMMSubsystem)
    {
        cout<< ss->Hessian<<endl;
        //why i can not 
    };

    // Oversampling
    unique_ptr<ADMM::WholeDomainProcessing> pW (
            new ADMM::WholeDomainProcessing(vADMMSubsystem,spA,
            fpc, vAll2Active, vActive2All));


    vector<Vector2d> vD(4),vCD;
    VectorXd resultO;

    vD[0]<<0,-2;
    vD[1]<<0,-2;
    vD[2]<<0,2;
    vD[3]<<0,2;
    pW->OversampleAnElement(4,vD,resultO,vCD);

    
    VectorXd a(dofB.size());
    int count=0;
    for(int i:indexOnBoundary)
    {   
        Vector2d v2;
        pW->InterpolateDisplacement(i,fpc-1,v2,vD);
        a(ADMM::Node2DOF(count))=v2;
        count++;
    }

    cout<<H.rows()<<" H "<< H.cols()<<endl;
    cout<<"a"<<a<<endl;
    SpMat Hnb=MatrixSlicingToDense(dofNB,dofB,H);
    cout<<Hnb<<endl;
    VectorXd RHS=-Hnb*a;
    cout<<"RHS"<<RHS<<endl;
    SparseQR<SpMat,COLAMDOrdering<int>> solver(LHS);
    VectorXd c= solver.solve(RHS);
    cout<<"c result:"<<endl;
    cout<<c<<endl;
    VectorXd b(2*WholeMesh.N_nodes);
    b.setZero();
    b(dofNB)=c;
    cout<<b<<endl;
    auto resultC= b(set2vec(dofInTheMiddleBlock));
    cout<<resultC<<endl;
    cout<<"ResultO"<<resultO<<endl;
    
/*
    Render::RenderCoarseElement(resultC, fpc,1.0);
    Render::RenderCoarseElement(resultO, fpc,1.0);
    
    
*/


};

};