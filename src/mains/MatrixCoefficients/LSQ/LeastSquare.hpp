#pragma once 
#include<Eigen/Core>
#include<Eigen/QR>
#include<Eigen/LU>
#include<iostream>
namespace LSQ{

    using namespace Eigen;
    using namespace std;
    class ConstrainedLeastSquare{
    public:
        vector<vector<Vector2d>> CoarseData;//CoarseData[k][i] k:Data set#;  i: Node # i in parametric order
        vector<Vector2d> FineData;// FineData[k]

        vector<Vector2d> vX_H;//CoarsePosition;
        Vector2d X_h;//FinePosition;
        const int F=4;//fine node index number
        const vector<int> vC{0,1,2,3};// coarse node index number
        const int d=2;
        VectorXd W;
        
        void setKernel();
        void Optimize();
        MatrixXd Kernel;
        VectorXd W0;

        ConstrainedLeastSquare(vector<Vector2d> _vX_H, Vector2d _X_h)
        {
            vX_H=_vX_H;//naive order
            X_h= _X_h;
            
                        
            if (vX_H.size()==4)
            {
                setKernel();
                W.resize(16);
            }          
            else
            {
                cout<<"The number of Coarse nodes is incorrect!!!"<<endl;
            };             
                
        };        

    };

    void ConstrainedLeastSquare::setKernel()
    {
        auto dX=vX_H;
        for(auto& v:dX)
        {
            v=v-X_h;
            double a=v(1);
            v(1)=v(0);
            v(0)=-a;
            cout<<"dX"<<v<<endl;
        }

        MatrixXd LHS_PU(4, 16);
        MatrixXd LHS_RO(2, 16);
        MatrixXd LHS(6,16);
        VectorXd RHS(6);
        RHS<< 1,0,0,1,0,0;
        // w(4i+2k+l)=Wi(k,l)

        VectorXd w(16);
        for(int i=0;i<4;i++)
        {
            for(int k=0; k<2; k++)
            {
                for(int l= 0; l<2;l++)
                {
                    LHS_PU(2*k+l,4*i+2*k+l)=1;
                    LHS_RO(k,4*i+2*k+l)= dX[i](l);

                }
            }

        }

        LHS.topRows(4)=LHS_PU;
        LHS.bottomRows(2)=LHS_RO;
        FullPivLU<Matrix<double,6,16>> LU(LHS);

        W0= LU.solve(RHS);
        cout<<"W0"<<endl;
        cout<<W0<<endl;
        cout<<"LHS"<<endl;
        cout<<LHS<<endl;

        auto QR_Kernel=LHS.transpose().colPivHouseholderQr();
        MatrixXd Q=QR_Kernel.matrixQ();
        Kernel= Q.rightCols(LHS.cols()-QR_Kernel.rank());
        cout<<"Kernel Set"<<endl;
        cout<<Kernel<<endl;
    }


void ConstrainedLeastSquare::Optimize()
{  
    MatrixXd LHS(Kernel.cols(),Kernel.cols());
    VectorXd RHS(Kernel.cols());
    LHS.setZero();
    RHS.setZero();
    for(int n=0; n<CoarseData.size(); n++)
    {   
        MatrixXd D(2,16);
        D.setZero();
        for(int i=0;i<4;i++)
        {
            for(int k=0; k<2;k++)
            {
                for(int l=0; l<2; l++)
                {
                    D(k,4*i+2*k+l)= CoarseData[n][vC[i]](l);                                        
                }
            }
        }

        LHS+= Kernel.transpose()*D.transpose()*D*Kernel;
        Vector2d v= D*W0- FineData[n];
        RHS-= Kernel.transpose()*D.transpose()*v;
    }
    
    PartialPivLU<MatrixXd> solver;
    solver.compute(LHS);
    VectorXd q= solver.solve(RHS);
    W=Kernel*q+W0;


}

};
