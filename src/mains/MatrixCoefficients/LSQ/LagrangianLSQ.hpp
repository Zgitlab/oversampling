#pragma once 
#include<Eigen/Core>
#include<Eigen/QR>
#include<Eigen/LU>
#include<iostream>
namespace LLSQ{

    using namespace Eigen;
    using namespace std;
    class ConstrainedLeastSquare{
    public:
        vector<vector<Vector2d>> CoarseData;//CoarseData[k][i] k:Data set#;  i: Node # i in parametric order
        vector<Vector2d> FineData;// FineData[k]

        vector<Vector2d> vX_H;//CoarsePosition;
        Vector2d X_h;//FinePosition;
        const int F=4;//fine node index number
        const vector<int> vC{0,1,2,3};// coarse node index number
        const int d=2;
        VectorXd W;
        
        void setL();
        void Optimize();
        MatrixXd L;
        VectorXd W0;
        VectorXd r;

        ConstrainedLeastSquare(vector<Vector2d> _vX_H, Vector2d _X_h)
        {
            vX_H=_vX_H;//naive order
            X_h= _X_h;
            
                        
            if (vX_H.size()==4)
            {
                setL();
                W.resize(16);
            }          
            else
            {
                cout<<"The number of Coarse nodes is incorrect!!!"<<endl;
            };             
                
        };        

    };

//     void ConstrainedLeastSquare::setL()
//     {
//         auto dX=vX_H;
//         for(auto& v:dX)
//         {
//             v=v-X_h;
//             double a=v(1);
//             v(1)=v(0);
//             v(0)=-a;
//             cout<<"dX"<<v<<endl;
//         }

//         MatrixXd LHS_PU(4, 16);
//         MatrixXd LHS_RO(2, 16);// todo set zeros
//         L.resize(6,16);
//         r.resize(6);
//         r<< 1,0,0,1,0,0;
//         // w(4i+2k+l)=Wi(k,l)

//         VectorXd w(16);
//         for(int i=0;i<4;i++)
//         {
//             for(int k=0; k<2; k++)
//             {
//                 for(int l= 0; l<2;l++)
//                 {
//                     LHS_PU(2*k+l,4*i+2*k+l)=1;
//                     LHS_RO(k,4*i+2*k+l)= dX[i](l);

//                 }
//             }

//         }

//         L.topRows(4)=LHS_PU;
//         L.bottomRows(2)=LHS_RO;
//     }


// void ConstrainedLeastSquare::Optimize()
// {  
//     MatrixXd LHS(22,22);
//     VectorXd RHS(22);
//     LHS.setZero();
//     RHS.setZero();
//     for(int n=0; n<CoarseData.size(); n++)
//     {   
//         MatrixXd D(2,16);
//         D.setZero();
//         for(int i=0;i<4;i++)
//         {
//             for(int k=0; k<2;k++)
//             {
//                 for(int l=0; l<2; l++)
//                 {
//                     D(k,4*i+2*k+l)= CoarseData[n][vC[i]](l);                                        
//                 }
//             }
//         }

//         LHS.block<16,16>(0,0)+= 2*D.transpose()*D;
//         RHS.segment(0,16)+=2*D.transpose()*FineData[n];
//     }
//     LHS.block<6,16>(16,0)=L;
//     LHS.block<16,6>(0,16)=L.transpose();
//     RHS.segment(16,6)= r;
    
//     PartialPivLU<MatrixXd> solver;
//     solver.compute(LHS);
//     VectorXd Re= solver.solve(RHS);
//     W=Re.segment(0,16);

// }

};
