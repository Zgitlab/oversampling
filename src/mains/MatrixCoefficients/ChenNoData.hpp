#pragma once
#include <Eigen/Sparse>
#include <iostream>
#include <mains/2DFEM/Mesh.hpp>
using namespace std;
using namespace Eigen;
typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> T;
namespace NoData{

class ConstrainedOptimisation
{
public:
    SpMat H;
    SpMat A;
    int nRowA=0;
    SpMat LHS;
    std::vector<T> tripletListLHS;
    std::vector<T> tripletListLHSNoHD;
    VectorXd b;
    VectorXd RHS;
    vector<double> vRHS;// set RHS with a vector  

    QuadRectangleMesh* pReferenceMesh;
    int TCN=0; //total constraints number 
    int TCN_withoutHD;

    int fpc=3;//fpc: fine nodes number(coarse nodes also counted) for each coarse element dimension
    int Nc=4;//n_coarse_shape function per coarse element
    int Nf=9;//n_fine_shape function per coarse element
    int d=2;//space dimension;
    
    int Hs, n_Kronecker, n_Rotation, n_POU, n_Data;//size of H
    vector<vector<MatrixXd>> vvHD;

    bool HDisSet=false;
    MatrixXi FineInCoarse;// m*n  fine index numbering for each course element m:course element n: fine nodes per element

    vector<int> IndC{0,2,6,8};// 
    vector<int> IndF; //the effective fine coeficient indices in one coarse elements
    vector<int> ve;//the effective coeficient indices
    vector<int> vr;//the redundant coeficient indices

    VectorXd vecI;//identiy matrix 3*3 vectorized
    
    vector<VectorXd> n_ij;//solution of the coefficients for each coarse element
    vector<VectorXd> vlambda;

public:
    ConstrainedOptimisation(int _fpc);//_pHMesh is the fine mesh
    
    void setObjectiveFunction();
    void setConstraintsKD();//Kronecker Delta
    void setConstraintsPU();//Partition of Unity
    void setConstraintsRO();//Rotational Constraints
    void setConstraintsHD(int CoarseElementNumber);//Harmonic Displacement;
    void setLHS();
    VectorXd solve();
    void buildAll();
    vector<VectorXd> get_n_ij();
    vector<VectorXd> get_lambda();
    void print_n_ij();
};

ConstrainedOptimisation::ConstrainedOptimisation(int _fpc)
{   
    fpc=_fpc;
    pReferenceMesh=new QuadRectangleMesh(fpc,fpc,1.0);
    
    Nf=fpc*fpc;
    IndC[1]=fpc-1;
    IndC[2]=fpc*fpc-fpc;
    IndC[3]=fpc*fpc-1;


    int j=0;
    for(int i=0;i<fpc*fpc;i++)
    {   
        if (i!=IndC[j])
        {
            IndF.push_back(i);
        }
        else 
        {
            j++;
        };
    }

	//remove the redundant part
    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nf;j++)
        {   
            for(int k=0; k<d*d;k++)
            {   
                bool b= false;
                for(int l=0;l<Nc; l++)
                {
                    b+=(j==IndC[l]);
                };
                if (b) {vr.push_back(d*d*Nf*i+d*d*j+k);}
                else {ve.push_back(d*d*Nf*i+d*d*j+k);};                                                                                          
            };  
        };
    };

    Hs=d*d*Nc*Nf;// the size of matrix H
	
	n_Kronecker=d*d*Nc*Nc;
	
	n_POU= d*d*(Nf-Nc);
	
	if(d==2)
		{n_Rotation= 2*(Nf-Nc);}
	else //d==3
		{n_Rotation= 9*(Nf-Nc);};
	
	n_Data=0; d*(Nf-Nc)*3;//The last number is subject to change --> 2D harmonic(3) 3D harmonic(6)
	
    VectorXd _RHS(Hs+n_Kronecker + n_POU+ n_Rotation+n_Data);//Assume all the constraints are set up;
    _RHS.setZero();
    RHS=_RHS;   
    
    VectorXd _vecI(4);
    _vecI<< 1,0,0,1;
    vecI=_vecI;
};

void ConstrainedOptimisation::setObjectiveFunction()
{   
   
    MatrixXd vlavlavla;
    pReferenceMesh->getInnerProductOfDerivative(vlavlavla);//TODO not yet done
    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nf;j++)
        {   
            for(int k=0; k<d*d;k++)
            {  
                for(int l=0; l<Nf;l++)
                {  
                    int r=d*d*Nf*i+d*d*j+k;
                    int c=d*d*Nf*i+d*d*l+k;
                    tripletListLHS.push_back(T(r,c,vlavlavla(l,j)));
 
                };
            };
        };
    };


};

void ConstrainedOptimisation::setConstraintsKD()
{
    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<Nc;j++)
        {
            for(int k=0; k<d*d;k++)
            {
                 int s1= Hs+TCN+(d*d*Nc*i+d*d*j);//shift_row
                 int s2=d*d*Nf*i+d*d*IndC[j];//shift_col
                 tripletListLHS.push_back(T(s1+k,s2+k,1));
                 tripletListLHS.push_back(T(s2+k,s1+k,1));
            };
        };
    }; 

    

    for(int i=0;i<Nc; i++) 
    {
        for(int j=0;j<Nc; j++)
        {
            if(i==j)
            {
                RHS.segment(Hs+TCN +d*d*(i*Nc+j),d*d)=vecI;
            };
        };
    };

    TCN+= d*d*Nc*Nc;

}

void ConstrainedOptimisation::setConstraintsPU()
{
    for(int k=0; k<d*d;k++)
    {
        for(int j=0; j<(Nf-Nc); j++)
        {
            for(int i=0;i<Nc; i++)
            {  
                tripletListLHS.push_back(T(TCN+Hs+d*d*j+k,ve[i*(Nf-Nc)*d*d+d*d*j+k],1));

                tripletListLHS.push_back(T(ve[i*(Nf-Nc)*d*d+d*d*j+k],TCN+Hs+d*d*j+k,1));
            };
        };
    };

    //RHS 
    for(int j=0; j<(Nf-Nc);j++)
    {   
        RHS.segment(TCN+Hs+d*d*j,d*d)=vecI;
    };

    TCN+= d*d*(Nf-Nc);
    cout<<"TCN after PU"<<TCN<<endl;
}

void ConstrainedOptimisation::setConstraintsRO()
{
    MatrixXd  X;
    pReferenceMesh->getPositions(X);


         
    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<(Nf-Nc);j++)
        {   
            int s1= Hs+ TCN +j*2;//shift_row for 2d only
            int s2=ve[i*(Nf-Nc)*d*d+j*d*d];//shift_col
            Vector2d a=(X.row(IndC[i])-X.row(IndF[j])).transpose();//TODO not finished
            tripletListLHS.push_back(T(  s1,s2,-a(1)));//ve transfer nominal index to effective index
			tripletListLHS.push_back(T(  s1,s2+1, a(0)));
			tripletListLHS.push_back(T(s1+1,s2+2,-a(1)));
			tripletListLHS.push_back(T(s1+1,s2+3, a(0)));
            
            
            tripletListLHS.push_back(T(s2  ,  s1,-a(1)));
			tripletListLHS.push_back(T(s2+1,  s1, a(0)));
			tripletListLHS.push_back(T(s2+2,s1+1,-a(1)));
			tripletListLHS.push_back(T(s2+3,s1+1, a(0)));
            
        };
    };    
    //RHS
    //second equation 
    //Essentially zeros
    
    
    //Total equations set up 
	TCN += 2 * (Nf-Nc);
    cout<<"TCN after RO"<<TCN<<endl;
}

void ConstrainedOptimisation::setConstraintsHD(int CoarseElementNumber)
{   
    const int Nab=3;
    MatrixXd Hab[Nab];//displacement function within element
    
    if (!HDisSet)
        { tripletListLHSNoHD=tripletListLHS;
          TCN_withoutHD=TCN;
        }
        else
        {
          tripletListLHS=tripletListLHSNoHD;
        };
            
    for(int ab=0; ab<Nab ;ab++)
    {   
        Hab[ab]=vvHD[ab][CoarseElementNumber];
    };


    vector<int> IndF;
    int i=0;
    for (int j=0; j<Nf;j++)
    {
        if (j==IndC[i])
            {i++;}
        else
            {
                IndF.push_back(j);
            };
    };
    for(int i=0;i<Nc; i++)
    {   
        for(int j=0; j<(Nf-Nc);j++)
        {
            for(int ab=0; ab<Nab ;ab++)
            {
                int s1= Hs + TCN_withoutHD +d*j*Nab+d*ab;
                int s2= ve[4*(Nf-Nc)*i+4*j];
                
                tripletListLHS.push_back(T( s1,   s2,Hab[ab](IndC[i],0)-Hab[ab](IndF[j],0)));
                tripletListLHS.push_back(T( s1, s2+1,Hab[ab](IndC[i],1)-Hab[ab](IndF[j],1)));                            
                tripletListLHS.push_back(T(s1+1,s2+2,Hab[ab](IndC[i],0)-Hab[ab](IndF[j],0)));
                tripletListLHS.push_back(T(s1+1,s2+3,Hab[ab](IndC[i],1)-Hab[ab](IndF[j],1)));

                tripletListLHS.push_back(T(  s2,  s1,Hab[ab](IndC[i],0)-Hab[ab](IndF[j],0)));
                tripletListLHS.push_back(T(s2+1,  s1,Hab[ab](IndC[i],1)-Hab[ab](IndF[j],1)));
                tripletListLHS.push_back(T(s2+2,s1+1,Hab[ab](IndC[i],0)-Hab[ab](IndF[j],0)));
                tripletListLHS.push_back(T(s2+3,s1+1,Hab[ab](IndC[i],1)-Hab[ab](IndF[j],1)));
            };

        };
    };
   
    //RHS
	//Third equation
    //RHS moved to LHS

    if (!HDisSet)
        {
            TCN += 3 * (Nf-Nc) * 2;
            HDisSet=true;
            cout<<"TCN after HD"<<TCN<<endl;
        };
	    
    
}

void ConstrainedOptimisation::setLHS()
{   SpMat _LHS(Hs+TCN,Hs+TCN);
    _LHS.setFromTriplets(tripletListLHS.begin(), tripletListLHS.end());
    LHS=_LHS;
};
VectorXd ConstrainedOptimisation::solve()
{
    //SparseLU<SparseMatrix<double>,COLAMDOrdering<int>>   solver(LHS);
    SparseQR<SparseMatrix<double>,COLAMDOrdering<int>>   solver(LHS);
    //SimplicialLDLT<SparseMatrix<double>>   solver(LHS);
    VectorXd _n_ij;
    std::cout<<"solver"<<std::endl;
    _n_ij = solver.solve(RHS);
    return _n_ij;

};


void ConstrainedOptimisation::buildAll()
{   
    cout<<"buildAll started"<<endl;
    setObjectiveFunction();
    std::cout<<"OBJ"<<std::endl;
    setConstraintsKD();//Kronecker Delta
    std::cout<<"KD"<<std::endl;
    setConstraintsPU();//Partition of Unity
    std::cout<<"PU"<<std::endl;
    setConstraintsRO();//Rotational Constraints
    // std::cout<<"RO"<<std::endl;
    cout<<"LHS size\n"<< LHS.rows()<< "  "<<LHS.cols()<<endl;

        setLHS();

        cout<<"nRO"<<n_Rotation<<endl;
        cout<<"nData"<<n_Data<<endl;
        cout<<"nKD"<<n_Kronecker<<endl;
        cout<<"nPU"<<n_POU<<endl;
        std::cout<<"LHS:   "<< LHS.rows()<<"  "<<LHS.cols()<<std::endl;
        std::cout<<"RHS:   "<< RHS.rows()<<std::endl;
        std::cout<<"H Size:"<< Hs<<std::endl;
        std::cout<<"TCN:   "<< TCN<<std::endl;
        VectorXd s;
        s=solve();        
        
        n_ij.push_back(s.segment(0,Hs));
        vlambda.push_back(s.segment(Hs,s.rows()-Hs));
    
};

vector<VectorXd> ConstrainedOptimisation::get_n_ij()
{
    return n_ij;
};

vector<VectorXd> ConstrainedOptimisation::get_lambda()
{
    return vlambda;
};
};