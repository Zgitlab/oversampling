#pragma once
#include <Eigen/Sparse>
#include <iostream>
#include <mains/2DFEM/Mesh.hpp>
#include<Eigen/QR>
#include<Eigen/LU>

using namespace std;
using namespace Eigen;
typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> T;
namespace nearSmooth{
    class SmoothnessApproximation// for a single fine node
    {
        public:
        vector<vector<Vector2d>> CoarseData;//CoarseData[k][i] k:Data set#;  i: Node # i in parametric order
        vector<Vector2d> FineData;// FineData[k]

        vector<Vector2d> vX_H;//CoarsePosition; in parametric order
        Vector2d X_h;//FinePosition;
        const int F=4;//fine node index number
        const vector<int> vC{0,1,2,3};// coarse node index number
        const int d=2;
        VectorXd W;// in parametric order
        
        void setL();
        void Optimize();
        MatrixXd L;
        VectorXd W0;//referece weights 
        VectorXd r;

        SmoothnessApproximation(vector<Vector2d> _vX_H, Vector2d _X_h,VectorXd _W0,vector<vector<Vector2d>> _CoarseData,
        vector<Vector2d> _FineData)
        {
            vX_H=_vX_H;//parametric order
            X_h= _X_h;
            W0=_W0;            
            CoarseData=_CoarseData;
            FineData=_FineData;
            if (vX_H.size()==4)
            {
                setL();
                W.resize(16);
            }          
            else
            {
                cout<<"The number of Coarse nodes is incorrect!!!"<<endl;
            };             
                
        };     
    };   

    void SmoothnessApproximation::setL()
    {
        auto dX=vX_H;
        for(auto& v:dX)
        {
            v=v-X_h;
            double a=v(1);
            v(1)=v(0);
            v(0)=-a;
            cout<<"dX"<<v<<endl;
        }

        MatrixXd LHS_PU(4, 16);
        MatrixXd LHS_RO(2, 16);
        MatrixXd LHS_DA(6, 16);
        LHS_PU.setZero();
        LHS_RO.setZero();
        LHS_DA.setZero();
        vector<vector<Vector2d>> vvCminusF(3);
        for(int i=0;i<3;i++)
        {
            auto dCF=CoarseData[i];
            for(auto& d: dCF)
            {
                d-=FineData[i];
            }
            vvCminusF[i]=dCF;
        }

        L.resize(12,16);
        r.resize(12);
        r<< 1,0,0,1,0,0,
            0,0,0,0,0,0;
        // w(4i+2k+l)=Wi(k,l)

        VectorXd w(16);
        for(int i=0;i<4;i++)
        {
            for(int k=0; k<2; k++)
            {
                for(int l= 0; l<2;l++)
                {
                    LHS_PU(2*k+l,4*i+2*k+l)=1;
                    LHS_RO(k,4*i+2*k+l)= dX[i](l);

                    LHS_DA(k,4*i+2*k+l)  = vvCminusF[0][i](l);
                    LHS_DA(2+k,4*i+2*k+l)= vvCminusF[1][i](l);
                    LHS_DA(4+k,4*i+2*k+l)= vvCminusF[2][i](l);

                }
            }

        }

        L.topRows(4)=LHS_PU;
        L.block<2,6>(4,0)=LHS_RO;
        L.bottomRows(6)=LHS_DA;
        cout<<L<<endl;
        cout<<r<<endl;
    }


void SmoothnessApproximation::Optimize()
{  
    MatrixXd LHS(28,28);
    VectorXd RHS(28);
    LHS.setZero();
    RHS.setZero();
   

    LHS.block<16,16>(0,0).setIdentity();
    RHS.segment(0,16)=W0;
    
    LHS.block<12,16>(16,0)=L;
    LHS.block<16,12>(0,16)=L.transpose();
    RHS.segment(16,12)= r;
    
    PartialPivLU<MatrixXd> solver;
    solver.compute(LHS);
    VectorXd Re= solver.solve(RHS);
    W=Re.segment(0,16);

}
        
};