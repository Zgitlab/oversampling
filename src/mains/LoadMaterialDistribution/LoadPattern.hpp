#ifndef __LOADPATTERN_H__
#define __LOADPATTERN_H__
#include<Eigen/Core>
#include<utils/io/readCSV.hpp>
namespace LoadPattern{
    using namespace Eigen;
    using namespace std;
    
     MatrixXd GlobalPattern()
    {
        string str2= "MatlabData/GlobalFineElementDistribution.csv";        
        MatrixXd m2 = utils::load_csv<MatrixXd>(str2);
        return m2;
    }
    MatrixXd GlobalCoarsePattern()
    {
        string str1= "MatlabData/CoarseElementDistribution.csv";        
        MatrixXd m1 = utils::load_csv<MatrixXd>(str1);
        return m1;
    }
    
    
};
#endif // __LOADPATTERN_H__
