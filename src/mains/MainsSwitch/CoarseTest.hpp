#pragma once
#include<mains/MatrixCoefficients/HarmonicDisplacement.hpp>

#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>

#include <mains/2DFEM/Materials/Material_StVK.hpp>
#include <mains/2DFEM/Mesh.hpp>
#include <mains/2DFEM/System.hpp>
#include <mains/2DFEM/Integrator.hpp>
#include <mains/2DFEM/BoundaryConditions/Dirichlet.hpp>
#include <mains/2DFEM/BoundaryConditions/Gravity.hpp>
#include <mains/2DFEM/BoundaryConditions/Neumann.hpp>

#include <mains/2DFEM/Integrators/NewtonStaticB.hpp>
#include <mains/2DFEM/Integrators/NewtonStaticCoarse.hpp>
#include <mains/2DFEM/Integrators/GradientDescent.hpp>

// #include <mains/MatrixCoefficients/HierachyMesh.hpp>
#include <mains/MatrixCoefficients/OnlyKD.hpp>

#include <mains/NumericalCoarsening/CoarseElement.hpp>
#include <mains/NumericalCoarsening/CoarseningSystem.hpp>

#include <utils/io/readCSV.hpp>
#include <utils/SparseMatrixIndex.hpp>
#include <chrono> 

namespace Mains2
{   
    void testCoarseSystem()
    {
        using namespace Eigen;
        using namespace std;
        using namespace std::chrono; 

        SpMat GlobalCoarsePattern,GlobalPattern;

        string str1= "MatlabData/CoarseElementDistribution.csv";        
        MatrixXd m1 = utils::load_csv<MatrixXd>(str1);
        GlobalCoarsePattern=m1.sparseView();

        string str2= "MatlabData/GlobalFineElementDistribution.csv";        
        MatrixXd m2 = utils::load_csv<MatrixXd>(str2);
        GlobalPattern=m2.sparseView();
        vector<SpMat> vSubPattern;

        for (int i=0; i<GlobalCoarsePattern.nonZeros();i++)
        {   

            int row,col;
            int Ny=GlobalCoarsePattern.cols();
            RowAndColForAnEntry(GlobalCoarsePattern, i, row,  col);
            string str= "MatlabData/sub"+ to_string(Ny*row+col)+ ".csv";
            MatrixXd Sub = utils::load_csv<MatrixXd>(str);
            SpMat spSub= Sub.sparseView();
            vSubPattern.push_back(spSub);
        }

        Material_StVK M[4];
        M[0].setMaterialFromYP(  0,   0,  0);
        M[1].setMaterialFromYP(1e6,0.49,1e3);
        M[2].setMaterialFromYP(1e6,0.49,1e3);
        M[3].setMaterialFromYP(1e4,0.49,1e3);

        vector<Material*> vMGlobal;
        for(int i=0;i<GlobalPattern.nonZeros();i++)
        {           
                int x,y;
                RowAndColForAnEntry(GlobalPattern,i,x,y);
                int m= round(GlobalPattern.coeff(x,y));
                vMGlobal.push_back(&M[m]);
        }
                
        int fepc=GlobalPattern.rows()/GlobalCoarsePattern.rows();
        int fpc=fepc+1;
        auto pCoarseMesh=new ArbitraryQuadMesh((fpc-1)*1.0,GlobalCoarsePattern);
        auto pFineMesh= new ArbitraryQuadMesh(1.0,GlobalPattern);

        auto pCMaterial= new CompositeMaterial();
        vector<CompositeMaterial*> v_pCMaterial;
        for(int i=0;i<GlobalCoarsePattern.nonZeros();i++)
        {
            auto pCMaterial= new CompositeMaterial();
            for(int j=0; j<fepc*fepc;j++)
            {   
                int x,y;
                x= j/fepc;
                y= j%fepc;
                int m= round(vSubPattern[i].coeff(x,y));
                pCMaterial->v_pMateiral.push_back(&M[m]);
            }
            v_pCMaterial.push_back(pCMaterial);
        }
        
        

        auto pSystem= new System(pFineMesh,vMGlobal);
        SpMat NaiveHessian;
        pSystem->getHessian(NaiveHessian);

        HarmonicDisplacement hd(pFineMesh,pSystem);

        vector<vector<MatrixXd>> vvhd;
        hd.getvvHD(vvhd, GlobalCoarsePattern,fpc);
        
        cout<<vvhd[0][0]<<endl;

        auto pChen= new ConstrainedOptimisation(vvhd,fpc);
        pChen->buildAll();

        auto n_ij= pChen->get_n_ij();
        vector<vector<vector<Matrix2d>>> vW;// first index: the coarse element number, second index:coarse node number, third index fine node number
               
        for(int k=0; k<pCoarseMesh->N_cell;k++)
        {   
            vector<vector<Matrix2d>> arrayW;
            VectorXd v= n_ij[k];

            //transport the coefficients
            for(int i=0;i<4 ;i++)
            {   vector<Matrix2d> vm1;
                for(int j=0; j<fpc*fpc; j++)
                {
                    VectorXd vs= v.segment(4*fpc*fpc*i+j*4,4);
                    Matrix2d m;
                    m<<vs(0), vs(1),
                    vs(2), vs(3);
                    vm1.push_back(m);
                };
                arrayW.push_back(vm1);
            };
        
            vW.push_back(arrayW);
        }
        for(int i=0; i<10;i++)
        {
            for(int k=0; k<9; k++)
                {
            for(int j=0; j<4;j++)
            {
                
                    cout<<"W"<< i<< j<< k<< ":\n"<< vW[i][j][k]<<endl;
                }
            }
        }

        cout<<"test coarse system 3"<<endl;
        auto pCoarseningSystem= new CoarseningSystem(pCoarseMesh, v_pCMaterial,vW,fpc);
        VectorXd p;
        pCoarseningSystem->getPosition(p);


        // SpMat H;
        // pCoarseningSystem->getHessian(H);
        // cout<<"Hessian"<<H<<endl;
        // cout<<"Naive Hessian"<<NaiveHessian<<endl;

        cout<<"test coarse system 4"<<endl;
        vector<int> indexDOF,indexDOFN,indexFineDOF,indexFineDOFN;
        vector<double> valueDOF,valueDOFN,valueFineDOFN;
        int n_dof_coarse=pCoarseningSystem->n_DOF;
        for(int i=0; i<4;i++)
        {
            indexDOF.push_back(2*i);
            indexDOF.push_back(2*i+1);
            // indexDOFN.push_back(8+2*i);            
            // valueDOFN.push_back(1e6);
            // indexDOFN.push_back(8+2*i+1);  
            // valueDOFN.push_back(3.0e6);
            // indexDOFN.push_back(n_dof_coarse-16+2*i+1);
            // valueDOFN.push_back(-2.0e6);

        }
        int n_dof_fine=  2*pFineMesh->N_nodes;

        for(int i=0; i<31;i++)
        {   
            indexFineDOF.push_back(2*i);
            indexFineDOF.push_back(2*i+1);

            // indexFineDOFN.push_back(15+2*i);
            // valueFineDOFN.push_back(4.0/7*3.0e6);

            // indexFineDOFN.push_back(14+2*i);
            // valueFineDOFN.push_back(4.0/7*3.0e6);

            // indexFineDOFN.push_back(n_dof_fine-15-2*i);
            // valueFineDOFN.push_back(-4.0/7*2.0e6);
        }
       
        // VectorXd p0,dp;
        // pCoarseningSystem->getPosition(p0);
        // dp.resize(indexDOF.size());
        // dp<<0, 1,0, 1,0, 1,0, 1,
        //     0,-1,0,-1,0,-1,0,-1;
        // p0(indexDOF)+=0.5*dp;
        // pCoarseningSystem->setPosition(p0);
        auto pDirichlet= new Dirichlet(indexDOF,valueDOF,pCoarseningSystem->n_DOF);
        auto pNeumann= new Neumann(indexDOFN,valueDOFN);
        
        auto pGravity= new Gravity(10);


        auto pFineDirichlet=new Dirichlet(indexFineDOF,valueDOF,pSystem->n_DOF);
        auto pFineNeumann= new Neumann(indexFineDOFN,valueFineDOFN);
        
        auto pIntegrator= new NewtonStaticForCoarsening(pCoarseningSystem);
        auto pFineIntegrator= new NB(pSystem);

        pIntegrator->v_pBoundaryConditions.push_back(pGravity);
        pIntegrator->v_pBoundaryConditions.push_back(pNeumann);
        pIntegrator->v_pBoundaryConditions.push_back(pDirichlet);

        pFineIntegrator->v_pBoundaryConditions.push_back(pGravity);
        pFineIntegrator->v_pBoundaryConditions.push_back(pFineNeumann);
        pFineIntegrator->v_pBoundaryConditions.push_back(pFineDirichlet);

        cout<<"test coarse system 5"<<endl;
        
        DenseMatrixX V; // Vertices
        DenseMatrixXi E; // Edges, Face
        int FineNodesNumber=pFineMesh->N_nodes;

        V.resize(pCoarseningSystem->n_Nodes,3);//+FineNodesNumber

        for(int i=0; i<40;i++)
        {
            pIntegrator->SingleStep();
            pCoarseningSystem->getPosition(p);
           
            for(int i=0; i< pCoarseningSystem->n_Nodes;i++)
            {
                V.row(i) = Vec3(p(2*i), 0.0, p(2*i+1));
		
            }           

            cout<<V<<endl;

        }
            
        

        // Window window("Visualizer");
        // window.setSize(1200, 700);
     

        // pCoarseMesh->getEdges(E);   

        // MeshTri3D mesh;
        // int FineNodesInSingleCoarseElements=0;//todo
        // int CoarseElementNumber=pCoarseMesh->N_cell;

        // mesh.setName("test mesh");
        // mesh.updatePointBuffer(V);
        // mesh.addEdgeLayer("Edges")->updateEdgeBuffer(E);
        // mesh.addPointMask("Coarse Nodes", 0, pCoarseningSystem->n_Nodes);
        // mesh.initRenderable();

       
        
        // while(!window.shouldClose())
        // {   

        //     mesh.updatePointBuffer(V);

        //     window.clear();
        //     window.drawGrid();
        //     window.draw(&mesh);

        //     window.drawGui(&mesh);
        //     window.renderGui();
        //     window.swapBuffers();

        //     window.pollEvents();
        //     window.manageEvents();
        // }



        Window window1("Visualizer");
        window1.setSize(1200, 700);
        DenseMatrixX V1; // Vertices
        DenseMatrixXi E1; // Edges, Face
        V1.resize(pSystem->n_Nodes,3);//+FineNodesNumber

        pFineMesh->getEdges(E1);   

        MeshTri3D mesh1;
        int FineElementNumber=pFineMesh->N_cell;

        for(int i=0; i<40;i++)
        {   
            VectorXd p1;
            pFineIntegrator->SingleStep();
            pSystem->getPosition(p1);
           
            for(int i=0; i< pSystem->n_Nodes;i++)
            {
                V1.row(i) = Vec3(p1(2*i), 0.0, p1(2*i+1));
		
            }           

            cout<<V1<<endl;

        }

        mesh1.setName("Ground truth");
        mesh1.updatePointBuffer(V1);
        mesh1.addEdgeLayer("Edges")->updateEdgeBuffer(E1);
        mesh1.initRenderable();

       
        
        while(!window1.shouldClose())
        {             

            window1.clear();
            window1.drawGrid();
            window1.draw(&mesh1);

            window1.drawGui(&mesh1);
            window1.renderGui();
            window1.swapBuffers();

            window1.pollEvents();
            window1.manageEvents();
        }
    }
};

