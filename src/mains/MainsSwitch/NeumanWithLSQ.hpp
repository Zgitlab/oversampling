#pragma once 
#include<mains/MatrixCoefficients/HarmonicDisplacement.hpp>

#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>

#include <mains/2DFEM/Materials/Material_StVK.hpp>
#include <mains/2DFEM/Mesh.hpp>
#include <mains/2DFEM/System.hpp>
#include <mains/2DFEM/Integrator.hpp>
#include <mains/2DFEM/BoundaryConditions/Dirichlet.hpp>
#include <mains/2DFEM/BoundaryConditions/Neumann.hpp>
#include <mains/2DFEM/BoundaryConditions/Gravity.hpp>

#include <mains/2DFEM/Integrators/NewtonStaticB.hpp>
#include <mains/2DFEM/NumericalCoarsening/NewtonStaticCoarse.hpp>
#include <mains/2DFEM/Integrators/GradientDescent.hpp>

#include <mains/MatrixCoefficients/OnlyKD.hpp>

#include <mains/NumericalCoarsening/CoarseElement.hpp>
#include <mains/NumericalCoarsening/CoarseningSystem.hpp>

#include<mains/ADMM/ADMM.hpp>
#include<mains/ADMM/Oversampling2D.hpp>
#include<mains/ADMM/util.hpp>

#include<mains/LSQ/LagrangianLSQ.hpp>

#include <chrono> 
#include <utils/io/writeMatrix.hpp>
#include <utils/io/readCSV.hpp>
#include <utils/SparseMatrixIndex.hpp>
#include <utils/SparseMatrixSlicing.hpp>
#include <utils/timing.hpp>
#include <utils/SquashPositionVector.hpp>
#include <mains/ADMM/setSubSystem.hpp>
#include <mains/ADMM/OversamplingWithoutADMM.hpp>
#include <mains/MatrixCoefficients/ChenNoData.hpp>
#include<mains/MatrixCoefficients/SmoothnessApproximation.hpp>

namespace NeumanWithLSQ
{
    using namespace std;
    using namespace Eigen;
    using namespace OversamplingWithoutADMM;

    void NeumanWithLSQ(){
        auto C=GlobalCoarsePattern();
        SpMat spC= C.sparseView();
        spC.makeCompressed();

        vector<vector<MatrixXd>> data2Chen(3);
        for (int k=0; k<spC.outerSize(); ++k)
        {
            for (SparseMatrix<double>::InnerIterator it(spC,k); it; ++it)
            {   
                int xc=it.row();
                int yc=it.col();
                SpMat Hessian;
                vector<int> vDOFCentralElement, xpDOF,ypDOF,xmDOF,ymDOF;
                int Nx,Ny;
                AssembleSurrounding(xc,yc,Hessian,vDOFCentralElement,xpDOF,ypDOF,xmDOF,ymDOF,Nx,Ny);
                auto LHS=Hessian;
                VectorXd RHS= VectorXd::Zero(Hessian.rows());
                VectorXd Rxp= VectorXd::Zero(xpDOF.size());
                VectorXd Rxm= VectorXd::Zero(xpDOF.size());
                for(int i=0;i<Rxp.size();i++)
                {   
                    if (i%2==0)
                    Rxp(i)=1;
                }
                for(int i=0;i<Rxm.size();i++)
                {   
                    if (i%2==0)
                    Rxm(i)=-1;
                }
                Rxp(0)*=0.5;
                Rxp.tail(1)*=0.5;
                Rxm(0)*=0.5;
                Rxm.tail(1)*=0.5;

                RHS(xpDOF)+=1e2*Rxp;
                RHS(xmDOF)+=1e2*Rxm;
                SparseQR<SpMat,COLAMDOrdering<int>> QR(LHS);
                VectorXd result=QR.solve(RHS);
                //Render::RenderRectangleMesh(1e2*result,Nx,Ny,1.0);
                data2Chen[0].push_back(utils::SquashPositionVector(result(vDOFCentralElement)));
            }
        }

        for (int k=0; k<spC.outerSize(); ++k)
        {
            for (SparseMatrix<double>::InnerIterator it(spC,k); it; ++it)
            {   
                int xc=it.row();
                int yc=it.col();
                SpMat Hessian;
                vector<int> vDOFCentralElement, xpDOF,ypDOF,xmDOF,ymDOF;
                int Nx,Ny;
                AssembleSurrounding(xc,yc,Hessian,vDOFCentralElement,xpDOF,ypDOF,xmDOF,ymDOF,Nx,Ny);
                auto LHS=Hessian;
                VectorXd RHS= VectorXd::Zero(Hessian.rows());
                VectorXd Rxp= VectorXd::Zero(xpDOF.size());
                VectorXd Rxm= VectorXd::Zero(xmDOF.size());
                for(int i=0;i<Rxp.size();i++)
                {   
                    if (i%2==1)
                    Rxp(i)=1;
                }
                for(int i=0;i<Rxm.size();i++)
                {   
                    if (i%2==1)
                    Rxm(i)=-1;
                }
                RHS(xpDOF)+=1e2*Rxp;
                RHS(xmDOF)+=1e2*Rxm;
                SparseQR<SpMat,COLAMDOrdering<int>> QR(LHS);
                VectorXd result=QR.solve(RHS);
                
                data2Chen[1].push_back(utils::SquashPositionVector(result(vDOFCentralElement)));
            }
        }

        for (int k=0; k<spC.outerSize(); ++k)
        {
            for (SparseMatrix<double>::InnerIterator it(spC,k); it; ++it)
            {   
                int xc=it.row();
                int yc=it.col();
                SpMat Hessian;
                vector<int> vDOFCentralElement, xpDOF,ypDOF,xmDOF,ymDOF;
                int Nx,Ny;
                AssembleSurrounding(xc,yc,Hessian,vDOFCentralElement,xpDOF,ypDOF,xmDOF,ymDOF,Nx,Ny);
                auto LHS=Hessian;
                VectorXd RHS= VectorXd::Zero(Hessian.rows());
                VectorXd Ryp= VectorXd::Zero(ypDOF.size());
                VectorXd Rym= VectorXd::Zero(ymDOF.size());
                for(int i=0;i<Ryp.size();i++)
                {   
                    if (i%2==0)
                    Ryp(i)=1;
                }
                for(int i=0;i<Rym.size();i++)
                {   
                    if (i%2==0)
                    Rym(i)=-1;
                }
                RHS(xpDOF)+=1e2*Ryp;
                RHS(xmDOF)+=1e2*Rym;
                SparseQR<SpMat,COLAMDOrdering<int>> QR(LHS);
                VectorXd result=QR.solve(RHS);
                
                data2Chen[2].push_back(utils::SquashPositionVector(result(vDOFCentralElement)));
            }
        }

        // get Chen with No data
        auto pChen= new NoData::ConstrainedOptimisation (fpc);
        
        pChen->buildAll();

        auto n_ij= pChen->get_n_ij()[0];

        


    }
};