#ifndef __CHENWITHOVERSAMPLEDATA_H__
#define __CHENWITHOVERSAMPLEDATA_H__

#include <mains/MatrixCoefficients/OnlyKD.hpp>
#include <mains/ADMM/ADMM2Dexample.hpp>
#include <mains/ADMM/setSubSystemNeuman.hpp>
#include <utils/SquashPositionVector.hpp>
#include <utils/Render.hpp>

namespace Mains{
    void ChenWithOversampleData()
    {
        // get oversample data


        vector<Material_StVK> M(4);
        M[0].setMaterialFromYP(  0,   0,  0);
        M[1].setMaterialFromYP(1e6,0.49,1e3);
        M[2].setMaterialFromYP(1e6,0.49,1e3);
        M[3].setMaterialFromYP(1e4,0.49,1e3);

        vector<ADMM::Subsystem*> vADMMSubsystem;
        SpMat spA;
        int fpc, fepc;
        vector<map<int,int>> vAll2Active;
        vector<map<int,int>> vActive2All;
        int OccupiedCoarseElementCount;
        vector<SpMat> vSubPattern;

        ADMM::setSubSystemNeuman(M,vADMMSubsystem,spA,
            fpc, fepc,vAll2Active,
            vActive2All,OccupiedCoarseElementCount, vSubPattern);

         // Oversampling
        unique_ptr<ADMM::WholeDomainProcessing> pW (
            new ADMM::WholeDomainProcessing(vADMMSubsystem,spA,
            fpc, vAll2Active, vActive2All));

        vector<vector<MatrixXd>> data2Chen(3);

        
                vector<Vector2d> vD(4);
                VectorXd F;
                vector<Vector2d> C;

        //1: x stretch              
                vD[0]<<-1,0;
                vD[1]<< 1,0;
                vD[2]<< 1,0;   
                vD[3]<<-1,0;

        for(int c=0; c<OccupiedCoarseElementCount;c++)
        {   
                pW->OversampleAnElement(c, vD,F,C);
                data2Chen[0].push_back(utils::SquashPositionVector(F));
                pW->RestoreChange();          
                Render::RenderCoarseElement(F,fpc,1.0);
        }

        double max_u=1.0;
        //2: y stretch
                vD[0]<<0,-max_u;
                vD[1]<<0,-max_u;
                vD[2]<<0, max_u;    
                vD[3]<<0, max_u;      
        for(int c=0; c<OccupiedCoarseElementCount;c++)
        {   
                pW->OversampleAnElement(c, vD,F,C);
                data2Chen[1].push_back(utils::SquashPositionVector(F));   
                pW->RestoreChange();                     
        }

        //3: x shear;
                vD[0]<<-max_u,0;
                vD[1]<<-max_u,0;
                vD[2]<< max_u,0;    
                vD[3]<< max_u,0; 
        for(int c=0; c<OccupiedCoarseElementCount;c++)
        {   
                pW->OversampleAnElement(c, vD,F,C);
                data2Chen[2].push_back(utils::SquashPositionVector(F));  
                pW->RestoreChange();                      
        }

        auto pChen= new ConstrainedOptimisation (data2Chen, fpc);
        
        pChen->buildAll();

        auto n_ij= pChen->get_n_ij();
        vector<vector<vector<Matrix2d>>> vW;// first index: the coarse element number, second index:coarse node number, third index fine node number


        string str1= "MatlabData/CoarseElementDistribution.csv";        
        MatrixXd m1 = utils::load_csv<MatrixXd>(str1);
        SpMat GlobalCoarsePattern=m1.sparseView();

        
        auto pCoarseMesh=new ArbitraryQuadMesh((fpc-1)*1.0,GlobalCoarsePattern); 
        for(int k=0; k<pCoarseMesh->N_cell;k++)
        {   
            vector<vector<Matrix2d>> arrayW;
            VectorXd v= n_ij[k];

            //transport the coefficients
            for(int i=0;i<4 ;i++)
            {   vector<Matrix2d> vm1;
                for(int j=0; j<fpc*fpc; j++)
                {
                    VectorXd vs= v.segment(4*fpc*fpc*i+j*4,4);
                    Matrix2d m;
                    m<<vs(0), vs(1),
                    vs(2), vs(3);
                    vm1.push_back(m);
                };
                arrayW.push_back(vm1);
            };
        
            vW.push_back(arrayW);
        }
        for(int i=0; i<10;i++)
        {
            for(int k=0; k<9; k++)
            {
                for(int j=0; j<4;j++)
                {
                    cout<<"W"<< i<< j<< k<< ":\n"<< vW[i][j][k]<<endl;
                }
            }
        }

        auto pCMaterial= new CompositeMaterial();
        vector<CompositeMaterial*> v_pCMaterial;
        for(int i=0;i<GlobalCoarsePattern.nonZeros();i++)
        {
            auto pCMaterial= new CompositeMaterial();
            for(int j=0; j<fepc*fepc;j++)
            {   
                int x,y;
                x= j/fepc;
                y= j%fepc;
                int m= round(vSubPattern[i].coeff(x,y));
                pCMaterial->v_pMateiral.push_back(&M[m]);
            }
            v_pCMaterial.push_back(pCMaterial);
        }

        
        auto pCoarseningSystem= new CoarseningSystem(pCoarseMesh, v_pCMaterial,vW,fpc);
        VectorXd p;
        pCoarseningSystem->getPosition(p); 


        vector<int> indexDOF,indexDOFN,indexFineDOF,indexFineDOFN;
        vector<double> valueDOF,valueDOFN,valueFineDOFN;
        int n_dof_coarse=pCoarseningSystem->n_DOF;
        for(int i=0; i<4;i++)
        {
            indexDOF.push_back(2*i);
            indexDOF.push_back(2*i+1);
            // indexDOFN.push_back(8+2*i);            
            // valueDOFN.push_back(1e6);
            // indexDOFN.push_back(8+2*i+1);  
            // valueDOFN.push_back(3.0e6);
            // indexDOFN.push_back(n_dof_coarse-16+2*i+1);
            // valueDOFN.push_back(-2.0e6);

        }
       

        auto pDirichlet= new Dirichlet(indexDOF,valueDOF,pCoarseningSystem->n_DOF);
        auto pNeumann= new Neumann(indexDOFN,valueDOFN);
        
        auto pGravity= new Gravity(10);
        
        auto pIntegrator= new NewtonStaticForCoarsening(pCoarseningSystem);
        pIntegrator->v_pBoundaryConditions.push_back(pGravity);
        pIntegrator->v_pBoundaryConditions.push_back(pNeumann);
        pIntegrator->v_pBoundaryConditions.push_back(pDirichlet);


        cout<<"test coarse system 5"<<endl;
        
        DenseMatrixX V; // Vertices
        DenseMatrixXi E; // Edges, Face

        V.resize(pCoarseningSystem->n_Nodes,3);//+FineNodesNumber

        for(int i=0; i<40;i++)
        {
            pIntegrator->SingleStep();
            pCoarseningSystem->getPosition(p);
           
            for(int i=0; i< pCoarseningSystem->n_Nodes;i++)
            {
                V.row(i) = Vec3(p(2*i), 0.0, p(2*i+1));
		
            }           

            cout<<V<<endl;

        }
            
        

        Window window("Visualizer");
        window.setSize(1200, 700);
     

        pCoarseMesh->getEdges(E);   

        MeshTri3D mesh;
        int FineNodesInSingleCoarseElements=0;//todo
        int CoarseElementNumber=pCoarseMesh->N_cell;

        mesh.setName("test mesh");
        mesh.updatePointBuffer(V);
        mesh.addEdgeLayer("Edges")->updateEdgeBuffer(E);
        mesh.addPointMask("Coarse Nodes", 0, pCoarseningSystem->n_Nodes);
        mesh.initRenderable();

       
        
        while(!window.shouldClose())
        {   

            mesh.updatePointBuffer(V);

            window.clear();
            window.drawGrid();
            window.draw(&mesh);

            window.drawGui(&mesh);
            window.renderGui();
            window.swapBuffers();

            window.pollEvents();
            window.manageEvents();
        }
            


            


    }

};


#endif // __CHENWITHOVERSAMPLEDATA_H__