#pragma once

#include <mains/MatrixCoefficients/OnlyKD.hpp>
#include <mains/ADMM/ADMM2Dexample.hpp>
#include <utils/SquashPositionVector.hpp>
#include <mains/MatrixCoefficients/ChenNoData.hpp>
#include <mains/MatrixCoefficients/SmoothnessApproximation.hpp>

namespace Mains{

    void SmoothApproximation()
    {
        vector<Material_StVK> M(4);
        M[0].setMaterialFromYP(  0,   0,  0);
        M[1].setMaterialFromYP(1e6,0.49,1e3);
        M[2].setMaterialFromYP(1e6,0.48,1e3);
        M[3].setMaterialFromYP(1e5,0.40,1e3);

        vector<ADMM::Subsystem*> vADMMSubsystem;
        SpMat spA;
        int fpc, fepc;
        vector<map<int,int>> vAll2Active;
        vector<map<int,int>> vActive2All;
        int OccupiedCoarseElementCount;
        vector<SpMat> vSubPattern;

        ADMM::setSubSystem(M,vADMMSubsystem,spA,
            fpc, fepc,vAll2Active,
            vActive2All,OccupiedCoarseElementCount, vSubPattern);

         // Oversampling
        unique_ptr<ADMM::WholeDomainProcessing> pW (
            new ADMM::WholeDomainProcessing(vADMMSubsystem,spA,
            fpc, vAll2Active, vActive2All));

        vector<vector<VectorXd>> data2Smooth(OccupiedCoarseElementCount); //[1][2] 1: Coarse elemet 2:scenario
        vector<vector<vector<Vector2d>>> CoarseData(OccupiedCoarseElementCount);// [1][2][3] 1.coarse element 2: scenario 3: coarse nodes in parametric order
        
                vector<Vector2d> vD(4);
                VectorXd F;
                vector<Vector2d> C;

        //1: x stretch              
                vD[0]<<-1,0;
                vD[1]<< 1,0;
                vD[2]<< 1,0;   
                vD[3]<<-1,0;

        for(int c=0; c<OccupiedCoarseElementCount;c++)
        {   
                pW->OversampleAnElement(c, vD,F,C);
                data2Smooth[c].push_back(F);  
                CoarseData[c].push_back(C);
                pW->RestoreChange();
        }

        double max_u=1.0;
        //2: y stretch
                vD[0]<<0,-max_u;
                vD[1]<<0,-max_u;
                vD[2]<<0, max_u;    
                vD[3]<<0, max_u;      
        for(int c=0; c<OccupiedCoarseElementCount;c++)
        {   
                pW->OversampleAnElement(c, vD,F,C);
                data2Smooth[c].push_back(F);   
                CoarseData[c].push_back(C);     
                pW->RestoreChange();        
        }

        //3: x shear;
                vD[0]<<-max_u,0;
                vD[1]<<-max_u,0;
                vD[2]<< max_u,0;    
                vD[3]<< max_u,0; 
         for(int c=0; c<OccupiedCoarseElementCount;c++)
        {   
                pW->OversampleAnElement(c, vD,F,C);
                data2Smooth[c].push_back(F);   
                CoarseData[c].push_back(C);       
                pW->RestoreChange();      
        }

    // get Chen with No data
        auto pChen= new NoData::ConstrainedOptimisation (fpc);
        
        pChen->buildAll();

        auto n_ij= pChen->get_n_ij()[0];

      

    //optimization

     vector<CompositeMaterial*> v_pCMaterial;
        for(int i=0;i<OccupiedCoarseElementCount;i++)
        {
            auto pCMaterial= new CompositeMaterial();
            for(int j=0; j<fepc*fepc;j++)
            {   
                int x,y;
                x= j/fepc;
                y= j%fepc;
                int m= round(vSubPattern[i].coeff(x,y));
                pCMaterial->v_pMateiral.push_back(&M[m]);
            }
            v_pCMaterial.push_back(pCMaterial);
        }

        vector<vector<vector<Matrix2d>>> vvvW(OccupiedCoarseElementCount);// first index: the coarse element number, second index:coarse node number, third index fine node number

      for(int c=0; c<OccupiedCoarseElementCount;c++)
        {

            vector<vector<Matrix2d>> vvW(4);
            for(auto&_vW:vvW)
            {
                _vW.resize(fpc*fpc);
            }
            auto RCoarse =CoarseData[c];
            vector<Vector2d> RFine(3);
            

            vector<int> vvv={0,2,3,1};//change naive to parametric order;
           
            auto QuadMeshCoarse= QuadRectangleMesh(2,2,fepc);
            auto QuadMeshFine  = QuadRectangleMesh(fpc,fpc,1);
            vector<int> vCoarse{0, fpc-1,  fpc*(fpc-1),fpc*fpc-1 };
        
            
            for (int nFineNode=0;nFineNode<QuadMeshFine.N_nodes;nFineNode++)//should be convert to the active one after some point
            {

                int Local;
                ADMM::mapGlobalIndex2Local(nFineNode,Local,fpc);
                for(int i=0;i<3;i++)
                {  
                        RFine[i]=data2Smooth[c][i].segment(2*Local,2);
                }
                VectorXd W0;
                W0.resize(16);
                

                for(int ic=0;ic<4;ic++)
                {      
                        W0.segment(4*ic,4)=n_ij.segment(4*fpc*fpc*vvv[ic]+4*Local,4);
                }

                if (find(vCoarse.begin(),vCoarse.end(),nFineNode)==vCoarse.end()) // fine node is not coarse node;
                {
                    vector<Vector2d> vX_H;
                    Vector2d X_h;

                    for(int i: vvv)
                    vX_H.push_back(QuadMeshCoarse.v_pCompleteListOfNodes[i]->getInitialPosition());

                    X_h=QuadMeshFine.v_pCompleteListOfNodes[nFineNode]->getInitialPosition();
                    
                    
                    unique_ptr<nearSmooth::SmoothnessApproximation> pLS(new nearSmooth::SmoothnessApproximation
                    (vX_H,X_h,W0,RCoarse,RFine));
                
                    
                    
                    pLS->Optimize();
                    cout<<"Weighting factor:"<<pLS->W<<endl;   
                    for(int k=0;k<4;k++)
                    {   
                        for(int i=0;i<2;i++)
                        {
                            for(int j=0; j<2;j++)
                            {                                
                                vvW[vvv[k]][nFineNode](i,j)=pLS->W(4*k+2*i+j);
                            }
                        }
                            
                    }                    
                }else{
                    auto it =find(vCoarse.begin(),vCoarse.end(),nFineNode);
                    int iC=distance(vCoarse.begin(),it);
                    for(int k=0;k<4;k++)
                    {
                        if (k==iC) vvW[k][nFineNode]=Matrix2d::Identity();
                        else  vvW[k][nFineNode]=Matrix2d::Zero();
                    }


                }

            }
            vvvW[c]=vvW;
        }

        auto pCoarseMesh=new ArbitraryQuadMesh(fepc*1.0,spA);
        auto pCoarseningSystem= new CoarseningSystem(pCoarseMesh, v_pCMaterial,vvvW,fpc);
        VectorXd p;
        pCoarseningSystem->getPosition(p);
        vector<int> indexDOF,indexDOFN,indexFineDOF,indexFineDOFN;
        vector<double> valueDOF,valueDOFN,valueFineDOFN;
        int n_dof_coarse=pCoarseningSystem->n_DOF;
        for(int i=0; i<4;i++)
        {
            indexDOF.push_back(2*i);
            indexDOF.push_back(2*i+1);
        //     indexDOFN.push_back(8+2*i);            
        //     valueDOFN.push_back(1e6);
        //     indexDOFN.push_back(8+2*i+1);  
        //     valueDOFN.push_back(1.0e6);
        //     indexDOFN.push_back(n_dof_coarse-16+2*i+1);
        //     valueDOFN.push_back(-1.0e6);

        }
       
        
        auto pDirichlet= new Dirichlet(indexDOF,valueDOF,pCoarseningSystem->n_DOF);
        auto pNeumann= new Neumann(indexDOFN,valueDOFN);
        
        auto pGravity= new Gravity(10);
        
        
        auto pIntegrator= new NewtonStaticForCoarsening(pCoarseningSystem);

        pIntegrator->v_pBoundaryConditions.push_back(pGravity);
        pIntegrator->v_pBoundaryConditions.push_back(pNeumann);
        pIntegrator->v_pBoundaryConditions.push_back(pDirichlet);

        cout<<"testcsv 15"<<endl;
        Window window("Visualizer");
        window.setSize(1200, 700);
        DenseMatrixX V; // Vertices
        V.resize(pCoarseningSystem->n_Nodes,3);
        DenseMatrixXi E; // Edges, Face
        pCoarseMesh->getEdges(E);   
        cout<<"testcsv 16"<<endl;
        MeshTri3D mesh;
        mesh.setName("test mesh");
        mesh.updatePointBuffer(V);
        mesh.addEdgeLayer("Edges")->updateEdgeBuffer(E);

        while(!window.shouldClose())
        {   

            pIntegrator->SingleStep();
            pCoarseningSystem->getPosition(p);

            for(int i=0; i< pCoarseningSystem->n_Nodes;i++)
            {
                V.row(i) = Vec3(p(2*i), 0.0, p(2*i+1));
		
            }
            cout<<V<<endl;


            mesh.updatePointBuffer(V);

            window.clear();
            window.drawGrid();
            window.draw(&mesh);

            window.drawGui(&mesh);
            window.renderGui();
            window.swapBuffers();

            window.pollEvents();
            window.manageEvents();
        }

        
    }
};