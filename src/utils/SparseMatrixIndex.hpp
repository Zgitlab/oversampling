#pragma once
#include <Eigen/Sparse>
#include <iostream>
typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> T;

namespace Eigen
{
    void RowAndColForAnEntry( SpMat A, int Entry, int& row, int& col)// for compressed col major sparse
    {
        auto pO=A.outerIndexPtr();
        auto pI=A.innerIndexPtr();
        row=pI[Entry];
        for (int i=0; i<=A.cols();i++)
        {
            if (Entry<pO[i]) 
            {
                col=i-1;
                break;
            };

        }

    }
};
