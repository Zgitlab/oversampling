#pragma once
#include <Eigen/Sparse>
#include <iostream>
typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> T;

namespace Eigen
{
    using namespace std;
    void MatrixAddition(SpMat A, vector<int> vDOF,SpMat& B)// add A to B(vDOF,vDOF) suppose the addition doesn't change the non-zero pattern of B
    {   
        if (vDOF.size()!=A.cols()){
            cout<<"Matrix Addition:column  number mismatch"<<endl;
        }else if (vDOF.size()!=A.rows())
        {
            cout<<"Matrix Addition:row number mismatch"<<endl;
        }else
        {
            for(int i = 0; i < A.outerSize(); i++)
            {
                for(typename Eigen::SparseMatrix<double>::InnerIterator it(A,i); it; ++it)                 
                {
                    B.coeffRef(vDOF[it.row()],vDOF[it.col()])+=it.value();
                };
            };       
        };   
    }
};