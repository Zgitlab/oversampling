# pragma once
#include<chrono>   
#include<string>
#include<iostream>
    class timing{
        public:
        double start;
        std::string message;

        timing(std::string _message)
        {   
            start=std::clock();
            message= _message;
        }

        void end()
        {
            
            auto duration = (std::clock() - start ) / (double) CLOCKS_PER_SEC;
            std::cout<< message<<"  :  "<<duration<<"s"<<std::endl;
        }


    };
        
