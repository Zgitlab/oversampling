#ifndef __SQUASHPOSITIONVECTOR_H__
#define __SQUASHPOSITIONVECTOR_H__

#include <Eigen/Core>

namespace utils{
    using namespace Eigen;
    MatrixXd SquashPositionVector(VectorXd p)
    {
        MatrixXd s( p.rows()/2,2);
        for(int i=0; i<s.rows();i++)
        {
            s(i,0)=p(2*i);
            s(i,1)=p(2*i+1);
        }
        return s;
        

       
        
    }
}
#endif // __SQUASHPOSITIONVECTOR_H__