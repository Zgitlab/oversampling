#pragma once

#include <utils/LinearAlgebra.hpp>

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
namespace utils
{
    template<class Matrix>
    inline bool writeMatrix(const char *filePath, const Matrix &X)
    {
        std::ofstream file;
	    file.open(filePath);
	    if(file.is_open())
	    {
            file<< X;
            file.close();
            return true;
	    }
	    else
        {
	    	std::cout << "Unable to create Obj file" << std::endl;
            return false;
        }
    }
    
     template<class Matrix>
    inline bool writeMatrix(const std::string filePath, const Matrix &X)
    {
        std::ofstream file;
	    file.open(filePath);
	    if(file.is_open())
	    {
            file<< X;
            file.close();
            return true;
	    }
	    else
        {
	    	std::cout << "Unable to create Obj file" << std::endl;
            return false;
        }
    }


    // inline bool writeSparseMatrix(const char *filePath, const SparseMatrixX &X)
    // {
    //     std::ofstream file;
	//     file.open(filePath);
	//     if(file.is_open())
	//     {
    //         file<< X;
    //         file.close();
    //         return true;
	//     }
	//     else
    //     {
	//     	std::cout << "Unable to create Obj file" << std::endl;
    //         return false;
    //     }
    // }

    // inline bool writeDenseMatrix(const char *filePath, const DenseMatrixX &X)
    // {
    //     std::ofstream file;
	//     file.open(filePath);
	//     if(file.is_open())
	//     {
    //         file<< X;
    //         file.close();
    //         return true;
	//     }
	//     else
    //     {
	//     	std::cout << "Unable to create Obj file" << std::endl;
    //         return false;
    //     }
    // }

    // inline bool writeSparseMatrix(std::string filePath, const SparseMatrixX &X)
    // {
    //     std::ofstream file;
	//     file.open(filePath);
	//     if(file.is_open())
	//     {
    //         file<< X;
    //         file.close();
    //         return true;
	//     }
	//     else
    //     {
	//     	std::cout << "Unable to create Obj file" << std::endl;
    //         return false;
    //     }
    // }

    // inline bool writeDenseMatrix(std::string filePath, const DenseMatrixX &X)
    // {
    //     std::ofstream file;
	//     file.open(filePath);
	//     if(file.is_open())
	//     {
    //         file<< X;
    //         file.close();
    //         return true;
	//     }
	//     else
    //     {
	//     	std::cout << "Unable to create Obj file" << std::endl;
    //         return false;
    //     }
    // }
    // inline bool writeDenseMatrix(std::string filePath, const Eigen::Matrix<int,-1,-1> &X)
    // {
    //     std::ofstream file;
	//     file.open(filePath);
	//     if(file.is_open())
	//     {
    //         file<< X;
    //         file.close();
    //         return true;
	//     }
	//     else
    //     {
	//     	std::cout << "Unable to create Obj file" << std::endl;
    //         return false;
    //     }
    // }
};