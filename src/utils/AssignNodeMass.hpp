
#ifndef ASSIGNNODEMASS_HPP
#define ASSIGNNODEMASS_HPP
#include <Eigen/Sparse>
#include <utils/LinearAlgebra.hpp>
#include <utils/RemoveRows.hpp>
#include <igl/volume.h>

Eigen::VectorXd NodeMass(DenseMatrixX Vtet,DenseMatrixXi Ttet)
{	Eigen::VectorXd vol;
	igl::volume(Vtet,Ttet,vol);
	cout<<vol.rows()<<endl;
	Eigen::VectorXd node_mass;
	node_mass=VectorXd::Zero(Vtet.rows());

	double rho=1;//density
	for(int i=0;i<Ttet.rows();i++)
	{
		for(int j=0; j<4; j++)
		{
			node_mass(Ttet(i,j))+=rho*vol(i);
		};
	};

	return node_mass;
}


#endif ASSIGNNODEMASS_HPP
