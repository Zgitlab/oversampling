#pragma once
#include <rendering/baseClasses/Window.hpp>
#include <rendering/renderables/MeshTri3D.hpp>
#include <Eigen/Core>
#include <mains/2DFEM/Mesh.hpp>
namespace Render
{
    void RenderCoarseElement(int fpc, double unit_length,MatrixXd V)
    {   using namespace std;
        using namespace Eigen;
        
        auto RefMesh=QuadRectangleMesh(fpc,fpc, unit_length);
        MatrixXd a,V0;
        RefMesh.getPositions(a);
        V0.resize(a.rows(),3);
        V0.setZero();
        V0.col(0)=a.col(0)+V.col(0);
        V0.col(2)=a.col(1)+V.col(1);

        Window window("Visualizer");
        window.setSize(1200, 700);

        DenseMatrixXi E; // Edges, Face

        RefMesh.getEdges(E);   

        MeshTri3D mesh;
        int FineNodesInSingleCoarseElements=0;//todo

        mesh.setName("test mesh");
        mesh.updatePointBuffer(V0);
        mesh.addEdgeLayer("Edges")->updateEdgeBuffer(E);
        mesh.initRenderable();       
        
        while(!window.shouldClose())
        {   

            mesh.updatePointBuffer(V0);

            window.clear();
            window.drawGrid();
            window.draw(&mesh);

            window.drawGui(&mesh);
            window.renderGui();
            window.swapBuffers();

            window.pollEvents();
            window.manageEvents();
        }

    }
    void RenderCoarseElement(VectorXd p,int fpc, double unit_length)
    {   using namespace std;
        using namespace Eigen;
        
        auto RefMesh=QuadRectangleMesh(fpc,fpc, unit_length);
        MatrixXd a,V;
        RefMesh.getPositions(a);
        V.resize(a.rows(),3);
        V.setZero();
        V.col(0)=a.col(0);
        V.col(2)=a.col(1);
        for(int i=0; i< fpc*fpc;i++)
        {
            V.row(i) += Vec3(5*p(2*i), 0.0, 5*p(2*i+1));
        }
        cout<<V<<endl;
        Window window("Visualizer");
        window.setSize(1200, 700);

        DenseMatrixXi E; // Edges, Face

        RefMesh.getEdges(E);   

        MeshTri3D mesh;
        int FineNodesInSingleCoarseElements=0;//todo

        mesh.setName("test mesh");
        mesh.updatePointBuffer(V);
        mesh.addEdgeLayer("Edges")->updateEdgeBuffer(E);
        mesh.initRenderable();       
        
        while(!window.shouldClose())
        {   

            mesh.updatePointBuffer(V);

            window.clear();
            window.drawGrid();
            window.draw(&mesh);

            window.drawGui(&mesh);
            window.renderGui();
            window.swapBuffers();

            window.pollEvents();
            window.manageEvents();
        }

    }
    void RenderRectangleMesh(VectorXd p,int Nx,int Ny, double unit_length)
    {
        using namespace std;
        using namespace Eigen;
        
        auto RefMesh=QuadRectangleMesh(Nx,Ny,unit_length);
        MatrixXd a,V;
        RefMesh.getPositions(a);
        V.resize(a.rows(),3);
        V.setZero();
        V.col(0)=a.col(0);
        V.col(2)=a.col(1);
        for(int i=0; i< Nx*Ny;i++)
        {
            V.row(i) += Vec3(p(2*i), 0.0, p(2*i+1));
        }
        Window window("Visualizer");
        window.setSize(1200, 700);

        DenseMatrixXi E; // Edges, Face

        RefMesh.getEdges(E);   

        MeshTri3D mesh;
        int FineNodesInSingleCoarseElements=0;//todo

        mesh.setName("test mesh");
        mesh.updatePointBuffer(V);
        mesh.addEdgeLayer("Edges")->updateEdgeBuffer(E);
        mesh.initRenderable();       
        
        while(!window.shouldClose())
        {   

            mesh.updatePointBuffer(V);

            window.clear();
            window.drawGrid();
            window.draw(&mesh);

            window.drawGui(&mesh);
            window.renderGui();
            window.swapBuffers();

            window.pollEvents();
            window.manageEvents();
        }

    }

};
        