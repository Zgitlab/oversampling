
#include <rendering/renderables/MeshTri3D.hpp>

#include <algorithm>

MeshTri3D::MeshTri3D()
: Renderable3D()
{   
    m_render = true;
    m_color = glm::vec3(0.0,0.0,0.0);
    m_pointSize = 4;

    //
    m_pointBuffer.setup(GL_ARRAY_BUFFER, GL_DYNAMIC_DRAW);
 
    //
    glGenVertexArrays(1, &m_vaoId);
    glBindVertexArray(m_vaoId);

    m_pointBuffer.bind();
    glEnableVertexAttribArray(0);
    
    if(sizeof(Scalar) == sizeof(double))
        glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, 0, nullptr);
    else
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    glBindVertexArray(0);

    //
    m_colorProgram.load("src/rendering/shaders/cameraShader.vert", "src/rendering/shaders/colorShader.frag");
    m_normalProgram.load("src/rendering/shaders/cameraShader.vert", "src/rendering/shaders/normalShader.frag");
}

MeshTri3D::~MeshTri3D()
{
    for(auto *m : m_pointMasks)
    {
        delete m;
    }

    for(auto *e : m_edgeLayers)
    {
        delete e;
    }

    for(auto *f : m_faceLayers)
    {
        delete f;
    }

    m_pointMasks.clear();
    m_edgeLayers.clear();
    m_faceLayers.clear();
}

//////////////////////////////////////////////////////////////////////////////////////////////

void MeshTri3D::draw(Camera *camera)
{  
    Renderable3D::draw(camera);

    glm::mat4 model;
    getModelMatrix(model);
    const glm::mat4 view = camera->getView();
    const glm::mat4 proj = camera->getProj();

    glBindVertexArray(m_vaoId);

    m_colorProgram.bind();
    m_colorProgram.setUniform("model", model);
    m_colorProgram.setUniform("view", view);
    m_colorProgram.setUniform("proj", proj);

    if(m_render)
    {
        glPointSize(m_pointSize);
        m_colorProgram.setUniform("color", m_color);
        glDrawArrays(GL_POINTS, 0, m_pointBuffer.size() / (3 * sizeof(Scalar)));
        glPointSize(1.0);
    }

    for(auto *m : m_pointMasks)
    {
        if(m->m_render)
        {
            glPointSize(m->m_pointSize);
            m_colorProgram.setUniform("color", m->m_color);
            glDrawArrays(GL_POINTS, m->m_firstPointIndex,  m->m_numPoints);
            glPointSize(1.0);
        }
    }

    for(auto *e : m_edgeLayers)
    {
        if(e->m_render)
        {   
            glLineWidth(e->m_lineWidth);
            m_colorProgram.setUniform("color", e->m_color);
            e->m_edgeBuffer.bind();
            glDrawElements(GL_LINES, e->m_edgeBuffer.size() / sizeof(int), GL_UNSIGNED_INT, 0);
            glLineWidth(1.0);
        }
    }

    m_normalProgram.bind();
    m_normalProgram.setUniform("model", model);
    m_normalProgram.setUniform("view", view);
    m_normalProgram.setUniform("proj", proj);

    for(auto *f : m_faceLayers)
    {
        if(f->m_render)
        {   
            m_normalProgram.setUniform("color", f->m_color);
            f->m_faceBuffer.bind();
            glDrawElements(GL_TRIANGLES, f->m_faceBuffer.size() / sizeof(int), GL_UNSIGNED_INT, 0);
        }
    }
}

void MeshTri3D::drawGui()
{
    ImGui::PushID(this);

    const std::string name = getName() + " (MeshTri3D)";
    if(ImGui::CollapsingHeader(name.data()))
    {   
        ImGui::Indent(16.0f);

        Renderable3D::drawGui();

        ImGui::Checkbox("Points", &m_render);

        if(m_render){
            ImGui::Indent(16.0f);
            ImGui::ColorEdit3("Point Color", &m_color[0]);
            ImGui::InputInt("Point Size", &m_pointSize);
            ImGui::Unindent(16.0f);
        }

        for( auto *m : m_pointMasks)
        {
            ImGui::PushID(m->m_name.data());
            ImGui::Checkbox(m->m_name.data(), &(m->m_render));
            if(m->m_render)
            {
                ImGui::Indent(16.0f);
                ImGui::ColorEdit3("Color", &(m->m_color[0]));
                ImGui::InputInt("Size", &m->m_pointSize);
                ImGui::Unindent(16.0f);
            }
            ImGui::PopID();
        }

        for( auto *e : m_edgeLayers)
        {   
            ImGui::PushID(e->m_name.data());
            ImGui::Checkbox(e->m_name.data(), &(e->m_render));
            if(e->m_render)
            {
                ImGui::Indent(16.0f);
                ImGui::ColorEdit3("Color", &(e->m_color[0]));
                ImGui::InputInt("Line Width", &e->m_lineWidth);
                ImGui::Unindent(16.0f);
            }
            ImGui::PopID();
        }

        for( auto *f : m_faceLayers)
        {
            ImGui::PushID(f->m_name.data());
            ImGui::Checkbox(f->m_name.data(), &(f->m_render));
            if(f->m_render)
            {
                ImGui::Indent(16.0f);
                ImGui::ColorEdit3("Color", &(f->m_color[0]));
                ImGui::Unindent(16.0f);
            }
            ImGui::PopID();
        }

        ImGui::Unindent(16.0f);
    }

    ImGui::PopID();
}

//////////////////////////////////////////////////////////////////////////////////////////////

PointMask *MeshTri3D::addPointMask(const std::string &name)
{   
    PointMask *mask = new PointMask(name);
    m_pointMasks.push_back(mask);
    return mask;
}
PointMask *MeshTri3D::addPointMask(const std::string &name, int FPI, int TPN)
{   
    PointMask *mask = new PointMask(name, FPI, TPN);
    m_pointMasks.push_back(mask);
    return mask;
}


EdgeLayer *MeshTri3D::addEdgeLayer(const std::string &name)
{
    EdgeLayer *edgeLayer = new EdgeLayer();
    edgeLayer->m_name = name;
    edgeLayer->m_edgeBuffer.setup(GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW);
    m_edgeLayers.push_back(edgeLayer);
    return edgeLayer;
}

FaceLayer *MeshTri3D::addFaceLayer(const std::string &name)
{
    FaceLayer *faceLayer = new FaceLayer();
    faceLayer->m_name = name;
    faceLayer->m_faceBuffer.setup(GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW);
    m_faceLayers.push_back(faceLayer);
    return faceLayer;
}

//////////////////////////////////////////////////////////////////////////////////////////////

void MeshTri3D::updatePointBuffer(const void *data, unsigned numElems)
{   
    m_pointBuffer.loadData(data, numElems * sizeof(Scalar));
}

void MeshTri3D::updatePointBuffer(const DenseMatrixX &data)
{   
    const DenseMatrixX dataTransposed = data.transpose();
    updatePointBuffer(dataTransposed.data(), dataTransposed.size());
}

//////////////////////////////////////////////////////////////////////////////////////////////

void EdgeLayer::updateEdgeBuffer(const void *data, unsigned numElems)
{
    m_edgeBuffer.loadData(data, numElems * sizeof(int));
}

void EdgeLayer::updateEdgeBuffer(const DenseMatrixXi &data)
{
    const DenseMatrixXi dataTransposed = data.transpose();
    updateEdgeBuffer(dataTransposed.data(), dataTransposed.size());
}

//////////////////////////////////////////////////////////////////////////////////////////////

void FaceLayer::updateFaceBuffer(const void *data, unsigned numElems)
{
    m_faceBuffer.loadData(data, numElems * sizeof(int));
}

void FaceLayer::updateFaceBuffer(const DenseMatrixXi &data)
{
    const DenseMatrixXi dataTransposed = data.transpose();
    updateFaceBuffer(dataTransposed.data(), dataTransposed.size());
}
