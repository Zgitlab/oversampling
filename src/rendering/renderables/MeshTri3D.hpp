#pragma once 

#include <rendering/renderables/Renderable3D.hpp>

#include <rendering/baseClasses/Buffer.hpp>
#include <rendering/baseClasses/ShaderProgram.hpp>

#include <utils/LinearAlgebra.hpp>
#include <vector>

class PointMask;
class EdgeLayer;
class FaceLayer;

class MeshTri3D: public Renderable3D
{
public:

    MeshTri3D();
    virtual ~MeshTri3D();
  
    //
    virtual void draw(Camera *camera) override;
    
    virtual void drawGui() override;

    //
    PointMask *addPointMask(const std::string &name);
    PointMask *addPointMask(const std::string &name, int FPI, int TPN);
    EdgeLayer *addEdgeLayer(const std::string &name);
    FaceLayer *addFaceLayer(const std::string &name);

    //
    void updatePointBuffer(const void *data, unsigned numElems);
    void updatePointBuffer(const DenseMatrixX &data);

public:

    bool m_render;
    glm::vec3 m_color;
    int m_pointSize;

protected:

    unsigned m_vaoId;
    Buffer m_pointBuffer;
    ShaderProgram m_colorProgram;
    ShaderProgram m_normalProgram;

private:

    std::vector<PointMask*> m_pointMasks;
    std::vector<EdgeLayer*> m_edgeLayers;
    std::vector<FaceLayer*> m_faceLayers;

};

/////////////////////////////////////////////////////////////

class PointMask
{
public:

    friend class MeshTri3D;

private:

    PointMask(){}

    PointMask(const std::string &name)
    : m_name(name), 
      m_firstPointIndex(0), 
      m_numPoints(0), 
      m_render(false), 
      m_color(glm::vec3(147.0/255, 201.0/255, 255.0/255)),
      m_pointSize(8){}

    PointMask(const std::string &name, int FPI, int TPN)//FPI: first point index, TPN: total point number
    :m_name(name), 
     m_firstPointIndex(FPI),
     m_numPoints(TPN), 
     m_render(true), 
     m_color(glm::vec3(228.0/255, 147.0/255, 255.0/255)),
     m_pointSize(8)
    {}

    virtual ~PointMask(){}

public:

    std::string m_name;

    unsigned m_firstPointIndex;
    unsigned m_numPoints;

    bool m_render=true;
    glm::vec3 m_color;
    int m_pointSize;
};

class EdgeLayer
{
public:

    friend class MeshTri3D;

private: 

    EdgeLayer()
    :m_render(true), m_color(glm::vec3(0.0,0.0,0.0)), m_lineWidth(2){}

    virtual ~EdgeLayer(){}

public:

    void updateEdgeBuffer(const void *data, unsigned numElems);
    void updateEdgeBuffer(const DenseMatrixXi &data);

public:

    std::string m_name;

    bool m_render=true;
    glm::vec3 m_color;
    int m_lineWidth;

    Buffer m_edgeBuffer;
};

class FaceLayer
{
public:

    friend class MeshTri3D;

private: 

    FaceLayer()
    :m_render(true), m_color(glm::vec3(0.8,0.8,0.8)){}

    virtual ~FaceLayer(){}

public:

    void updateFaceBuffer(const void *data, unsigned numElems);
    void updateFaceBuffer(const DenseMatrixXi &data);

public:

    std::string m_name;

    bool m_render=true;
    glm::vec3 m_color;

    Buffer m_faceBuffer;
};
