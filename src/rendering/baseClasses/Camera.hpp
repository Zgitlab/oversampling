#pragma once

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>

class Camera
{
public:

    Camera();
    void updateView();

    inline void updateProj(float fovY, float aspectRatio)
    {
        m_proj = glm::perspective(glm::radians(fovY), aspectRatio, 0.01f, 100.0f);
    }

    inline void orbit(glm::vec3 sphericalDelta)
    {
        // m_spherical = m_sphericalRef + sphericalDelta;
        // updateView();
    }

    inline void translate(glm::vec2 targetDelta)
    {
        glm::mat4 viewTransposed = glm::transpose(m_view);

        m_target = m_targetRef + targetDelta[0] * glm::vec3(viewTransposed[0]) +
                                 targetDelta[1] * glm::vec3(viewTransposed[1]) ;
        updateView();
    }

    inline void saveStateAsRef()
    {
        m_targetRef = m_target;
        m_sphericalRef = m_spherical;
    }

    inline glm::mat4 getProj()
    {
        return m_proj;
    }

    inline glm::mat4 getView()
    {
        return m_view;
    }

    inline glm::mat4 getProjView()
    {
        return m_proj*m_view;
    }

public:

    glm::mat4 m_view;
    glm::mat4 m_proj;

    glm::vec3 m_target, m_spherical;
    glm::vec3 m_targetRef, m_sphericalRef;
};