
#include <rendering/baseClasses/Camera.hpp>

Camera::Camera()
{
        m_target = glm::vec3(0.0, 0.0, 0.0);
        m_spherical = glm::vec3(1.57, 0, 30.0);
        //m_spherical = glm::vec3(0.0, 0.0, 6.0);
        saveStateAsRef();
        updateView();
}

void Camera::updateView()
{
        glm::vec3 trans = glm::vec3(0.0, 0.0, -m_spherical[2]);
        m_view = glm::translate(trans) * 
             glm::eulerAngleXY(m_spherical[0], m_spherical[1]) * 
             glm::translate(-m_target);
}