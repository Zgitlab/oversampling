#pragma once

#include <rendering/baseClasses/imgui/imgui.h>
#include <rendering/baseClasses/imgui/imgui_impl_glfw.h>
#include <rendering/baseClasses/imgui/imgui_impl_opengl3.h>

class IGui
{
public:

    virtual void drawGui() = 0;
};