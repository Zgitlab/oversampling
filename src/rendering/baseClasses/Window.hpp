#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <rendering/baseClasses/IRenderable.hpp>
#include <rendering/baseClasses/Camera.hpp>
#include <rendering/baseClasses/ShaderProgram.hpp>
#include <rendering/baseClasses/IGui.hpp>

class Window
{
public:

    Window(const char *title);
    ~Window();

    void manageEvents();

    void draw(IRenderable *renderable);
    void drawGrid();

    void drawGui(IGui *gui);
    void renderGui();

    inline void setSize(const int width, const int height)
    {
        glfwSetWindowSize(m_window, width, height);
    }

    inline void clear()
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    inline void enableDepthTest()
    {
        glEnable(GL_DEPTH_TEST);
    }

    inline void disableDepthTest()
    {
        glDisable(GL_DEPTH_TEST);
    }

    inline int shouldClose()
    {
        return glfwWindowShouldClose(m_window);
    }

    inline void swapBuffers()
    {
        glfwSwapBuffers(m_window);
    }

    inline void pollEvents()
    {
        glfwPollEvents();
    }

    static void windowSizeCallback(GLFWwindow* window, int width, int height);
    static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
    static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
    static void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);

    GLFWwindow *m_window;

private:

    bool m_guiFrameInProgress;

    static Camera m_camera;

    unsigned m_vaoId;
    ShaderProgram m_gridProgram;
    bool m_renderGrid = true;

    static struct KeyboardState m_keyboardState;
    static struct MouseState m_mouseState;
};