#version 330 core

uniform vec3 color;

out vec4 FragColor;

in vec3 fpos;

void main()
{
    FragColor = vec4(color, 1.0);
    gl_FragDepth = gl_FragCoord.z - 0.00001;
} 
