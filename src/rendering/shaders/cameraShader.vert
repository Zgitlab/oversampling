#version 330 core

layout(location = 0) in vec3 pos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

out vec3 fpos;

void main()
{
  vec4 viewModelPos = view * model * vec4(pos, 1.0);
  fpos = viewModelPos.xyz;
  gl_Position = proj * viewModelPos;
}
